#pragma once

#include <SDL.h>
#include <SDL_events.h>
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>

#include <util.hpp>
#include <simple_buffer.hpp>
#include <hole_vector.hpp>
#include <sampler.hpp>
#include <aabb.hpp>

#include "mesh.hpp"
#include "component.hpp"

namespace renderer {
class Device;
}

struct PointLight : Component<PointLight> {
	glm::vec3 position;
	glm::vec3 color;
	float power;
};

class DirectionalShadowPipeline;

struct DirectionalLight : Component<DirectionalLight> {
	glm::vec3 direction;
	glm::vec3 color;
	DirectionalShadowPipeline* directional_shadow_pipeline; // MUST BE DELETED BEFORE COMPONENT REMOVAL TODO: that's dumb
	float power;
	float max_shadow_distance;
	float bias;
};


struct Basis {
	glm::vec3 x = glm::vec3(1, 0, 0);
	glm::vec3 y = glm::vec3(0, 1, 0);
	glm::vec3 z = glm::vec3(0, 0, 1);
};


struct Transform3D {
	glm::mat4 model_matrix = glm::identity<glm::mat4>();
	Basis basis;
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale = glm::vec3(1, 1, 1);
	renderer::AABB local_aabb = renderer::AABB();
	renderer::AABB global_aabb = renderer::AABB();

	void update_model_matrix();
	void update_aabb();
	void update_basis();
	void update_rotation_scale();
	void parent(const Transform3D& parent);
};

struct MeshComponent : Component<MeshComponent> {
	Mesh mesh;
};

struct Texture : Component<Texture> {
	renderer::Sampler texture;
};

struct Material : Component<Material> {
	glm::vec4 color;
	float metallic = 0.0f;
	float roughness = 1.0f;
	Texture::ID color_texture;
	Texture::ID normal_texture;
	bool has_transmission = false;
};

struct MeshInstance : Component<MeshInstance>, Transform3D {
	MeshComponent::ID mesh;
	glm::vec4 color;

	Material::ID material = 0;
};

struct Camera : Component<Camera>, Transform3D {
	glm::mat4 projection_matrix;
	glm::mat4 view_projection;
	float fov;
	float aspect;
	float near;
	float far;

	void update_model_matrix();
	void update_projection_matrix();
	void update_view_projection();
	void parent(const Transform3D& parent);
	static Camera create_camera(float fov, float aspect, float near, float far);
};


struct SpinLight : Component<SpinLight> {
	PointLight::ID light;
	float current_angle;
	float speed;
};

struct FlyCamera : Component<FlyCamera> {
	glm::vec3 input = glm::vec3();
	float speed = 1.0;
	glm::vec2 angle = glm::vec2();
};

template <typename T>
struct WorldComponentTables {
	ComponentTable<T> component_table;
};

struct InputEvent : Component<InputEvent> {
	SDL_Event event;
};


template <typename... Args>
struct WorldGeneric : WorldComponentTables<Args>... {
	using System = void (*)(WorldGeneric<Args...>&);

	Camera camera;

	UnorderedVector<System> systems;

	template <typename T>
	ComponentTable<T>& get_components()
	{
		return WorldComponentTables<T>::component_table;
	}

	template <typename T>
	typename T::ID add_component(T&& component)
	{
		return get_components<T>().add_component(renderer::move(component));
	}

	template <typename T>
	T* get_component(typename T::ID id)
	{
		return get_components<T>().get_component(id);
	}

	template <typename T>
	bool remove_component(typename T::ID id)
	{
		return get_components<T>().remove_component(id);
	}

	template <typename T>
	T& operator[](size_t index) { return get_components<T>()[index]; }

	template <typename T>
	T* begin() { return get_components<T>().begin(); }

	template <typename T>
	T* end() { return get_components<T>().end(); }

	template <typename T>
	size_t get_size() { return get_components<T>().get_size(); }

	void load_gltf(
		renderer::Device* device, 
		const char* path, 
		glm::vec3 position = glm::vec3(),
		glm::vec3 rotation = glm::vec3(),
		glm::vec3 scale = glm::vec3(1, 1, 1));
};

using World = WorldGeneric<
	PointLight,
	DirectionalLight,
	MeshComponent,
	MeshInstance,
	Texture,
	Material,
	SpinLight,
	FlyCamera,
	InputEvent
>;

using System=World::System;
