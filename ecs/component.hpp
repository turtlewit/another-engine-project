#pragma once

#include <util.hpp>
#include <simple_buffer.hpp>
#include <hole_vector.hpp>

using GenerationalIndex = uint64_t;

template <typename T>
class ComponentTable {
public:
	static constexpr bool compare_generation(GenerationalIndex a, GenerationalIndex b)
	{
		return (a & 0xffffffff00000000) == (b & 0xffffffff00000000);
	}

	static constexpr uint32_t get_generation(GenerationalIndex from)
	{
		return renderer::u32(from >> 32);
	}

	static constexpr uint32_t get_index(GenerationalIndex from)
	{
		return renderer::u32(from);
	}

	static constexpr GenerationalIndex make_index(uint32_t generation, uint32_t index)
	{
		return renderer::u64(index) | (renderer::u64(generation) << 32);
	}

	UnorderedVector<T>& get_component_storage() { return component_storage; };

	T* begin() { return component_storage.begin(); }
	T* end() { return component_storage.end(); }

	size_t get_size() { return component_storage.get_size(); }

	T& operator[](size_t i) { return component_storage.get_buffer()[i]; }

	typename T::ID add_component(T&& component)
	{
		size_t next_index = entity_component_table.get_next_index();
		GenerationalIndex next_gen_index = entity_component_table[next_index];

		GenerationalIndex gen_index = make_index(get_generation(next_gen_index) + 1, renderer::u32(component_storage.get_size()));
		size_t ect_index = entity_component_table.add(make_index(get_generation(gen_index), component_storage.get_size()));

		typename T::ID component_id = make_index(get_generation(gen_index), renderer::u32(ect_index));

		component_storage.add(renderer::move(component));
		component_storage.get_buffer()[component_storage.get_size() - 1].id = component_id;

		return component_id;
	}

	T* get_component(typename T::ID id)
	{
		//TODO: remove branching with multiplication maybe
		// generation of 0 is automatically invalid
		if (id < 0x0000000100000000)
			return nullptr;

		GenerationalIndex index = entity_component_table[get_index(id.id)];

		if (!compare_generation(id.id, index))
			return nullptr;

		return (component_storage.get_buffer() + get_index(index));
	}

	bool remove_component(typename T::ID id)
	{
		// generation of 0 is automatically invalid
		if (id < 0x0000000100000000)
			return false;

		size_t ect_index = get_index(id.id);

		GenerationalIndex component_id = entity_component_table[ect_index];

		if (!compare_generation(id.id, component_id))
			return false;

		size_t component_index = get_index(component_id);

		component_storage.remove(component_storage.begin() + component_index);
		entity_component_table.remove(ect_index);

		if (component_index + 1 == component_storage.get_size()) {
			// no swapping will occur
			return true;
		}

		// swapping will occur
		typename T::ID swapped_id = component_storage.get_buffer()[component_index].id;
		entity_component_table[get_index(swapped_id.id)] = make_index(get_generation(swapped_id.id), component_index);

		return true;
	}

	void clear()
	{
		component_storage.clear();
		entity_component_table.clear();
	}

private:
	UnorderedVector<T> component_storage;
	HoleVector<GenerationalIndex> entity_component_table;
};

template <typename T>
struct Component {
	struct ID {
		GenerationalIndex id;

		ID() = default;
		ID(const ID& other) = default;
		ID(ID&& other) = default;
		ID& operator=(const ID&) = default;
		ID& operator=(ID&&) = default;

		ID(GenerationalIndex id) : id{ id }
		{
		}

		ID& operator=(GenerationalIndex from)
		{
			id = from;
			return *this;
		}

		bool operator==(GenerationalIndex o) const
		{
			return id == o;
		}

		bool operator>(GenerationalIndex o) const
		{
			return id > o;
		}

		bool operator<(GenerationalIndex o) const
		{
			return id < o;
		}
	};

	ID id;
};
