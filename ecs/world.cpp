#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/orthonormalize.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>

#include <util.hpp>
#include <gltf.hpp>
#include <device.hpp>
#include <logger.hpp>

#include "world.hpp"

using namespace renderer;

void Transform3D::update_model_matrix()
{
	model_matrix = glm::mat4(glm::mat3(basis.x, basis.y, basis.z));
	model_matrix = glm::translate(glm::identity<glm::mat4>(), position) * model_matrix;
	update_aabb();
}

void Transform3D::update_aabb()
{
	if (!local_aabb.is_empty())
		global_aabb = local_aabb.transformed(model_matrix);
	else
		global_aabb = AABB(position);
}

void Transform3D::update_basis()
{
	glm::mat4 new_matrix = glm::eulerAngleYXZ(rotation.y, rotation.x, rotation.z);
	new_matrix = glm::scale(new_matrix, scale);
	glm::mat3 new_basis = glm::mat3(new_matrix);
	basis = Basis{ glm::column(new_basis, 0), glm::column(new_basis, 1), glm::column(new_basis, 2) };
}

void Transform3D::update_rotation_scale()
{
	// TODO: This probably doesn't work
	scale = glm::vec3(
		glm::length(basis.x),
		glm::length(basis.y),
		glm::length(basis.z)
	);

	glm::mat4 matrix = glm::mat4(glm::orthonormalize(glm::mat3(basis.x, basis.y, basis.z)));

	glm::extractEulerAngleYXZ(matrix, rotation.y, rotation.x, rotation.z);
}

void Transform3D::parent(const Transform3D& parent)
{
	update_model_matrix();
	model_matrix = parent.model_matrix * model_matrix;
}

void Camera::update_model_matrix()
{
	Transform3D::update_model_matrix();
	model_matrix = glm::affineInverse(model_matrix);
	//model_matrix = glm::mat4(glm::affineInverse(glm::orthonormalize(glm::mat3(basis.x, basis.y, basis.z))));
	//model_matrix = glm::translate(model_matrix, -position);

	update_view_projection();
}

void Camera::parent(const Transform3D& parent)
{
	Transform3D::update_model_matrix();
	model_matrix = glm::affineInverse(model_matrix) * glm::affineInverse(parent.model_matrix);

	update_view_projection();
}

void Camera::update_projection_matrix()
{
	projection_matrix = glm::perspective(glm::radians(fov), aspect, near, far);
	// TODO:
	//projection_matrix[1][1] *= -1;

	update_view_projection();
}

void Camera::update_view_projection()
{
	view_projection = projection_matrix * model_matrix;
}

Camera Camera::create_camera(float fov, float aspect, float near, float far)
{
	Camera camera = Camera{};
	
	camera.fov = fov;
	camera.aspect = aspect;
	camera.near = near;
	camera.far = far;

	camera.update_projection_matrix();

	return camera;
}

namespace {
glm::quat quat_from_euler(const glm::vec3& euler)
{
	glm::quat r = glm::identity<glm::quat>();

	r = glm::rotate(r, euler.z, glm::vec3(0, 0, 1));
	r = glm::rotate(r, euler.x, glm::vec3(1, 0, 0));
	r = glm::rotate(r, euler.y, glm::vec3(0, 1, 0));

	return r;
}

} // namespace

template <>
void World::load_gltf(Device* device, const char* path, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
{
	Gltf gltf{ device, path };

	Vector<Sampler> samplers{};
	Vector<size_t> indices{};
	gltf.get_textures(samplers, indices);

	Vector<Texture::ID> textures{};
	textures.reserve(samplers.get_size());

	for (auto& sampler : samplers) {
		textures.add(add_component<Texture>(Texture{ .texture = move(sampler) }));
	}
	samplers.clear();

	for (auto& mesh : gltf.get_meshes()) {
		MeshComponent::ID m = add_component(MeshComponent{
			.mesh = Mesh(
				device, 
				mesh.get_vertex_count(), mesh.get_vertex_buffer(), 
				mesh.get_index_count(), mesh.get_index_buffer(), 
				mesh.get_index_type()
			),
		});

		Material::ID mat = 0;

		size_t color_tex, normal_tex;

		bool has_color = mesh.get_albedo_texture(color_tex);
		bool has_normal = mesh.get_normal_texture(normal_tex);

		if (has_color || has_normal || mesh.has_transmission()) {
			mat = add_component(Material{
				.color = mesh.get_color(),
				.metallic = mesh.get_metallic(),
				.roughness = mesh.get_roughness(),
				.color_texture = has_color ? textures[indices[color_tex]] : 0,
				.normal_texture = has_normal ? textures[indices[normal_tex]] : 0,
				.has_transmission = mesh.has_transmission(),
			});
		}

		MeshInstance::ID id = add_component(MeshInstance{
			.mesh = m,
			.color = mesh.get_color(),
			.material = mat,
		});

		MeshInstance& mc = *get_component<MeshInstance>(id);
		mc.position = mesh.get_position() * scale * -1.0f;
		mc.position += position;

		glm::mat4 r = glm::toMat4(quat_from_euler(rotation) * mesh.get_rotation());
		glm::extractEulerAngleYXZ(r, mc.rotation.y, mc.rotation.x, mc.rotation.z);
		mc.rotation.y *= -1;
		mc.rotation.z *= -1;
		mc.scale *= scale;

		mc.local_aabb = mesh.get_aabb();

		mc.update_basis();
		mc.update_model_matrix();
	}
}
