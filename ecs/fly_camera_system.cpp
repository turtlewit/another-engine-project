#include <glm/gtx/string_cast.hpp>
#include <glm/ext/quaternion_float.hpp>
#include <glm/ext/quaternion_trigonometric.hpp>
#include <glm/ext/quaternion_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <SDL_mouse.h>

#include <logger.hpp>

#include "fly_camera_system.hpp"

using namespace renderer;

void fly_camera_system(World& world)
{
	double delta = 1.0 / 120.0;

	glm::vec3 input = glm::vec3(0.0);
	glm::vec2 mouse_input = glm::vec2(0.0);
	float speed = 0.0;

	for (auto& e : world.get_components<InputEvent>()) {
		switch (e.event.type) {
			case SDL_KEYDOWN:
			case SDL_KEYUP: {
				float dir = e.event.key.state == SDL_PRESSED ? 1.0 : -1.0;
				switch (e.event.key.keysym.sym) {
					case SDLK_w:
						input.z += -1.0 * dir;
						break;
					case SDLK_s:
						input.z +=  1.0 * dir;
						break;
					case SDLK_a:
						input.x += -1.0 * dir;
						break;
					case SDLK_d:
						input.x +=  1.0 * dir;
						break;
					case SDLK_e:
					case SDLK_SPACE:
						input.y += -1.0 * dir;
						break;
					case SDLK_q:
					case SDLK_LCTRL:
						input.y +=  1.0 * dir;
						break;
					case SDLK_LSHIFT:
						speed += 1.0 * dir;
				}
			} break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				if (e.event.button.button == SDL_BUTTON_RIGHT) {
					SDL_SetRelativeMouseMode(e.event.button.state == SDL_PRESSED ? SDL_TRUE : SDL_FALSE);
				}
				break;
			case SDL_MOUSEMOTION:
				if (SDL_GetRelativeMouseMode()) {
					mouse_input += glm::vec2(-f32(e.event.motion.xrel), f32(e.event.motion.yrel)) * 0.06f;
				}
		}
	}

	glm::clamp(input, glm::vec3(-1.0), glm::vec3(1.0));

	for (auto& fly_camera : world.get_components<FlyCamera>()) {
		fly_camera.input += input;
		fly_camera.angle += mouse_input;
		fly_camera.speed += speed;
	}

	for (auto& fly_camera : world.get_components<FlyCamera>()) {
		glm::mat4 rot = glm::mat4((
			glm::angleAxis(fly_camera.angle.x * f32(delta), glm::vec3(0, 1, 0)) *
			glm::angleAxis(fly_camera.angle.y * f32(delta), glm::vec3(1, 0, 0))
		));

		glm::extractEulerAngleYXZ(rot, world.camera.rotation.y, world.camera.rotation.x, world.camera.rotation.z);

		world.camera.update_basis();

		world.camera.position += ( 
			world.camera.basis.x * fly_camera.input.x +
			glm::vec3(0, 1, 0) * fly_camera.input.y +
			world.camera.basis.z * fly_camera.input.z
		) * fly_camera.speed * f32(delta);

		world.camera.update_model_matrix();
	}
}
