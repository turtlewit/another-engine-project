Files: core/, renderer/, ecs/, examples/
Copyright 2021 Kat Witten
License: MIT (see LICENSE.txt)

Files: thirdparty/cgltf/cgltf.h
Copyright (c) 2018 Johannes Kuhlmann
License: MIT (see thirdparty/cgltf/cgltf.h)

Files: thirdparty/spirv-reflect/
Copyright 2017-2018 Google Inc.
License: Apache License, Version 2.0 (see thirdparty/spirv-reflect/LICENSE)

Files: thirdparty/spng/
Copyright (c) 2018-2020, Randy
All rights reserved.
License: BSD 2-Clause License (see thirdparty/spng/LICENSE)
