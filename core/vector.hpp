// vector.hpp
//
// Created by Kat Witten

#pragma once

#include "unordered_vector.hpp"

template <typename T>
class Vector : UnorderedVector<T> {
public:
	using Iterator = typename UnorderedVector<T>::Iterator;
	
	using UnorderedVector<T>::add;
	using UnorderedVector<T>::reserve;
	using UnorderedVector<T>::resize;
	using UnorderedVector<T>::resize_uninitialized;
	using UnorderedVector<T>::clear;
	using UnorderedVector<T>::begin;
	using UnorderedVector<T>::end;
	using UnorderedVector<T>::cend;
	using UnorderedVector<T>::get_size;
	using UnorderedVector<T>::get_capacity;
	using UnorderedVector<T>::get_buffer;
	using UnorderedVector<T>::sum;
	using UnorderedVector<T>::sum_to;


	void remove(Iterator element)
	{
		assert(is_element_in_range(element) == true);

		element->~T();

		size_t n = --size - static_cast<size_t>(element - buffer);

		// memmove because arrays overlap
		std::memmove(element, element + 1, n);
	}

	void remove(size_t index) { remove(&buffer[index]); }

	T pop(Iterator element) 
	{
		assert(is_element_in_range(element) == true);

		T moved_element = static_cast<T&&>(*element);

		remove(element);

		return moved_element;
	}

	T pop(size_t index) { return pop(&buffer[index]); }

	T pop() { return pop(end() - 1); }

	T& operator[](size_t index) noexcept
	{
		return buffer[index];
	}

	const T& operator[](size_t index) const noexcept
	{
		return buffer[index];
	}

	T& at(size_t index)
	{ 
		assert(is_element_in_range(&buffer[index]) == true);

		return operator[](index); 
	}

	const T& at(size_t index) const
	{
		return at(index);
	}

	Vector() noexcept 
		: UnorderedVector<T>{}
	{
	}

	Vector(const Vector& from)
		: UnorderedVector<T>{ from }
	{
	}

	template <typename U>
	Vector(Vector<U>&& from) noexcept
		: UnorderedVector<T>{ static_cast<UnorderedVector<U>&&>(from) }
	{
	}

	Vector(Vector&& from) noexcept
		: UnorderedVector<T>{ static_cast<UnorderedVector<T>&&>(from) }
	{
	}

	Vector& operator=(const Vector& from)
	{
		UnorderedVector<T>::operator=(from);
		return *this;
	}

	Vector& operator=(Vector&& from)
	{
		UnorderedVector<T>::operator=(static_cast<Vector&&>(from));
		return *this;
	}

protected:
	using UnorderedVector<T>::size;
	using UnorderedVector<T>::capacity;
	using UnorderedVector<T>::buffer;
	using UnorderedVector<T>::is_element_in_range;
};
