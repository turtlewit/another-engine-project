// simple_buffer.hpp
//
// Created by Kat Witten

#pragma once

#include <cstring>
#include <initializer_list>

template <typename T>
class SimpleBuffer {
public:
	template<typename U>
	friend class SimpleBuffer;

	T* get_buffer() noexcept { return buffer; }
	const T* get_buffer() const noexcept { return buffer; }
	size_t get_size() const noexcept { return size; }

	void resize(size_t new_size);

	void copy_from(const SimpleBuffer& other);

	void clear();

	SimpleBuffer operator+(const SimpleBuffer& other) const;

	SimpleBuffer() noexcept;
	SimpleBuffer(std::initializer_list<T> initial_contents);
	SimpleBuffer(size_t initial_size, const T* initial_contents);

	SimpleBuffer(const SimpleBuffer& from);
	SimpleBuffer(SimpleBuffer&& from) noexcept;
	template <typename U>
	SimpleBuffer(SimpleBuffer<U>&& from) noexcept;
	SimpleBuffer& operator=(SimpleBuffer&& from) noexcept;

	~SimpleBuffer() noexcept;
private:
	T* buffer;
	size_t size;
};

template <typename T>
void SimpleBuffer<T>::resize(size_t new_size)
{
	T* new_buffer = static_cast<T*>(::operator new(sizeof(T) * new_size));

	if (buffer)
		std::memcpy(new_buffer, buffer, sizeof(T) * size);

	::operator delete(buffer);

	buffer = new_buffer;
	size = new_size;
}

template <typename T>
void SimpleBuffer<T>::copy_from(const SimpleBuffer& other)
{
	::operator delete(buffer);

	buffer = static_cast<T*>(::operator new(sizeof(T) * other.size));
	
	if (buffer)
		std::memcpy(buffer, other.buffer, sizeof(T) * other.size);

	size = other.size;
}

template <typename T>
void SimpleBuffer<T>::clear()
{
	::operator delete(buffer);
	buffer = nullptr;
	size = 0;
}

template <typename T>
SimpleBuffer<T> SimpleBuffer<T>::operator+(const SimpleBuffer& other) const
{
	SimpleBuffer r;
	r.resize(size + other.size);
	std::memcpy(r.buffer, buffer, sizeof(T) * size);
	std::memcpy(r.buffer + size, other.buffer, sizeof(T) * other.size);
	return r;
}

template <typename T>
SimpleBuffer<T>::SimpleBuffer() noexcept
	: buffer{ nullptr }, size{ 0 }
{
}

template <typename T>
SimpleBuffer<T>::SimpleBuffer(std::initializer_list<T> initial_contents)
	: buffer{ nullptr }, size{ initial_contents.size() }
{
	buffer = static_cast<T*>(::operator new(sizeof(T) * size));
	std::memcpy(buffer, initial_contents.begin(), sizeof(T) * size);
}

template <typename T>
SimpleBuffer<T>::SimpleBuffer(size_t initial_size, const T* initial_contents)
	: buffer{ static_cast<T*>(::operator new(sizeof(T) * initial_size)) }, size{ initial_size } 
{
	std::memcpy(reinterpret_cast<void*>(buffer), initial_contents, sizeof(T) * size);
}

template <typename T>
SimpleBuffer<T>::SimpleBuffer(const SimpleBuffer& from)
	: SimpleBuffer{ from.size, from.buffer }
{
}

template <typename T>
SimpleBuffer<T>::SimpleBuffer(SimpleBuffer&& from) noexcept
	: buffer{ from.buffer }, size{ from.size }
{
	from.buffer = nullptr;
	from.size = 0;
}

template <typename T>
template <typename U>
SimpleBuffer<T>::SimpleBuffer(SimpleBuffer<U>&& from) noexcept
	: buffer{ reinterpret_cast<T*>(from.buffer) }, size{ (sizeof(U) * from.size) / sizeof(T) }
{
	static_assert( sizeof(U) % sizeof(T) == 0 );
	from.buffer = nullptr;
	from.size = 0;
}


template <typename T>
SimpleBuffer<T>& SimpleBuffer<T>::operator=(SimpleBuffer&& from) noexcept
{
	T* temp = buffer;
	buffer = from.buffer;
	from.buffer = temp;

	size_t temp_size = size;
	size = from.size;
	from.size = temp_size;

	return *this;
}

template <typename T>
SimpleBuffer<T>::~SimpleBuffer() noexcept
{
	::operator delete(buffer);
}
