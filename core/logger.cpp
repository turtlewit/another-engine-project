#include <cstdarg>

#ifdef _WIN32
# include <windows.h>
#endif

#include "logger.hpp"

namespace renderer {

int Logger::get_format_length(const char* m, ...)
{
	va_list args;
	va_start(args, m);

	int length = vsnprintf(nullptr, 0, m, args);

	va_end(args);
	
	return length;
}

char* Logger::format_string(const char* m, ...)
{
	va_list args;
	va_start(args, m);

	int length = vsnprintf(nullptr, 0, m, args);
	char* buffer = new char[length];
	vsnprintf(buffer, length, m, args);

	va_end(args);

	return buffer;
}

char* Logger::vformat_string(const char* m, va_list args) 
{
	int length = vsnprintf(nullptr, 0, m, args);
	char* buffer = new char[length];
	vsnprintf(buffer, length, m, args);

	return buffer;
}


#define VPRINT(f) \
	va_list args;\
	va_start(args, m);\
	vfprintf(f, m, args);\
	va_end(args);


void Logger::log_info(const char* m, ...)
{
	printf(info_prefix);
	VPRINT(stdout);
}

void Logger::print_info(const char* m, ...)
{
	VPRINT(stdout);
}

void Logger::log_warning(const char* m, ...)
{
	printf(warning_prefix);
	VPRINT(stdout);
}

void Logger::print_warning(const char* m, ...)
{
	VPRINT(stdout);
}

void Logger::log_error(const char* m, ...)
{
	fprintf(stderr, error_prefix);
#ifdef _WIN32
	{
		va_list args;
		va_start(args, m);
		char* msg = vformat_string(m, args);
		va_end(args);

		MessageBox(nullptr, msg, "ERROR", 0);
		GetStockObject(0);
		
		delete[] msg;
	}
#endif
	VPRINT(stderr);
}

void Logger::print_error(const char* m, ...)
{
	VPRINT(stderr);
}

#define RESULT_CASE(name) \
	case name: {\
		return #name;\
	}

const char* Logger::get_result_name(VkResult result)
{
	switch (result) {
		RESULT_CASE(VK_SUCCESS)
		RESULT_CASE(VK_NOT_READY)
		RESULT_CASE(VK_TIMEOUT)
		RESULT_CASE(VK_EVENT_SET)
		RESULT_CASE(VK_EVENT_RESET)
		RESULT_CASE(VK_INCOMPLETE)
		RESULT_CASE(VK_ERROR_OUT_OF_HOST_MEMORY)
		RESULT_CASE(VK_ERROR_OUT_OF_DEVICE_MEMORY)
		RESULT_CASE(VK_ERROR_INITIALIZATION_FAILED)
		RESULT_CASE(VK_ERROR_DEVICE_LOST)
		RESULT_CASE(VK_ERROR_MEMORY_MAP_FAILED)
		RESULT_CASE(VK_ERROR_LAYER_NOT_PRESENT)
		RESULT_CASE(VK_ERROR_EXTENSION_NOT_PRESENT)
		RESULT_CASE(VK_ERROR_FEATURE_NOT_PRESENT)
		RESULT_CASE(VK_ERROR_INCOMPATIBLE_DRIVER)
		RESULT_CASE(VK_ERROR_TOO_MANY_OBJECTS)
		RESULT_CASE(VK_ERROR_FORMAT_NOT_SUPPORTED)
		RESULT_CASE(VK_ERROR_FRAGMENTED_POOL)
		RESULT_CASE(VK_ERROR_UNKNOWN)
		RESULT_CASE(VK_ERROR_OUT_OF_POOL_MEMORY)
		RESULT_CASE(VK_ERROR_INVALID_EXTERNAL_HANDLE)
		RESULT_CASE(VK_ERROR_FRAGMENTATION)
		RESULT_CASE(VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS)
		RESULT_CASE(VK_ERROR_SURFACE_LOST_KHR)
		RESULT_CASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR)
		RESULT_CASE(VK_SUBOPTIMAL_KHR)
		RESULT_CASE(VK_ERROR_OUT_OF_DATE_KHR)
		RESULT_CASE(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR)
		RESULT_CASE(VK_ERROR_VALIDATION_FAILED_EXT)
		RESULT_CASE(VK_ERROR_INVALID_SHADER_NV)
		//RESULT_CASE(VK_ERROR_INCOMPATIBLE_VERSION_KHR)
		RESULT_CASE(VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT)
		RESULT_CASE(VK_ERROR_NOT_PERMITTED_EXT)
		RESULT_CASE(VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT)
		RESULT_CASE(VK_THREAD_IDLE_KHR)
		RESULT_CASE(VK_THREAD_DONE_KHR)
		RESULT_CASE(VK_OPERATION_DEFERRED_KHR)
		RESULT_CASE(VK_OPERATION_NOT_DEFERRED_KHR)
		RESULT_CASE(VK_PIPELINE_COMPILE_REQUIRED_EXT)
		//RESULT_CASE(VK_ERROR_OUT_OF_POOL_MEMORY_KHR)
		//RESULT_CASE(VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR)
		//RESULT_CASE(VK_ERROR_FRAGMENTATION_EXT)
		//RESULT_CASE(VK_ERROR_INVALID_DEVICE_ADDRESS_EXT)
		//RESULT_CASE(VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR)
		//RESULT_CASE(VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT)
		//RESULT_CASE(VK_RESULT_MAX_ENUM)
	}
	return "UNKNOWN_ERROR_NAME";
}

} // namespace renderer
