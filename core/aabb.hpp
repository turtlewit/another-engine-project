#pragma once

#include <glm/glm.hpp>

#include "util.hpp"

namespace renderer {

class AABB {
public:
	glm::vec3 min, max;

	AABB() = default;

	AABB(const glm::vec3& point) : min{ point }, max{ point }
	{
	}

	AABB(const glm::vec3& min, const glm::vec3& max) : min{ min }, max{ max }
	{
	}

	AABB(const AABB&) = default;

	AABB& operator=(const AABB&) = default;

	AABB& operator+=(const AABB& other)
	{
		with_point(other.min);
		with_point(other.max);

		return *this;
	}

	AABB operator+(const AABB& other) const
	{
		AABB r{};
		r += *this;
		r += other;

		return r;
	}

	void with_point(const glm::vec3& point)
	{
#define WITH_POINT_MACRO(attrib)\
		max.attrib = renderer::max(point.attrib, max.attrib);\
		min.attrib = renderer::min(point.attrib, min.attrib);
		WITH_POINT_MACRO(x);
		WITH_POINT_MACRO(y);
		WITH_POINT_MACRO(z);
#undef WITH_POINT_MACRO
	}

	bool is_empty() const
	{
		return max == min;
	}

	AABB transformed(const glm::mat4 by) const
	{
		const glm::vec3& nnn = min;
		const glm::vec3& ppp = max;

		glm::vec3 nnp = glm::vec3(nnn.x, nnn.y, ppp.z);
		glm::vec3 npn = glm::vec3(nnn.x, ppp.y, nnn.z);
		glm::vec3 npp = glm::vec3(nnn.x, ppp.y, ppp.z);
		glm::vec3 pnn = glm::vec3(ppp.x, nnn.y, nnn.z);
		glm::vec3 pnp = glm::vec3(ppp.x, nnn.y, ppp.z);
		glm::vec3 ppn = glm::vec3(ppp.x, ppp.y, nnn.z);

		AABB r{by * glm::vec4(nnn, 1.0)};
		for (auto& point : {nnp, npn, npp, pnn, pnp, ppn, ppp}) {
			r.with_point(by * glm::vec4(point, 1.0));
		}

		return r;
	}
};

} // namespace renderer
