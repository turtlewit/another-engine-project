// hole_vector.hpp
//
// Created by Kat Witten
//
// A vector that leaves "holes", which are then filled on the next insertion.
// Iteration is NOT SAFE, you will get uninitialized memory when hitting a hole.

#pragma once

#include "vector.hpp"

template <typename T>
class HoleVector : UnorderedVector<T> {
public:
	using Iterator = typename UnorderedVector<T>::Iterator;
	
	using UnorderedVector<T>::reserve;
	using UnorderedVector<T>::resize;
	using UnorderedVector<T>::resize_uninitialized;
	using UnorderedVector<T>::begin;
	using UnorderedVector<T>::end;
	using UnorderedVector<T>::cend;
	using UnorderedVector<T>::get_size;
	using UnorderedVector<T>::get_capacity;
	using UnorderedVector<T>::get_buffer;

	size_t add(const T& element)
	{
		if (hole_stack.get_size() == 0) {
			UnorderedVector<T>::add(element);
			return size - 1;
		}

		size_t hole_index = hole_stack.pop();
		
		memset(static_cast<void*>(buffer + hole_index), 0, sizeof(T));
		::new(static_cast<void*>(buffer + hole_index)) T(element);

		return hole_index;
	}

	void remove(Iterator element)
	{
		assert(is_element_in_range(element) == true);

		element->~T();

		if (is_last_element(element)) {
			size -= 1;
			return;
		}

		// make hole
		hole_stack.add(static_cast<size_t>(element - buffer));
	}

	void clear()
	{
		UnorderedVector<T>::clear();
		hole_stack.clear();
		hole_stack.reserve(INITIAL_HOLE_STACK_SIZE);
	}

	void remove(size_t index) { remove(buffer + index); }

	size_t get_next_index()
	{
		if (hole_stack.get_size() == 0) {
			if (size == capacity)
				reallocate();

			memset(static_cast<void*>(buffer + size), 0, sizeof(T));
			return size;
		}

		return *(hole_stack.end() - 1);
	}

	T& operator[](size_t index) noexcept
	{
		return buffer[index];
	}

	const T& operator[](size_t index) const noexcept
	{
		return buffer[index];
	}

	HoleVector() noexcept 
		: UnorderedVector<T>{}
		, hole_stack{}
	{
		hole_stack.reserve(INITIAL_HOLE_STACK_SIZE);
	}

	HoleVector(const HoleVector& from)
		: UnorderedVector<T>{ from }
		, hole_stack{ from.hole_stack }
	{
	}

	template <typename U>
	HoleVector(HoleVector<U>&& from) noexcept
		: UnorderedVector<T>{ static_cast<UnorderedVector<U>&&>(from) }
		, hole_stack{ static_cast<Vector<size_t>&&>(from.hole_stack) }
	{
	}

	HoleVector(HoleVector&& from) noexcept
		: UnorderedVector<T>{ static_cast<UnorderedVector<T>&&>(from) }
		, hole_stack{ static_cast<Vector<size_t>&&>(from.hole_stack) }
	{
	}

	HoleVector& operator=(const HoleVector& from)
	{
		UnorderedVector<T>::operator=(from);
		hole_stack = from.hole_stack;
		return *this;
	}

	HoleVector& operator=(HoleVector&& from)
	{
		UnorderedVector<T>::operator=(static_cast<HoleVector&&>(from));
		hole_stack = static_cast<Vector<size_t>&&>(from.hole_stack);
		return *this;
	}

protected:
	Vector<size_t> hole_stack;
	using UnorderedVector<T>::size;
	using UnorderedVector<T>::capacity;
	using UnorderedVector<T>::buffer;
	using UnorderedVector<T>::is_element_in_range;
	using UnorderedVector<T>::reallocate;

	static constexpr auto INITIAL_HOLE_STACK_SIZE = 10;
};
