#pragma once 

#include <cerrno>
#define _USE_MATH_DEFINES
#include <cmath>

#include <simple_buffer.hpp>

#include "logger.hpp"

#define STRNCMP_LITERAL(literal, to) (strncmp(literal, to, sizeof(literal) - 1))

namespace renderer {

constexpr uint8_t* BYTEARRAY(auto b) { return static_cast<uint8_t*>(b); }

constexpr auto f32(auto x) { return static_cast<float>(x); }
constexpr auto f64(auto x) { return static_cast<double>(x); }

constexpr auto  u8(auto x) { return static_cast<uint8_t>(x); }
constexpr auto u16(auto x) { return static_cast<uint16_t>(x); }
constexpr auto u32(auto x) { return static_cast<uint32_t>(x); }
constexpr auto u64(auto x) { return static_cast<uint64_t>(x); }

constexpr auto  i8(auto x) { return static_cast<int8_t>(x); }
constexpr auto i16(auto x) { return static_cast<int16_t>(x); }
constexpr auto i32(auto x) { return static_cast<int32_t>(x); }
constexpr auto i64(auto x) { return static_cast<int64_t>(x); }

constexpr auto usize(auto x) { return static_cast<size_t>(x); }

constexpr double f64_mul(auto x, auto y) { return f64(x) * f64(y); }
constexpr double f64_div(auto x, auto y) { return f64(x) / f64(y); }

constexpr auto ceil_div(auto x, auto y) { return static_cast<decltype(x)>(ceil(f64_div(x, y))); }

template <typename T> constexpr T&& move(T& x) { return static_cast<T&&>(x); }

constexpr void drop(auto& x) { auto drop = move(x); }

template <typename T>
constexpr const T& max(const T& a, const T& b)
{
	return a > b ? a : b;
}

template <typename T, typename... Args>
constexpr const T& max(const T& a, const T& b, const Args&... args)
{
	const T& m = max(b, args...);
	return a > m ? a : m;
}

template <typename T>
constexpr const T& min(const T& a, const T& b)
{
	return a < b ? a : b;
}

template <typename T, typename... Args>
constexpr const T& min(const T& a, const T& b, const Args&... args)
{
	const T& m = min(b, args...);
	return a < m ? a : m;
}

inline char* concatenate_recurse(size_t size, char* buffer, const char* first)
{
	return strncat(buffer, first, size);
}

template <typename... Args>
char* concatenate_recurse(size_t size, char* buffer, const char* first, Args... args)
{
	strncat(buffer, first, size);
	return concatenate_recurse(size, buffer, args...);
}

template <typename... Args>
char* concatenate(Args... args)
{
	size_t total_size = ((strlen(args)) + ...);
	total_size += 1;

	char* buffer = new char[total_size];
	memset(buffer, '\0', total_size);

	return concatenate_recurse(total_size, buffer, args...);
}


class File {
public:
	static char* get_path(const char* filename, size_t size)
	{
		size_t new_size = size;
	
		for (const char* c = filename + (size - 1); c != filename - 1; --c) {
			if (*c == '/')
				break;
			--new_size;
		}
	
		if (new_size == 0)
			new_size = size;
	
		char* buffer = new char[new_size + 1];
		buffer[new_size] = '\0';
	
		strncpy(buffer, filename, new_size);
	
		return buffer;
	}

	static SimpleBuffer<uint8_t> read_whole_file(const char* filepath)
	{
		std::FILE* file = std::fopen(filepath, "rb");
	
		if (!file) {
			LOG_ERROR("Failed to open file %s: %s\n", filepath, strerror(errno));
			exit(errno);
		}
	
		std::fseek(file, 0, SEEK_END);
		size_t size = static_cast<size_t>(std::ftell(file));
		SimpleBuffer<uint8_t> bytes{};
		bytes.resize(size);
		std::fseek(file, 0, SEEK_SET);
		std::fread(bytes.get_buffer(), sizeof(uint8_t), size, file);
		std::fclose(file);
	
		return bytes;
	}
};

} // namespace renderer
