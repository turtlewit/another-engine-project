// unordered_vector.hpp
//
// Created by Kat Witten

#pragma once

// for std::memcpy
#include <cstring>
// for placement new
#include <new>
// for list initialization
#include <initializer_list>
// for assertions
#include <cassert>

template <typename T>
class UnorderedVector {
public:
	using Iterator = T*;

	void add(const T& element);
	void add(const T& element, size_t count);
	void add(T&& element);
	void add(std::initializer_list<T> elements);
	void remove(Iterator element);
	T pop(Iterator element);
	void reserve(size_t new_allocation_size);
	void resize(size_t new_size);
	void resize_uninitialized(size_t new_size);
	void clear() noexcept;

	Iterator begin() noexcept { return buffer; }
	const Iterator begin() const noexcept { return cbegin(); }
	Iterator end() noexcept { return buffer + size; }
	const Iterator end() const noexcept { return cend(); }
	const Iterator cbegin() const noexcept { return buffer; }
	const Iterator cend() const noexcept { return buffer + size; }

	size_t get_size() const noexcept { return size; }
	size_t get_capacity() const noexcept { return capacity; }
	T* get_buffer() { return buffer; }
	const T* get_buffer() const { return buffer; }

	T sum() const;
	T sum_to(size_t index) const;

	UnorderedVector() noexcept;
	UnorderedVector(const UnorderedVector& from);
	UnorderedVector(UnorderedVector&& from) noexcept;
	template <typename U>
	UnorderedVector(UnorderedVector<U>&& from) noexcept;
	UnorderedVector& operator=(const UnorderedVector& from);
	UnorderedVector& operator=(UnorderedVector&& from) noexcept;
	~UnorderedVector() noexcept;

protected:
	template <typename U>
	void swap(U& a, U& b)
	{
		U temp = static_cast<U&&>(a);
		a = static_cast<U&&>(b);
		b = static_cast<U&&>(temp);
	}

	size_t size;
	size_t capacity;
	T* buffer;

	void reallocate();
	bool is_element_in_range(const T* element) const;
	bool is_last_element(const T* element) const;
	void free_elements();
};


template <typename T>
void UnorderedVector<T>::add(const T& element)
{
	if (size == capacity)
		reallocate();

	memset(static_cast<void*>(buffer + size), 0, sizeof(T));
	::new(static_cast<void*>(&buffer[size++])) T(element);
}


template <typename T>
void UnorderedVector<T>::add(const T& element, size_t count)
{
	while (size + count >= capacity + 1)
		reallocate();

	memset(static_cast<void*>(buffer + size), 0, sizeof(T) * count);
	for (size_t i = 0; i < count; ++i)
		::new(static_cast<void*>(&buffer[size + i])) T(element);

	size += count;
}


template <typename T>
void UnorderedVector<T>::add(T&& element)
{
	if (size == capacity)
		reallocate();

	memset(static_cast<void*>(buffer + size), 0, sizeof(T));
	// call move contsructor on uninitialized memory
	::new(static_cast<void*>(&buffer[size++])) T(static_cast<T&&>(element));
}


template <typename T>
void UnorderedVector<T>::add(std::initializer_list<T> elements)
{
	while (size + elements.size() >= capacity + 1)
		reallocate();

	memset(static_cast<void*>(buffer + size), 0, sizeof(T) * elements.size());
	for (size_t i = 0; i < elements.size(); ++i) {
		::new(&buffer[size + i]) T(*(elements.begin() + i));
	}

	size += elements.size();
}


template <typename T>
void UnorderedVector<T>::remove(Iterator element)
{
	assert(is_element_in_range(element) == true);

	if (!is_last_element(element))
		swap(*element, buffer[size - 1]);

	// call destructor on last element
	buffer[--size].~T();
}


template <typename T>
T UnorderedVector<T>::pop(Iterator element)
{
	assert(is_element_in_range(element) == true);

	if (!is_last_element(element))
		swap(*element, buffer[size - 1]);

	T moved_last_element = static_cast<T&&>(buffer[--size]);

	return moved_last_element;
}


template <typename T>
void UnorderedVector<T>::reserve(size_t new_allocation_size)
{
	if (new_allocation_size <= capacity)
		return;

	T* new_buffer = static_cast<T*>(::operator new(sizeof(T) * new_allocation_size));
	std::memcpy(static_cast<void*>(new_buffer), buffer, sizeof(T) * size);
	::operator delete(static_cast<void*>(buffer));
	buffer = new_buffer;
	capacity = new_allocation_size;
}

template <typename T>
void UnorderedVector<T>::resize(size_t new_size)
{
	reserve(new_size);
	for (size_t i = size; i < new_size; ++i) {
		::new (buffer + i) T();
	}
	size = new_size;
}


template <typename T>
void UnorderedVector<T>::resize_uninitialized(size_t new_size)
{
	reserve(new_size);
	size = new_size;
}


template <typename T>
void UnorderedVector<T>::clear() noexcept
{
	free_elements();
	size = 0;
	capacity = 0;
}


template <typename T>
T UnorderedVector<T>::sum() const
{
	T total = T();

	for (const T& e : *this)
		total += e;

	return total;
}


template <typename T>
T UnorderedVector<T>::sum_to(size_t index) const
{
	T total = T();

	for (size_t i = 0; i < (index < size ? index : size); ++i)
		total += buffer[i];

	return total;
}


template <typename T>
UnorderedVector<T>::UnorderedVector() noexcept
	: size{0}, capacity{0}, buffer{nullptr}
{
}


template <typename T>
UnorderedVector<T>::UnorderedVector(const UnorderedVector& from)
	: size{from.size}, capacity{from.capacity}, buffer{nullptr}
{
	if (capacity == 0)
		return;

	buffer = static_cast<T*>(::operator new(sizeof(T) * capacity));
	for (size_t i = 0; i < size; ++i) {
		new(&buffer[i]) T(from.buffer[i]);
	}
}


template <typename T>
UnorderedVector<T>::UnorderedVector(UnorderedVector&& from) noexcept
	: size{0}, capacity{0}, buffer{nullptr}
{
	swap(size, from.size);
	swap(capacity, from.capacity);
	swap(buffer, from.buffer);
}

template <typename T>
template <typename U>
UnorderedVector<T>::UnorderedVector(UnorderedVector<U>&& from) noexcept
	: size{ (sizeof(U) * from.size) / sizeof(T) }
	, capacity{ (sizeof(U) * from.capacity) / sizeof(T) }
	, buffer{ reinterpret_cast<T*>(from.buffer) }
{
	// sizeof(U) must be integer divisable by sizeof(T)
	static_assert( sizeof(U) % sizeof(T) == 0 );
	from.size = 0;
	from.capacity = 0;
	from.buffer = nullptr;
}


template <typename T>
UnorderedVector<T>& UnorderedVector<T>::operator=(const UnorderedVector& from)
{
	free_elements();
	size = from.size;
	capacity = from.capacity;
	buffer = static_cast<T*>(::operator new(sizeof(T) * capacity));
	for (size_t i = 0; i < size; ++i) {
		new(&buffer[i]) T(from.buffer[i]);
	}
	return *this;
}


template <typename T>
UnorderedVector<T>& UnorderedVector<T>::operator=(UnorderedVector&& from) noexcept
{
	free_elements();
	size = from.size;
	from.size = 0;
	capacity = from.capacity;
	from.capacity = 0;
	swap(buffer, from.buffer);
	return *this;
}


template <typename T>
UnorderedVector<T>::~UnorderedVector() noexcept
{
	free_elements();
}


template <typename T>
void UnorderedVector<T>::reallocate()
{
	size_t new_capacity;
	if (capacity == 0)
		new_capacity = 1;
	else
		new_capacity = capacity << 1;
	T* new_buffer = static_cast<T*>(::operator new(sizeof(T) * new_capacity));
	std::memcpy(static_cast<void*>(new_buffer), buffer, sizeof(T) * size);
	::operator delete(static_cast<void*>(buffer));
	buffer = new_buffer;
	capacity = new_capacity;
}


template <typename T>
bool UnorderedVector<T>::is_element_in_range(const T* element) const
{
	return (element >= buffer) && (element < buffer + size);
}


template <typename T>
bool UnorderedVector<T>::is_last_element(const T* element) const
{
	return element == buffer + size - 1;
}


template <typename T>
void UnorderedVector<T>::free_elements()
{
	for (T* p = buffer; p != buffer + size; ++p)
		p->~T();

	::operator delete(static_cast<void*>(buffer));
	buffer = nullptr;
}
