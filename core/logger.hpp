#pragma once

#include <cstring>
#include <cstdio>
#include <cstdlib>

#include <vulkan/vulkan.h>

#define LOG_WARNING(...) \
	{Logger::print_warning( "\033[38;2;255;255;0m(%s:%d)\033[0m ", __func__, __LINE__ );\
	Logger::log_warning( __VA_ARGS__ );}
#define LOG_ERROR(...) \
	{Logger::print_error( "\033[38;2;255;0;0m(%s:%d)\033[0m ", __func__, __LINE__ );\
	Logger::log_error( __VA_ARGS__ );}

#define ASSERT_VK_SUCCESS(expression) \
	{\
		VkResult result;\
		if ((result = (expression)) != VK_SUCCESS) {\
			LOG_ERROR("%s == VK_SUCCESS is false. Result: %s\n", #expression, Logger::get_result_name(result));\
			exit(1);\
		}\
	}

// assumes integral eq
#define ASSERT_EQUALS(expression, eq)\
{\
	decltype(eq) r;\
	if ((r = (expression)) != (eq)) {\
		LOG_ERROR("%s != %s. Result: %d\n", #expression, #eq, r);\
		exit(1);\
	}\
}

namespace renderer {

class Logger {
public:
	static int get_format_length(const char* m, ...);
	static char* format_string(const char* m, ...);
	static char* vformat_string(const char* m, va_list args);

	static void log_info(const char* m, ...);
	static void print_info(const char* m, ...);
	static void log_warning(const char* m, ...);
	static void print_warning(const char* m, ...);
	static void log_error(const char* m, ...);
	static void print_error(const char* m, ...);

	static void clear_line() { printf(clear_command); }

	static const char* get_result_name(VkResult result);
private:
	static constexpr const char* error_prefix = "\033[38;2;255;0;0mERROR: \033[0m";
	static constexpr const char* warning_prefix = "\033[38;2;255;255;0mWARNING: \033[0m";
	static constexpr const char* info_prefix = "INFO: ";

	static constexpr const char* clear_command = "\033[2K\r";
};

} // namespace renderer
