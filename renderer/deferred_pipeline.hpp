#pragma once

#include <vulkan/vulkan.h>
#include <glm/fwd.hpp>

#include <simple_buffer.hpp>
#include <vector.hpp>
#include <memory.hpp>
#include <pipeline.hpp>
#include <sampler.hpp>

#include "world.hpp"
#include "gbuffer.hpp"

namespace renderer {
	class Device;
	class DescriptorPool;
	class CommandBuffer;
	class Sampler;
}

struct GBuffer;

class DeferredPipeline {
public:
	static VkSampleCountFlags allowed_samples;

	DeferredPipeline(renderer::Device* device,
	                 World* world,
	                 Vector<renderer::SharedDeviceBuffer>* abuffer_heads,
	                 Vector<renderer::SharedDeviceBuffer>* abuffers)
		: device{ device }
		, world{ world }
		, gbuffers{ new Vector<GBuffer>{} }
		, abuffer_heads{ abuffer_heads }
		, abuffers{ abuffers }
	{
		initialize();
	}

	void set_shading_mode(uint32_t mode) { shading_mode = mode; }

	void handle_resize();
	void draw(renderer::DescriptorPool& descriptor_pool, renderer::CommandBuffer& command_buffer, uint32_t image_index);
	void destroy_render_pass();

	SimpleBuffer<VkDescriptorPoolSize> get_descriptor_pool_sizes();
	uint32_t get_max_descriptor_sets();

	~DeferredPipeline();

private:
	renderer::Device* device;
	World* world;
	renderer::RenderPass* render_pass;
	renderer::Pipeline opaque_pipeline{};
	renderer::Pipeline transparent_pipeline{};
	renderer::Pipeline lighting_pipeline{};

	Vector<GBuffer>* gbuffers;
	renderer::Sampler depth_buffer{};
	renderer::Sampler color_target{};
	renderer::Sampler default_sampler{};

	renderer::SharedDeviceBuffer vertex_buffer{};

	Vector<renderer::SharedDeviceBuffer> common_uniform_memories{};
	Vector<renderer::SharedDeviceBuffer> per_object_uniform_memories{};
	Vector<renderer::SharedDeviceBuffer> descriptor_memories{};

	Vector<renderer::SharedDeviceBuffer>* abuffer_heads;
	Vector<renderer::SharedDeviceBuffer>* abuffers;

	uint32_t shading_mode = 0;
	float ambient = 0.01;

	VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_1_BIT;

	void initialize();
};
