#include <spng/spng.h>

#include "png.hpp"
#include "util.hpp"

namespace renderer {
namespace {

constexpr size_t memory_limit = 1024 * 1024 * 64;


void cleanup(spng_ctx* ctx)
{
	if (ctx) {
		spng_ctx_free(ctx);
	}
}

} // namespace

SimpleBuffer<uint8_t> load_png(const char* filepath, VkExtent2D& extent)
{
	auto file_data = File::read_whole_file(filepath);

	spng_ctx* ctx = spng_ctx_new(0);

	if (ctx == nullptr) {
		LOG_ERROR("Could not create spng context.\n");
		exit(1);
	}

	spng_set_crc_action(ctx, SPNG_CRC_USE, SPNG_CRC_USE);

	spng_set_chunk_limits(ctx, memory_limit, memory_limit);

	spng_set_png_buffer(ctx, file_data.get_buffer(), file_data.get_size());

	spng_ihdr ihdr;
	ASSERT_EQUALS(spng_get_ihdr(ctx, &ihdr), 0);

	spng_plte plte;
	{
		int result = spng_get_plte(ctx, &plte);
		if (result != SPNG_ECHUNKAVAIL)
			ASSERT_EQUALS(result, 0);
	}

	size_t image_size, image_width, image_height, width_size;

	int fmt = SPNG_FMT_RGBA8;

	ASSERT_EQUALS(spng_decoded_image_size(ctx, fmt, &image_size), 0);

	SimpleBuffer<uint8_t> image;
	image.resize(image_size);

	ASSERT_EQUALS(spng_decode_image(ctx, nullptr, 0, fmt, SPNG_DECODE_PROGRESSIVE), 0);

	image_height = ihdr.height;
	image_width = ihdr.width;
	width_size = image_size / image_height;

	spng_row_info row_info = {0};

	{
		int result = 0;
		do {
			result = spng_get_row_info(ctx, &row_info);

			if (result)
				break;

			result = spng_decode_row(ctx, image.get_buffer() + row_info.row_num * width_size, width_size);
		} while (result == 0);
		
		if (result != SPNG_EOI)
		{
			LOG_ERROR("png decode error: %s\n", spng_strerror(result));
		}
	}

	extent.width = image_width;
	extent.height = image_height;

	return move(image);
}

} // namespace renderer
