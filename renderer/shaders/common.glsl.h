#define FLAG_COLOR_TEXTURE 1
#define FLAG_NORMAL_TEXTURE (1 << 1)
#define FLAG_BLEND_MUL (1 << 2)

#define NODEFLAG_BLEND_MUL 1

#define HAS_FLAG(a, b) (((b) & a) == b)

#define MAX_POINT_LIGHTS 32
#define MAX_DIRECTIONAL_LIGHTS 32

#define EXPONENT 64.0
#define MODE_SHADED 0
#define MODE_POSITION 1
#define MODE_NORMAL 2
#define MODE_ALBEDO 3
#define MODE_DEPTH 4

#define PI 3.14159265358979323846

struct NodeData {
	uint color;
	uint nodeflags;
	f16vec2 normal;
	f16vec4 position;
};

#ifdef DEFERRED
# define ABUFFER_SET 0
# define ABUFFER_BINDING 1
layout (set = 0, binding = 0)
uniform CommonUniform {
	mat4 view;
	mat4 projection;
	uvec2 screen_size;
};

layout (set = 1, binding = 0)
uniform PerObjectUniform {
	mat4 model;
	mat4 modelview;
	mat4 mvp;

	vec4 color;
	float metallic;
	float roughness;
	uint flags;
};

#else
# define ABUFFER_SET 0
# define ABUFFER_BINDING 5
#endif

layout (set = ABUFFER_SET, binding = ABUFFER_BINDING)
buffer ABuffer {
	uint free_node;
	uint buffer_size;
	uint64_t nodebuffer[];
};

layout (set = ABUFFER_SET, binding = ABUFFER_BINDING + 1)
buffer AbufferData {
	NodeData data[];
};

#define DEPTH_MAX   0x0fffffff
#define DEPTH_MASK  0x0fffffff

uint encode_depth(float depth)
{
	return uint(depth * uint(DEPTH_MAX));
}

float decode_depth(uint depth)
{
	return float(depth) / float(uint(DEPTH_MAX));
}

// https://aras-p.info/texts/CompactNormalStorage.html#method03spherical
vec2 encode_normal(vec3 n) {
	float p = sqrt((n.z * 8.0) + 8.0);
	return (n.xy / p) + 0.5;
}

vec3 decode_normal(vec2 n) {
	vec2 fn = (n * 4.0) - 2.0;
	float f = dot(fn, fn);
	float g = sqrt(1.0 - (f / 4.0));
	vec3 o;
	o.xy = fn * g;
	o.z = 1.0 - (f / 2.0);
	return normalize(o);
}
