#version 450
#extension GL_KHR_memory_scope_semantics : enable
#extension GL_ARB_gpu_shader_int64 : enable
#extension GL_EXT_shader_atomic_int64 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable

#define DEFERRED

#ifdef VERTEX
# pragma shader_stage(vertex)

#include "common.glsl.h"

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

layout (location = 0) out vec3 out_position;
layout (location = 1) out vec3 out_normal;
layout (location = 2) out vec2 out_uv;
layout (location = 3) out mat4 out_tbn;

void main()
{
	gl_Position  = mvp * vec4(position, 1.0);
	out_position = (modelview * vec4(position, 1.0)).xyz;
	out_normal   = (modelview * vec4(normal, 0.0)).xyz;
	out_uv       = uv;

	out_tbn = modelview * mat4(mat3(
		normalize(tangent), 
		normalize(bitangent), 
		normalize(normal)
	));
}

#else
# pragma shader_stage(fragment)

#include "common.glsl.h"

# ifdef TRANSPARENT
layout (early_fragment_tests) in;
# endif

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;
layout (location = 3) in mat4 tbn;

layout (location = 0) out vec4 out_position;
layout (location = 1) out vec4 out_normal_metallic_roughness;
layout (location = 2) out vec4 out_albedo;

layout (set = 1, binding = 1) uniform sampler2D color_texture;
layout (set = 1, binding = 2) uniform sampler2D normal_texture;

uint64_t pack(uint depth, uint next)
{
	return uint64_t(uint64_t(depth) << 32) | uint64_t(next);
}

uint get_next(uint64_t node)
{
	return uint(node);
}

uint pack_color(vec4 c)
{
	c *= 255.0;
	return (uint(c.r) << 24) | (uint(c.g) << 16) | (uint(c.b) << 8) | uint(c.a);
}

bool write_to_abuffer(uint x, uint y, float depth, vec3 position, vec3 normal, vec4 albedo, uint flags)
{
	if (free_node >= buffer_size) {
		return false;
	}

	uint heads_end = screen_size.x * screen_size.y;
	uint head_idx = x + (y * screen_size.x);
	uint current_node_idx = atomicAdd(free_node, 1);

	nodebuffer[current_node_idx] = 0;
	memoryBarrier();

	data[current_node_idx - heads_end] = NodeData(
		pack_color(albedo),
		flags,
		f16vec2(normal.xy),
		f16vec4(position, normal.z)
	);

	uint64_t minimum = pack(encode_depth(depth), current_node_idx);
	uint64_t old = 0;
	uint pos = head_idx;

	uint i = 0;
	// i < 50 failsafe
	// this shouldn't be needed, but
	// it doesn't work on my laptop without it.
	while ((old = atomicMax(nodebuffer[pos], minimum)) > 0 && i < 50) {
		pos = get_next(max(old, minimum));
		minimum = min(minimum, old);
		i += 1;
	}


	return true;
}

vec3 srgb_to_linear(vec3 x)
{
	return pow(x, 1.0 / vec3(2.2));
}

void main() 
{
# ifdef TRANSPARENT
	// for now, do not use multisampling on transparent pixels
	if (gl_SampleID > 0)
		discard;
# endif

	vec4 c = color;
	if (HAS_FLAG(flags, FLAG_COLOR_TEXTURE)) {
		c = texture(color_texture, uv);
	}

	vec3 n;
	if (HAS_FLAG(flags, FLAG_NORMAL_TEXTURE)) {
		n = normalize((texture(normal_texture, uv).rgb * 2.0) - vec3(1.0));
		n = normalize((tbn * vec4(n, 0.0)).xyz);
		n = vec3(encode_normal(n), 0.0);
	} else {
		n = vec3(encode_normal(normalize(normal)), 0.0);
	}

# ifdef TRANSPARENT
	uint nflags = 0;
	if (HAS_FLAG(flags, FLAG_BLEND_MUL)) {
		nflags |= NODEFLAG_BLEND_MUL;
	}

	if (color.a < 1.0 && c.a < 1.0) {
		// write to abuffer
		if (write_to_abuffer(
			uint(gl_FragCoord.x), 
			uint(gl_FragCoord.y), 
			gl_FragCoord.z,
			position,
			n,
			c,
			nflags)
		) {
			discard;
		}
	} else {
		discard;
	}
# else
	if (color.a < 1.0 && c.a < 1.0) {
		discard;
	}
# endif

	out_position = vec4(position, 1.0);
	out_normal_metallic_roughness = vec4(n.xy, metallic, roughness);
	out_albedo   = vec4(c.rgb, 0.0);
}

#endif
