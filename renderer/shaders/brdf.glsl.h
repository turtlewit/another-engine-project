#define PI 3.14159265358979323846

vec3 blinn_phong(vec3 l, float dist, vec3 color, float power, vec3 position, vec3 n)
{
	vec3 o = -normalize(position);
	vec3 h = normalize(l + o);

	float use_specular = clamp(ceil(dot(n, l)), 0.0, 1.0);

	return (color * max(dot(l, n), 0.0) * (power / dist)) 
		+ (vec3(1.0) * pow(max(dot(n, h), 0.0), EXPONENT) * (power / dist));
}

float trowbridge_reitz(vec3 n, vec3 h, float a)
{
	float a2 = a * a;

	float nh2 = max(dot(n, h), 0.0);
	nh2 = nh2 * nh2;

	float interm = (nh2 * (a2 - 1.0)) + 1.0;

	return a2 / (PI * interm * interm);
}

float schlick(vec3 n, vec3 v, float k)
{
	float nv = max(dot(n, v), 0.0);
	return nv / ((nv * (1.0 - k)) + k);
}

float ak_direct(float a)
{
	float a1 = a + 1.0;
	return (a1 * a1) / 8.0;
}

float ak_environment(float a)
{
	return (a * a) / 2.0;
}

float schlick_ggx(vec3 n, vec3 v, vec3 l, float k)
{
	return schlick(n, v, k) * schlick(n, l, k);
}

vec3 schlick_fresnel(vec3 h, vec3 v, vec3 F0)
{
	float one_minus_hv = 1.0 - max(dot(h, v), 0.0);
	float one_minus_hv5 = one_minus_hv;
	for (int i = 0; i < 4; ++i) {
		one_minus_hv5 *= one_minus_hv;
	}

	return F0 + ((1.0 - F0) * one_minus_hv5);
}

vec3 calculate_F0(vec3 color, float metallic)
{
	return mix(vec3(0.04), color, metallic);
}

vec3 lambert(vec3 color)
{
	return color / PI;
}

vec3 cook_torrance(vec3 position, vec3 color, vec3 normal, float metallic, float roughness, vec3 light_direction, float light_distance, vec3 light_color, float attenuation)
{
	vec3 n = normalize(normal);
	vec3 v = -normalize(position);
	vec3 l = normalize(light_direction);
	vec3 h = normalize(l + v);

	vec3 wi = normalize((light_direction * light_distance) - position);
	float nwi = max(dot(n, wi), 0.0);
	vec3 radiance = light_color * attenuation * nwi;

	clamp(roughness, 0.001, 1.0);
	float a = roughness * roughness;

	float D = trowbridge_reitz(n, h, a);
	vec3 F = schlick_fresnel(h, v, calculate_F0(color, metallic));
	float G = schlick_ggx(n, v, l, ak_direct(a));

	vec3 kd = vec3(1.0) - F;
	kd *= 1.0 - metallic;

	vec3 specular = (D * F * G) / max(4.0 * max(dot(n, v), 0.0) * max(dot(n, l), 0.0), 0.001);

	return ((kd * lambert(color)) + specular) * radiance * max(dot(n, l), 0.0);
}

