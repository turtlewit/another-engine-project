#version 450
#extension GL_ARB_gpu_shader_int64 : enable
#extension GL_EXT_shader_explicit_arithmetic_types_float16 : enable

#ifdef VERTEX
# pragma shader_stage(vertex)

layout (location = 0) in vec2 position;

layout (location = 0) out vec2 out_uv;


void main()
{
	gl_Position = vec4((position - 0.5) * 2.0, 0.0, 1.0);
	out_uv = position;
}

#else
# pragma shader_stage(fragment)

#include "common.glsl.h"
#include "brdf.glsl.h"

layout (constant_id = 0) const int NUM_SAMPLES = 8;
#define FULL_MASK ((1 << NUM_SAMPLES) - 1)

layout (location = 0) in vec2 uv;

layout (location = 0) out vec4 out_color;

struct PointLight {
	vec3 position;
	vec3 color;
	float power;
};

struct DirectionalLight {
	vec3 direction;
	vec3 color;
	float power;
	bool has_shadow;
};

struct DirectionalShadow {
	mat4 light_space;
	float bias;
};

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput gbuffer_position;
layout (input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput gbuffer_normal_metallic_roughness;
layout (input_attachment_index = 2, set = 0, binding = 2) uniform subpassInput gbuffer_albedo;

layout (set = 0, binding = 3) uniform LightingUniform {
	PointLight point_lights[MAX_POINT_LIGHTS];
	DirectionalLight directional_lights[MAX_DIRECTIONAL_LIGHTS];
	DirectionalShadow directional_shadows[MAX_DIRECTIONAL_LIGHTS];
	uvec2 screen_size;
	float ambient;
	uint mode;
};

layout (set = 0, binding = 4) uniform sampler2D directional_shadow_maps[MAX_DIRECTIONAL_LIGHTS];

uint get_next(uint64_t node)
{
	return uint(node);
}

uint get_depth_int(uint64_t node)
{
	return uint(node >> 32) & DEPTH_MASK;
}

vec4 unpack_color(uint c)
{
	vec4 o = vec4(
		float((c & 0xff000000) >> 24),
		float((c & 0x00ff0000) >> 16),
		float((c & 0x0000ff00) >> 8),
		float( c & 0x000000ff )
	);

	return o / 255.0;
}

// https://aras-p.info/texts/CompactNormalStorage.html#method03spherical
vec3 get_normal16(f16vec2 n, f16vec4 p)
{
	vec3 o;
	o.x = float(n.x);
	o.y = float(n.y);
	//o.z = float(p.w);
	o.z = sqrt(1+dot(o.xy, -o.xy));
	return o;
}

vec3 get_normal(vec2 n)
{
	vec3 o;
	o.x = n.x;
	o.y = n.y;
	o.z = sqrt(1-dot(o.xy, o.xy));
	//if (o.z < 0) {
	//	o.z *= -1;
	//}
	return o;
}

float sample_shadow(int iter, vec2 uv, float depth, float bias, int x, int y)
{
	uv.x += (float(x) - 1.5) * (1.0 / 16738.0) * 4.0 * depth;
	uv.y += (float(y) - 1.5) * (1.0 / 16738.0) * 4.0 * depth;
	float light_depth = texture(directional_shadow_maps[iter], uv).r;
	return float((depth - bias) <= light_depth);
}

vec3 calculate_point_light(vec3 position, vec3 albedo, vec3 normal, float metallic, float roughness, PointLight light)
{
	vec3 light_direction = normalize(light.position - position);
	float light_distance = length(light.position - position);
	vec3 light_color = light.color * light.power;
	float attenuation = pow(light_distance, 2.0);

	return cook_torrance(position, albedo, normal, metallic, roughness, light_direction, light_distance, light_color, attenuation);
}

vec3 calculate_directional_light(vec3 position, vec3 albedo, vec3 normal, float metallic, float roughness, DirectionalLight light, int iter)
{
	float shadow_factor = 1.0f;
	if (light.has_shadow) {
		// shadow test
		shadow_factor = 0.0f;
		DirectionalShadow shadow = directional_shadows[iter];
		vec4 light_space_clip = (shadow.light_space * vec4(position, 1.0));
		vec3 light_space_ndc = light_space_clip.xyz / light_space_clip.w;
		vec2 light_uv = (light_space_ndc.xy + 1.0) / 2.0;
		shadow_factor = 0.0f;
		for (int y = 0; y < 4; ++y) {
			for (int x = 0; x < 4; ++x) {
				shadow_factor += sample_shadow(iter, light_uv, light_space_ndc.z, shadow.bias, x, y);
			}
		}
		shadow_factor /= 16.0f;
	}

	vec3 light_direction = -normalize(light.direction);
	float light_distance = 1.0f;
	vec3 light_color = light.color * light.power;
	float attenuation = 1.0f;

	return cook_torrance(position, albedo, normal, metallic, roughness, light_direction, light_distance, light_color, attenuation) * shadow_factor;
}

void main()
{
	vec3 avg_color = vec3(0.0);
	vec3 avg_position = vec3(0.0);
	vec3 avg_normal = vec3(0.0);
	vec3 avg_albedo = vec3(0.0);

	vec3 a = vec3(1.0) * ambient;

	vec3 position = subpassLoad(gbuffer_position).rgb;
	float depth = subpassLoad(gbuffer_position).a;
	vec3 n;
	n = normalize(decode_normal(subpassLoad(gbuffer_normal_metallic_roughness).rg));
	vec3 albedo = subpassLoad(gbuffer_albedo).rgb;
	float metallic = subpassLoad(gbuffer_normal_metallic_roughness).b;
	float roughness = subpassLoad(gbuffer_normal_metallic_roughness).a;

	vec3 final_color = vec3(0.0);

	final_color += a * albedo;

	for (int iter = 0; iter < MAX_POINT_LIGHTS; ++iter) {
		PointLight light = point_lights[iter];
		if (light.power == 0.0)
			break;

		final_color += calculate_point_light(position, albedo, n, metallic, roughness, light);

		final_color = max(final_color, vec3(0.0f));
	}

	for (int iter = 0; iter < MAX_DIRECTIONAL_LIGHTS; ++iter) {
		DirectionalLight light = directional_lights[iter];

		if (light.power == 0.0)
			break;

		final_color += calculate_directional_light(position, albedo, n, metallic, roughness, light, iter);

		final_color = max(final_color, vec3(0.0f));
	}

	avg_color += final_color;
	avg_position += position;
	avg_normal += n;
	avg_albedo += albedo;

	uint head_idx = uint(gl_FragCoord.x) + (uint(gl_FragCoord.y) * screen_size.x);
	uint64_t node = nodebuffer[head_idx];
	uint pos = get_next(node);
	while (node > 0) {
		float ndepth = decode_depth(get_depth_int(node));
		uint data_idx = pos - (screen_size.x * screen_size.y);

		vec3 np = vec3(data[data_idx].position.xyz);
		vec3 nn = normalize(decode_normal(get_normal16(data[data_idx].normal, data[data_idx].position).xy));
		vec4 nc = unpack_color(data[data_idx].color);
		uint flags = data[data_idx].nodeflags;

		//if (ndepth < depth) {
		if (true) {

			vec3 c = vec3(0.0);
			for (int iter = 0; iter < MAX_POINT_LIGHTS; ++iter) {
				PointLight light = point_lights[iter];

				if (light.power == 0.0)
					break;

				c += calculate_point_light(np, nc.rgb, nn, 0.0, 1.0, light);
				c = max(c, vec3(0.0f));
			}

			for (int iter = 0; iter < MAX_DIRECTIONAL_LIGHTS; ++iter) {
				DirectionalLight light = directional_lights[iter];

				if (light.power == 0.0)
					break;

				c += calculate_directional_light(np, nc.rgb, nn, 0.0, 1.0, light, iter);
				c = max(c, vec3(0.0f));
			}

			c += a * nc.rgb;

			if (HAS_FLAG(flags, NODEFLAG_BLEND_MUL)) {
				avg_color = avg_color * (nc.rgb * (1.0 - nc.a));
				//avg_color = (avg_color * (1.0 - nc.a)) + (c * nc.a);
			} else {
				avg_color = (avg_color * (1.0 - nc.a)) + (c * nc.a);
			}

			avg_normal = ((normalize(nn) + 1.0) / 2.0);
			avg_albedo = (avg_albedo * (1.0 - nc.a)) + (nc.rgb * nc.a);
			avg_position = (avg_position * (1.0 - nc.a)) + (np.rgb * nc.a);
		}

		node = nodebuffer[pos];
		pos = get_next(node);
	}

	//out_color = vec4(avg_color, 1.0);
	switch(mode){
		case MODE_SHADED:
			out_color = vec4(avg_color, 1.0);
			break;
		case MODE_POSITION:
			out_color = vec4(avg_position, 1.0);
			break;
		case MODE_NORMAL:
			out_color = vec4(avg_normal, 1.0);
			break;
		case MODE_ALBEDO:
			out_color = vec4(avg_albedo, 1.0);
			break;
		//case MODE_DEPTH:
		//	out_color = vec4(vec3(1.0 - final_depth), 1.0);
	}
}

#endif
