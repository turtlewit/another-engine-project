#pragma once

#include <simple_buffer.hpp>

#include <vulkan/vulkan.h>

#include "surface.hpp"

struct SDL_Window;

namespace renderer {

class Instance;

class SurfaceProvider {
public:
	Surface create_surface(Instance* instance);
	SimpleBuffer<const char*> get_instance_extensions();
	VkExtent2D get_extent();

// SDL implementation
public:
	SurfaceProvider(SDL_Window* window) : window{ window }
	{
	}
private:
	SDL_Window* window;
//
};

} // namespace renderer
