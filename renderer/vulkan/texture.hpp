#pragma once

#include <vulkan/vulkan.h>

#include "memory.hpp"
#include "image.hpp"

namespace renderer {

class Device;

class Texture {
public:
	static Vector<Texture> create_simple_textures(Device* device, const Vector<Image>& images);

	Image get_image() const { return image; }
	VkImageView get_image_view() const { return view; }
	VkSampler get_sampler() const { return sampler; }

	Texture(Device* device, VkImage image, VkImageView view, SharedDeviceMemory memory, VkSampler sampler)
		: device{ device }, image{ image }, view{ view }, memory{ memory }, sampler{ sampler }
	{
	}

	~Texture();


private:
	Device* device;
	Image image;
	VkImageView view;
	SharedDeviceMemory memory;
	VkSampler sampler;
};

} // namespace renderer
