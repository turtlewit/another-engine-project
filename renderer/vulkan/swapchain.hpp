#pragma once

#include <vulkan/vulkan.h>
#include <vector.hpp>

#include "image.hpp"
#include "synchronization.hpp"
#include "util.hpp"

namespace renderer {

class Device;
class Surface;

class Swapchain {
public:
	static Swapchain create_swapchain(Device* device, Surface* surface, uint32_t max_frames_in_flight);

	static void set_preferred_present_mode(VkPresentModeKHR preferred_mode);

	VkSwapchainKHR get_inner() const { return swapchain; }
	VkExtent2D get_extent() const { return extent; }
	VkSurfaceFormatKHR get_surface_format() const { return surface_format; }
	const Vector<Image>& get_images() const { return images; }
	const Vector<VkImageView>& get_image_views() const { return image_views; }
	uint32_t get_image_count() const { return u32(images.get_size()); }
	uint32_t get_max_frames_in_flight() const { return max_frames_in_flight; }
	uint32_t get_current_image_index() const { return current_image_index; }
	VkViewport& get_viewport() { return viewport; }
	VkRect2D& get_scissor() { return scissor; }

	VkResult acquire_next_image(VkSemaphore signal_semaphore = VK_NULL_HANDLE);
	VkResult present(VkSemaphore wait_semaphore);

	Swapchain(Device* device, 
	          Surface* surface, 
	          VkSwapchainKHR swapchain, 
	          VkExtent2D extent, 
		  VkViewport viewport,
		  VkRect2D scissor,
	          VkSurfaceFormatKHR surface_format, 
		  uint32_t max_frames_in_flight,
	          Vector<Image>& images,
		  Vector<VkImageView>& image_views);

	~Swapchain();

private:
	Device* device;
	Surface* surface;
	VkSwapchainKHR swapchain;
	VkExtent2D extent;
	VkViewport viewport;
	VkRect2D scissor;
	VkSurfaceFormatKHR surface_format;
	uint32_t max_frames_in_flight;
	Vector<Image> images;
	Vector<VkImageView> image_views;

	uint32_t current_image_index;
};

} // namespace renderer
