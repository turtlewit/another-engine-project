#pragma once

namespace renderer {

struct Vertex2D {
	float x, y;
};

struct Tri2D {
	Vertex2D vertices[3];
};

struct Quad2D {
	Tri2D tris[2];
};

constexpr Quad2D UNIT_QUAD = Quad2D{
	{
		Tri2D{{ { 0.0f, 0.0f }, { 1.0f, 0.0f }, { 1.0f, 1.0f } }}, // TL, TR, BL
		Tri2D{{ { 1.0f, 1.0f }, { 0.0f, 1.0f }, { 0.0f, 0.0f } }}, // TR, BR, BL
	}
};

} // namespace renderer
