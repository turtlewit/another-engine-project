#include "synchronization.hpp"
#include "device.hpp"

namespace renderer {

Fence Fence::create_fence(Device* device, VkFenceCreateFlags flags)
{
	VkFenceCreateInfo fence_create_info{};
	fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fence_create_info.flags = flags;
	
	VkFence fence;
	vkCreateFence(device->get_inner(), &fence_create_info, nullptr, &fence);

	return Fence(device, fence);
}

void Fence::wait_for_fences(Device* device, const Vector<VkFence>& fences, VkBool32 wait_all, uint64_t timeout)
{
	vkWaitForFences(device->get_inner(), fences.get_size(), fences.get_buffer(), wait_all, timeout);
}

void Fence::wait(uint64_t timeout)
{
	vkWaitForFences(device->get_inner(), 1, &fence, VK_TRUE, timeout);
}

void Fence::reset()
{
	vkResetFences(device->get_inner(), 1, &fence);
}

Fence::~Fence()
{
	if (fence != VK_NULL_HANDLE)
		vkDestroyFence(device->get_inner(), fence, nullptr);
}

Semaphore Semaphore::create_semaphore(Device* device)
{
	VkSemaphoreCreateInfo semaphore_create_info{};
	semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkSemaphore semaphore;
	vkCreateSemaphore(device->get_inner(), &semaphore_create_info, nullptr, &semaphore);

	return Semaphore(device, semaphore);
}

Semaphore::~Semaphore()
{
	if (semaphore != VK_NULL_HANDLE)
		vkDestroySemaphore(device->get_inner(), semaphore, nullptr);
}

} // namespace renderer
