#pragma once

#include <vulkan/vulkan.h>

#include "simple_buffer.hpp"

namespace renderer {

class Device;
class CommandBuffer;
class Semaphore;
class Fence;
class Swapchain;

class DeviceQueue {
public:
	static DeviceQueue get_device_queue(Device* device, uint32_t queue_family, uint32_t queue_index);

	VkQueue get_inner() { return queue; }

	VkResult submit(const SimpleBuffer<VkCommandBuffer>& command_buffers, const SimpleBuffer<VkSemaphore>& wait_semaphores = SimpleBuffer<VkSemaphore>(), const SimpleBuffer<VkPipelineStageFlags>& wait_stages = SimpleBuffer<VkPipelineStageFlags>(), const SimpleBuffer<VkSemaphore>& signal_semaphores = SimpleBuffer<VkSemaphore>(), VkFence fence = VK_NULL_HANDLE);
	VkResult present(const SimpleBuffer<VkSemaphore>& wait_semaphores, const SimpleBuffer<VkSwapchainKHR>& swapchains, const SimpleBuffer<uint32_t>& image_indices);
	void wait();

	DeviceQueue(VkQueue queue)
		: queue{ queue }
	{
	}

private:
	VkQueue queue;
};

} // namespace renderer
