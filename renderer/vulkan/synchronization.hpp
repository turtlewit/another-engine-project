#pragma once

#include <vulkan/vulkan.h>
#include <vector.hpp>

namespace renderer {

class Device;

class Fence {
public:
	static Fence create_fence(Device* device, VkFenceCreateFlags flags = 0);

	static void wait_for_fences(Device* device, const Vector<VkFence>& fences, VkBool32 wait_all = VK_TRUE, uint64_t timeout = UINT64_MAX);

	template<typename... Args>
	static void wait_for_fences(Fence& fence, Args&... fences)
	{
		wait_for_fences(VK_TRUE, UINT64_MAX, fence, fences...);
	}

	template<typename... Args>
	static void wait_for_fences(VkBool32 wait_all, uint64_t timeout, Fence& fence, Args&... fences)
	{
		Vector<VkFence> vk_fences{ fence.fence, fences.fence... };
		wait_for_fences(fence.device, vk_fences, wait_all, timeout);
	}

	VkFence get_inner() { return fence; }
	void wait(uint64_t timeout = UINT64_MAX);
	void reset();

	Fence(Device* device, VkFence fence)
		: device{ device }, fence{ fence }
	{
	}

	Fence(Fence&& other)
		: device{ other.device }, fence{ other.fence }
	{
		other.fence = VK_NULL_HANDLE;
	}

	~Fence();

private:
	Device* device;
	VkFence fence;
};

class Semaphore {
public:
	static Semaphore create_semaphore(Device* device);

	VkSemaphore get_inner() { return semaphore; }

	Semaphore(Device* device, VkSemaphore semaphore)
		: device{ device }, semaphore{ semaphore }
	{
	}

	Semaphore(Semaphore&& other)
		: device{ other.device }, semaphore{ other.semaphore }
	{
		other.semaphore = VK_NULL_HANDLE;
	}

	~Semaphore();

private:
	Device* device;
	VkSemaphore semaphore;
};

} // namespace renderer
