#pragma once

#include <vulkan/vulkan.h>

namespace renderer {

class Device;

struct Image {
	VkImageView create_simple_image_view(Device* device) const;

	VkImage image;
	VkFormat format;
};

} // namespace renderer
