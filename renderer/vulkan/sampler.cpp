#include "device.hpp"
#include "memory.hpp"
#include "sampler.hpp"

namespace renderer {

SamplerBuilder& SamplerBuilder::with_extent_2d(uint32_t width, uint32_t height)
{
	image_create_info.imageType = VK_IMAGE_TYPE_2D;
	image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	image_create_info.extent.width = width;
	image_create_info.extent.height = height;
	image_create_info.extent.depth = 1;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_format(VkFormat format)
{
	image_create_info.format = format;
	image_view_create_info.format = format;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_tiling(VkImageTiling tiling)
{
	image_create_info.tiling = tiling;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_initial_layout(VkImageLayout layout)
{
	image_create_info.initialLayout = layout;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_usage(VkImageUsageFlags flags)
{
	image_create_info.usage = flags;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_sharing_mode(VkSharingMode mode)
{
	image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_samples(VkSampleCountFlagBits flags)
{
	image_create_info.samples = flags;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_aspect(VkImageAspectFlagBits flags)
{
	image_view_create_info.subresourceRange.aspectMask = flags;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_min_mag_filter(VkFilter min_filter, VkFilter mag_filter)
{
	sampler_create_info.minFilter = min_filter;
	sampler_create_info.magFilter = mag_filter;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_address_mode(VkSamplerAddressMode mode)
{
	sampler_create_info.addressModeU = mode;
	sampler_create_info.addressModeV = mode;
	sampler_create_info.addressModeW = mode;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_anisotropy(float max_samples)
{
	sampler_create_info.anisotropyEnable = VK_TRUE;
	sampler_create_info.maxAnisotropy = max_samples;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_border_color(VkBorderColor color)
{
	sampler_create_info.borderColor = color;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_unnormalized_coordinates()
{
	sampler_create_info.unnormalizedCoordinates = VK_TRUE;

	return *this;
}

SamplerBuilder& SamplerBuilder::with_mip_mapping()
{
	uint32_t mip_levels = u32(floor(log2(f64(max(image_create_info.extent.width, image_create_info.extent.height))))) + 1;
	image_create_info.mipLevels = mip_levels;
	image_view_create_info.subresourceRange.levelCount = mip_levels;
	sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	sampler_create_info.maxLod = f32(mip_levels);
	image_create_info.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	return *this;
}

Sampler SamplerBuilder::build(Device* device)
{
	VkPhysicalDeviceProperties properties{};
	vkGetPhysicalDeviceProperties(device->get_physical_device(), &properties);
	if (sampler_create_info.anisotropyEnable == VK_TRUE) {
		sampler_create_info.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
	}

	VkFormat format = image_create_info.format;

	VkImage image;
	vkCreateImage(device->get_inner(), &image_create_info, nullptr, &image);

	SharedDeviceMemory memory = Memory::allocate_image_memory(device, image, format);

	image_view_create_info.image = image;
	
	VkImageView image_view;
	vkCreateImageView(device->get_inner(), &image_view_create_info, nullptr, &image_view);
	
	VkSampler sampler;
	vkCreateSampler(device->get_inner(), &sampler_create_info, nullptr, &sampler);

	return Sampler(device, memory, image, format, image_view, sampler, image_view_create_info.subresourceRange, image_create_info.extent);
}

SamplerBuilder::SamplerBuilder()
	: image_create_info{}
	, image_view_create_info{}
	, sampler_create_info{}
{
	image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image_create_info.imageType = VK_IMAGE_TYPE_2D;
	image_create_info.mipLevels = 1;
	image_create_info.arrayLayers = 1;
	image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
	image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
	image_create_info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	//image_create_info.queueFamilyIndexCount = 
	//image_create_info.pQueueFamilyIndices =
	image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	image_view_create_info.components = {
		.r = VK_COMPONENT_SWIZZLE_IDENTITY,
		.g = VK_COMPONENT_SWIZZLE_IDENTITY,
		.b = VK_COMPONENT_SWIZZLE_IDENTITY,
		.a = VK_COMPONENT_SWIZZLE_IDENTITY,
	};
	image_view_create_info.subresourceRange = {
		.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
		.baseMipLevel   = 0,
		.levelCount     = 1,
		.baseArrayLayer = 0,
		.layerCount     = 1,
	};

	sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	sampler_create_info.magFilter = VK_FILTER_LINEAR;
	sampler_create_info.minFilter = VK_FILTER_LINEAR;
	sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	sampler_create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	sampler_create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	sampler_create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	sampler_create_info.mipLodBias = 0;
	sampler_create_info.anisotropyEnable = VK_TRUE;
	sampler_create_info.maxAnisotropy = 1.0f;
	sampler_create_info.compareEnable = VK_FALSE;
	sampler_create_info.compareOp = VK_COMPARE_OP_NEVER;
	sampler_create_info.minLod = 0;
	sampler_create_info.maxLod = 0;
	sampler_create_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	sampler_create_info.unnormalizedCoordinates = VK_FALSE;
}

void Sampler::copy_to_image(size_t size, void* data, VkImageLayout final_layout, VkAccessFlags final_access_mask)
{
	Memory::transition_image_layout(
		device,
		*this,
		VK_IMAGE_LAYOUT_UNDEFINED,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		0,
		VK_ACCESS_TRANSFER_WRITE_BIT
	);

	Memory::copy_to_image_staged(
		device,
		image,
		{
			.aspectMask = subresource_range.aspectMask,
			.mipLevel = 0,
			.baseArrayLayer = 0,
			.layerCount = subresource_range.layerCount,
		},
		extent,
		size,
		data);
	
	if (subresource_range.levelCount > 1) {
		uint32_t mip_levels = u32(floor(log2(f64(max(extent.width, extent.height))))) + 1;
		// generate mipmaps
		CommandBuffer command_buffer = device->get_transfer_pool().begin_single_use_command_buffer();

		VkImageMemoryBarrier barrier {
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.image = image,
			.subresourceRange = VkImageSubresourceRange {
				.aspectMask = subresource_range.aspectMask,
				.levelCount = 1,
				.baseArrayLayer = subresource_range.baseArrayLayer,
				.layerCount = subresource_range.layerCount,
			},
		};

		int32_t mip_width = i32(extent.width);
		int32_t mip_height = i32(extent.height);

		for (uint32_t i = 1; i < mip_levels; ++i) {
			barrier.subresourceRange.baseMipLevel = i - 1;
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

			vkCmdPipelineBarrier(
				command_buffer.get_inner(),
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &barrier
			);

			int32_t new_mip_width = mip_width > 1 ? mip_width / 2 : 1;
			int32_t new_mip_height = mip_height > 1 ? mip_height / 2 : 1;

			VkImageBlit blit{
				.srcSubresource = {
					.aspectMask = subresource_range.aspectMask,
					.mipLevel = i - 1,
					.baseArrayLayer = 0,
					.layerCount = subresource_range.layerCount,
				},
				.srcOffsets = { { 0, 0, 0 }, { mip_width, mip_height, 1 } },
				.dstSubresource = {
					.aspectMask = subresource_range.aspectMask,
					.mipLevel = i,
					.baseArrayLayer = 0,
					.layerCount = subresource_range.layerCount,
				},
				.dstOffsets = { { 0, 0, 0 }, { new_mip_width, new_mip_height, 1 } },
			};

			vkCmdBlitImage(
				command_buffer.get_inner(),
				image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
				image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1, &blit,
				VK_FILTER_LINEAR
			);

			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.newLayout = final_layout;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			barrier.dstAccessMask = final_access_mask;

			vkCmdPipelineBarrier(
				command_buffer.get_inner(),
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
				0,
				0, nullptr,
				0, nullptr,
				1, &barrier
			);

			mip_width = new_mip_width;
			mip_height = new_mip_height;
		}

		barrier.subresourceRange.baseMipLevel = mip_levels - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = final_layout;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		barrier.dstAccessMask = final_access_mask;

		vkCmdPipelineBarrier(
			command_buffer.get_inner(),
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
			0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

		command_buffer.end();
		device->get_graphics_queue().submit({command_buffer.get_inner()});
		device->get_graphics_queue().wait();
	} else {

		Memory::transition_image_layout(
			device,
			*this,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			final_layout,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			final_access_mask
		);
	}
}

Sampler::~Sampler()
{
	if (device == nullptr)
		return;
	vkDestroySampler(device->get_inner(), sampler, nullptr);
	vkDestroyImageView(device->get_inner(), image_view, nullptr);
	vkDestroyImage(device->get_inner(), image, nullptr);
}

} // namespace renderer
