#pragma once

#include <vulkan/vulkan.h>

namespace renderer {

class Instance;

class Surface {
public:
	VkSurfaceKHR get_inner() { return surface; }
	Instance* get_instance() { return instance; }
	VkExtent2D get_extent() { return extent; }

	Surface(Instance* instance, VkSurfaceKHR surface, VkExtent2D extent)
		: instance{ instance }, surface{ surface }, extent{ extent }
	{
	}

	~Surface();
private:
	Instance* instance;
	VkSurfaceKHR surface;
	VkExtent2D extent;
};

} // namespace renderer
