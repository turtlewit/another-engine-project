#include <vulkan/vulkan.h>
#include <cstdlib>

#include "logger.hpp"
#include "instance.hpp"

namespace renderer {

SimpleBuffer<VkPhysicalDevice> Instance::enumerate_physical_devices()
{
	uint32_t physical_device_count;

	ASSERT_VK_SUCCESS(vkEnumeratePhysicalDevices(instance, &physical_device_count, nullptr));

	if (physical_device_count == 0) {
		LOG_ERROR("No physical device found. Please make sure your graphics drivers support Vulkan.\n");
		exit(1);
	}

	SimpleBuffer<VkPhysicalDevice> physical_devices;
	physical_devices.resize(physical_device_count);

	ASSERT_VK_SUCCESS(vkEnumeratePhysicalDevices(instance, &physical_device_count, physical_devices.get_buffer()));

	return physical_devices;
}

Instance Instance::create_instance(SurfaceProvider& sp, uint32_t max_frames_in_flight)
{
	SimpleBuffer<const char*> surface_instance_extensions = sp.get_instance_extensions();
	VkInstance instance = create_instance_internal(surface_instance_extensions);
	return Instance(instance, sp, max_frames_in_flight);
}

VkInstance Instance::create_instance_internal(const SimpleBuffer<const char*>& surface_instance_extensions)
{
	uint32_t api_version;
	vkEnumerateInstanceVersion(&api_version);

	Logger::log_info("Returned Vulkan Instance Version: %d.%d.%d\n", VK_VERSION_MAJOR(api_version), VK_VERSION_MINOR(api_version), VK_VERSION_PATCH(api_version));

	if (api_version < VK_MAKE_VERSION(1, 2, 0)) {
		LOG_WARNING("Vulkan instance version is less than 1.2. App may crash...\n");
	}

	api_version = VK_MAKE_VERSION(1, 2, 0);

	VkApplicationInfo application_info{};
	application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	application_info.pApplicationName = "ECS Renderer"; // TODO Application Name
	application_info.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
	application_info.pEngineName = "ECS Engine Project"; // TODO Engine Name
	application_info.engineVersion = VK_MAKE_VERSION(0, 0, 1);
	application_info.apiVersion = api_version;

	VkInstanceCreateInfo instance_create_info{};
	instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_create_info.pApplicationInfo = &application_info;
	instance_create_info.enabledExtensionCount = surface_instance_extensions.get_size();
	instance_create_info.ppEnabledExtensionNames = surface_instance_extensions.get_buffer();

	Logger::log_info("Instance extensions:\n");
	for (const char* const* extension = surface_instance_extensions.get_buffer(); extension != surface_instance_extensions.get_buffer() + surface_instance_extensions.get_size(); ++extension) {
		Logger::print_info("      %s\n", *extension);
	}

	VkInstance vk_instance;
	ASSERT_VK_SUCCESS(vkCreateInstance(&instance_create_info, nullptr, &vk_instance));

	return vk_instance;
}

Instance::~Instance() 
{
	delete device;
	delete surface;

	vkDestroyInstance(instance, nullptr);
}

} // namespace renderer
