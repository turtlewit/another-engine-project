#include "device_queue.hpp"
#include "device.hpp"
#include "command_buffer.hpp"
#include "synchronization.hpp"
#include "swapchain.hpp"

namespace renderer {

DeviceQueue DeviceQueue::get_device_queue(Device* device, uint32_t queue_family, uint32_t queue_index)
{
	VkQueue queue;
	vkGetDeviceQueue(device->get_inner(), queue_family, queue_index, &queue);

	return DeviceQueue(queue);
}

VkResult DeviceQueue::submit(const SimpleBuffer<VkCommandBuffer>& command_buffers, const SimpleBuffer<VkSemaphore>& wait_semaphores, const SimpleBuffer<VkPipelineStageFlags>& wait_stages, const SimpleBuffer<VkSemaphore>& signal_semaphores, VkFence fence)
{
	VkSubmitInfo submit_info{};
	submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submit_info.waitSemaphoreCount = wait_semaphores.get_size();
	submit_info.pWaitSemaphores = wait_semaphores.get_buffer();
	submit_info.pWaitDstStageMask = wait_stages.get_buffer();
	submit_info.commandBufferCount = command_buffers.get_size();
	submit_info.pCommandBuffers = command_buffers.get_buffer();;
	submit_info.signalSemaphoreCount = signal_semaphores.get_size();
	submit_info.pSignalSemaphores = signal_semaphores.get_buffer();

	return vkQueueSubmit(queue, 1, &submit_info, fence);
}

VkResult DeviceQueue::present(const SimpleBuffer<VkSemaphore>& wait_semaphores, const SimpleBuffer<VkSwapchainKHR>& swapchains, const SimpleBuffer<uint32_t>& image_indices)
{
	VkPresentInfoKHR present_info{};
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.waitSemaphoreCount = wait_semaphores.get_size();
	present_info.pWaitSemaphores = wait_semaphores.get_buffer();
	present_info.swapchainCount = swapchains.get_size();
	present_info.pSwapchains = swapchains.get_buffer();
	present_info.pImageIndices = image_indices.get_buffer();

	return vkQueuePresentKHR(queue, &present_info);
}

void DeviceQueue::wait()
{
	vkQueueWaitIdle(queue);
}

} // namespace renderer
