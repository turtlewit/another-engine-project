#pragma once

#include <vulkan/vulkan.h>

#include <vector.hpp>
#include <simple_buffer.hpp>
#include "util.hpp"

namespace renderer {

class Device;
class Swapchain;

enum class RenderPassAttachmentType {
	INPUT,
	COLOR,
	RESOLVE,
	DEPTH_STENCIL,
};

struct RenderPassAttachment {
	static RenderPassAttachment create_swapchain_color_attachment(Swapchain* swapchain);
	static RenderPassAttachment create_color_attachment(
			VkFormat format, 
			const Vector<VkImageView>& image_views, 
			VkImageLayout initial_layout, 
			VkImageLayout final_layout, 
			VkBool32 may_alias = VK_FALSE,
			VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_1_BIT,
			VkAttachmentLoadOp load_op = VK_ATTACHMENT_LOAD_OP_CLEAR, 
			VkAttachmentStoreOp store_op = VK_ATTACHMENT_STORE_OP_STORE);

	static RenderPassAttachment create_depth_attachment(
			VkFormat format, 
			VkImageView image_view, 
			VkImageLayout initial_layout, 
			VkImageLayout final_layout, 
			VkBool32 may_alias = VK_FALSE,
			VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_1_BIT,
			VkAttachmentLoadOp load_op = VK_ATTACHMENT_LOAD_OP_CLEAR, 
			VkAttachmentStoreOp store_op = VK_ATTACHMENT_STORE_OP_DONT_CARE);

	static RenderPassAttachment create_input_attachment(
			VkFormat format, 
			const Vector<VkImageView>& image_views, 
			VkImageLayout initial_layout, 
			VkImageLayout final_layout, 
			VkBool32 may_alias = VK_FALSE,
			VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_1_BIT,
			VkAttachmentLoadOp load_op = VK_ATTACHMENT_LOAD_OP_CLEAR, 
			VkAttachmentStoreOp store_op = VK_ATTACHMENT_STORE_OP_DONT_CARE);

	VkAttachmentDescription attachment_description;
	Vector<VkImageView> image_views;
	RenderPassAttachmentType type;
	bool preserve = false;
};

enum class RenderPassType {
	PRIMARY,
};

class RenderPass {
public:
	Device* get_device() const { return device; }
	VkRenderPass get_inner() const { return render_pass; }
	const Vector<VkFramebuffer>& get_framebuffers() const { return framebuffers; }
	Vector<VkClearValue>& get_clear_values() { return clear_values; }
	RenderPassType get_type() { return type; }

	RenderPass(Device* device, VkRenderPass render_pass, RenderPassType type, Vector<VkFramebuffer>& framebuffers, Vector<VkClearValue>& clear_values)
		: device{ device }, render_pass{ render_pass }, type{ type }, framebuffers{ move(framebuffers) }
		, clear_values{ move(clear_values) }
	{
	}

	~RenderPass();

private:
	Device* device;
	VkRenderPass render_pass;
	RenderPassType type;
	Vector<VkFramebuffer> framebuffers;
	Vector<VkClearValue> clear_values;
};

struct Subpass {
	VkSubpassDescription subpass_description;
	Vector<VkAttachmentReference> input_attachment_references;
	Vector<VkAttachmentReference> depth_attachment_references;
	Vector<VkAttachmentReference> color_attachment_references;
	Vector<VkAttachmentReference> resolve_attachment_references;
};

class RenderPassBuilder {
public:
	RenderPassBuilder() : attachments{}, subpasses{}, dependencies{}, clear_values{} {}

	RenderPassBuilder& with_subpasses(size_t num);
	RenderPassBuilder& with_attachment(RenderPassAttachment attachment);
	RenderPassBuilder& with_depth_attachment(RenderPassAttachment attachment);
	RenderPassBuilder& with_input_attachment_reference(uint32_t subpass, VkAttachmentReference attachment);
	RenderPassBuilder& with_color_attachment_reference(uint32_t subpass, VkAttachmentReference attachment);
	RenderPassBuilder& with_depth_attachment_reference(uint32_t subpass, VkAttachmentReference attachment);
	RenderPassBuilder& with_resolve_attachment_reference(uint32_t subpass, VkAttachmentReference attachment);
	RenderPassBuilder& with_dependency(VkSubpassDependency dependency);

	RenderPass build(Device* device);
	RenderPass build(Device* device, uint32_t framebuffer_width, uint32_t framebuffer_height, uint32_t framebuffer_count);
private:
	Vector<RenderPassAttachment> attachments;
	Vector<Subpass> subpasses;
	Vector<VkSubpassDependency> dependencies;
	Vector<VkClearValue> clear_values;
};

} // namespace renderer
