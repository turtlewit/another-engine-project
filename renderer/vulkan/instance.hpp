#pragma once

#include <vulkan/vulkan.h>

#include "device.hpp"
#include "surface_provider.hpp"

namespace renderer {

class Instance {
public:
	static Instance create_instance(SurfaceProvider& sp, uint32_t max_frames_in_flight = 1);

	VkInstance get_inner() { return instance; }
	Surface* get_surface() { return surface; }
	Device* get_device() { return device; }
	VkExtent2D get_extent() { return surface_provider.get_extent(); }

	SimpleBuffer<VkPhysicalDevice> enumerate_physical_devices();

	Instance(VkInstance instance, SurfaceProvider& surface_provider, uint32_t max_frames_in_flight)
		: instance{ instance }, surface_provider{ surface_provider }
	{
		surface = new Surface(surface_provider.create_surface(this));
		device = new Device(Device::create_device(surface, max_frames_in_flight));
	}

	~Instance();

private:
	static VkInstance create_instance_internal(const SimpleBuffer<const char*>& surface_instance_extensions);
	VkInstance instance;

	Surface* surface;
	SurfaceProvider surface_provider;
	Device* device;
};

} // namespace renderer
