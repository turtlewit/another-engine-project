#include <vulkan/vulkan.h>

#include "surface.hpp"
#include "instance.hpp"

namespace renderer {

Surface::~Surface()
{
	VkInstance vk_instance = instance->get_inner();

	vkDestroySurfaceKHR(vk_instance, surface, nullptr);
}

} // namespace renderer
