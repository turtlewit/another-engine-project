#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#define CGLTF_IMPLEMENTATION
#include <cgltf/cgltf.h>

#include "gltf.hpp"
#include "logger.hpp"
#include "device.hpp"
#include "memory.hpp"
#include "sampler.hpp"
#include "util.hpp"
#include "png.hpp"

namespace renderer {

namespace {

cgltf_accessor* find_accessor(cgltf_primitive* primitive, cgltf_attribute_type type)
{
	for (cgltf_attribute* attrib = primitive->attributes; attrib != primitive->attributes + primitive->attributes_count; ++attrib) {
		if (attrib->type == type)
			return attrib->data;
	}
	return nullptr;
}


} // namespace

// TODO: hrmmmst
struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 uv;
	glm::vec3 tangent;
	glm::vec3 bitangent;
};

uint32_t GltfMesh::get_vertex_count()
{
	cgltf_accessor* position = find_accessor(mesh->primitives, cgltf_attribute_type_position);
	return position->count;
}

SharedDeviceBuffer GltfMesh::get_vertex_buffer()
{
	if (!initialized)
		create_vertex_index_buffer();

	return move(vertex_buffer);
}

uint32_t GltfMesh::get_index_count()
{
	return mesh->primitives->indices->count;
}

SharedDeviceBuffer GltfMesh::get_index_buffer()
{
	if (!initialized)
		create_vertex_index_buffer();

	return move(index_buffer);
}

VkIndexType GltfMesh::get_index_type()
{
	switch (mesh->primitives->indices->component_type) {
		case cgltf_component_type_r_16u:
			return VK_INDEX_TYPE_UINT16;
		case cgltf_component_type_r_32u:
			return VK_INDEX_TYPE_UINT32;
	}

	LOG_WARNING("gltf: Unknown mesh index type (%d). Returning VK_INDEX_TYPE_UINT32...\n", mesh->primitives->indices->component_type);

	return VK_INDEX_TYPE_UINT32;
}

glm::vec3 GltfMesh::get_position()
{
	for (cgltf_node* node = gltf->data->nodes; node != gltf->data->nodes + gltf->data->nodes_count; ++node) {
		if (node->mesh == mesh) {
			if (!node->has_translation)
				return glm::vec3();

			return glm::vec3(node->translation[0], node->translation[1], node->translation[2]);
		}
	}
	
	return glm::vec3();
}

glm::quat GltfMesh::get_rotation()
{
	for (cgltf_node* node = gltf->data->nodes; node != gltf->data->nodes + gltf->data->nodes_count; ++node) {
		if (node->mesh == mesh) {
			if (!node->has_rotation)
				return glm::quat();

			return glm::quat(node->rotation[3], node->rotation[0], node->rotation[1], node->rotation[2]);
		}
	}
	
	return glm::quat();
}

glm::vec4 GltfMesh::get_color()
{
	if (!mesh->primitives->material)
		return glm::vec4(1, 1, 1, 1);

	if (mesh->primitives->material->has_pbr_metallic_roughness)
		return *(reinterpret_cast<glm::vec4*>(mesh->primitives->material->pbr_metallic_roughness.base_color_factor));

	if (mesh->primitives->material->has_pbr_specular_glossiness)
		return *(reinterpret_cast<glm::vec4*>(mesh->primitives->material->pbr_specular_glossiness.diffuse_factor));
	
	return glm::vec4(1.0, 1.0, 1.0, 1.0);
}

float GltfMesh::get_metallic()
{
	if (!mesh->primitives->material)
		return 0.0f;

	if (!mesh->primitives->material->has_pbr_metallic_roughness)
		return 0.0f;

	return mesh->primitives->material->pbr_metallic_roughness.metallic_factor;
}

float GltfMesh::get_roughness()
{
	if (!mesh->primitives->material)
		return 1.0f;

	if (!mesh->primitives->material->has_pbr_metallic_roughness)
		return 1.0f;

	return mesh->primitives->material->pbr_metallic_roughness.roughness_factor;
}

bool GltfMesh::has_transmission()
{
	if (!mesh->primitives->material)
		return false;
	return mesh->primitives->material->has_transmission;
}

AABB GltfMesh::get_aabb()
{
	AABB aabb{};

	cgltf_accessor* position = find_accessor(mesh->primitives, cgltf_attribute_type_position);

	for (uint32_t i = 0; i < position->count; ++i) {
		aabb.with_point(
			*reinterpret_cast<glm::vec3*>(BYTEARRAY(position->buffer_view->buffer->data) + position->buffer_view->offset + (i * sizeof(glm::vec3)))
		);
	}

	return aabb;
}

bool GltfMesh::get_albedo_texture(size_t& index)
{
	if (!mesh->primitives->material)
		return false;

	if (mesh->primitives->material->has_pbr_metallic_roughness && mesh->primitives->material->pbr_metallic_roughness.base_color_texture.texture) {
		index = static_cast<size_t>(mesh->primitives->material->pbr_metallic_roughness.base_color_texture.texture - gltf->data->textures);
		return true;
	} else if (mesh->primitives->material->has_pbr_specular_glossiness && mesh->primitives->material->pbr_specular_glossiness.diffuse_texture.texture) {
		index = static_cast<size_t>(mesh->primitives->material->pbr_specular_glossiness.diffuse_texture.texture - gltf->data->textures);
		return true;
	}
	return false;
}

bool GltfMesh::get_normal_texture(size_t& index)
{
	if (!mesh->primitives->material)
		return false;

	if (mesh->primitives->material->normal_texture.texture) {
		index = static_cast<size_t>(mesh->primitives->material->normal_texture.texture - gltf->data->textures);
		return true;
	}
	return false;
}

template <typename T>
void calculate_tangents(SimpleBuffer<Vertex>& vertices, Vector<T>& index_buffer, cgltf_accessor* indices)
{
	for (size_t i = 0; i < indices->count; i += 3) {
		T i0 = *reinterpret_cast<T*>(BYTEARRAY(indices->buffer_view->buffer->data) + indices->buffer_view->offset + (i * sizeof(T)));
		T i2 = *reinterpret_cast<T*>(BYTEARRAY(indices->buffer_view->buffer->data) + indices->buffer_view->offset + ((i + 1) * sizeof(T)));
		T i1 = *reinterpret_cast<T*>(BYTEARRAY(indices->buffer_view->buffer->data) + indices->buffer_view->offset + ((i + 2) * sizeof(T)));

		Vertex& v0 = vertices.get_buffer()[i0];
		Vertex& v1 = vertices.get_buffer()[i1];
		Vertex& v2 = vertices.get_buffer()[i2];

		glm::vec3 e0 = v1.position - v0.position;
		glm::vec3 e1 = v2.position - v0.position;

		glm::vec2 duv0 = v1.uv - v0.uv;
		glm::vec2 duv1 = v2.uv - v0.uv;

		float r = 1.0f / (duv0.x * duv1.y - duv0.y * duv1.x);
		glm::vec3 tangent = (e0 * duv1.y - e1 * duv0.y) * r;
		//glm::vec3 bitangent = (e1 * duv0.x - e0 * duv1.x) * r;

		for (auto v : {i0, i1, i2}) {
			glm::vec3 n = glm::normalize(vertices.get_buffer()[v].normal);
			glm::vec3 t = glm::normalize(tangent - n * glm::dot(n, tangent));
			glm::vec3 b = glm::normalize(glm::cross(n, t));

			if (glm::dot(glm::cross(n, t), b) < 0.0f) {
				t *= -1.0f;
			}

			vertices.get_buffer()[v].tangent += t;
			vertices.get_buffer()[v].bitangent += b;
		}

		index_buffer.add({ i0, i1, i2 });
	}
}

void GltfMesh::create_vertex_index_buffer() 
{
	initialized = true;

	cgltf_accessor* position = find_accessor(mesh->primitives, cgltf_attribute_type_position);
	cgltf_accessor* normal = find_accessor(mesh->primitives, cgltf_attribute_type_normal);
	cgltf_accessor* uv = find_accessor(mesh->primitives, cgltf_attribute_type_texcoord);

	cgltf_accessor* indices = mesh->primitives->indices;

	SimpleBuffer<Vertex> vertices;
	vertices.resize(position->count);

	for (uint32_t i = 0; i < position->count; ++i) {
		vertices.get_buffer()[i] = Vertex{
			*reinterpret_cast<glm::vec3*>(BYTEARRAY(position->buffer_view->buffer->data) + position->buffer_view->offset + (i * sizeof(glm::vec3))),
			*reinterpret_cast<glm::vec3*>(BYTEARRAY(normal->buffer_view->buffer->data)   + normal->buffer_view->offset   + (i * sizeof(glm::vec3))),
			*reinterpret_cast<glm::vec2*>(BYTEARRAY(uv->buffer_view->buffer->data)       + uv->buffer_view->offset       + (i * sizeof(glm::vec2))),
			glm::vec3(0, 0, 0),
			glm::vec3(0, 0, 0),
		};
		vertices.get_buffer()[i].position.y *= -1;
		vertices.get_buffer()[i].normal.y *= -1;
		//vertices.get_buffer()[i].position.z *= -1;
		//vertices.get_buffer()[i].position.x *= -1;
	}

	if (indices->component_type == cgltf_component_type_r_16u) {
		Vector<uint16_t> new_indices{};
		calculate_tangents<uint16_t>(vertices, new_indices, indices);

		Memory::create_device_local_vertex_index_buffer(
			gltf->device, 
			sizeof(Vertex) * vertices.get_size(), vertices.get_buffer(), 
			indices->buffer_view->size, new_indices.get_buffer(), 
			vertex_buffer, index_buffer);

	} else if (indices->component_type == cgltf_component_type_r_32u) {
		Vector<uint32_t> new_indices{};
		calculate_tangents<uint32_t>(vertices, new_indices, indices);

		Memory::create_device_local_vertex_index_buffer(
			gltf->device, 
			sizeof(Vertex) * vertices.get_size(), vertices.get_buffer(), 
			indices->buffer_view->size, new_indices.get_buffer(), 
			vertex_buffer, index_buffer);

	} else {
		LOG_ERROR("unhandled cgltf component type %d!");
		exit(1);
	}

}

void Gltf::get_textures(Vector<Sampler>& textures, Vector<size_t>& indices)
{
	textures.reserve(data->textures_count);
	indices.reserve(data->textures_count);
	char* base_path = File::get_path(path, strlen(path));

	size_t i = 0;
	for (cgltf_texture* texture = data->textures; texture != data->textures + data->textures_count; ++texture) {
		bool same_texture = false;
		for (size_t j = 0; j < i; ++j) {
			if (strcmp(data->textures[j].image->uri, texture->image->uri) == 0) {// same texture
				indices.add(indices[j]);
				same_texture = true;
			}
		}

		if (!same_texture && STRNCMP_LITERAL("image/png", texture->image->mime_type) == 0) {
			char* filename = concatenate(base_path, texture->image->uri);
			Logger::log_info("Image path: %s\n", filename);

			indices.add(i);

			VkExtent2D extent;
			auto image = load_png(filename, extent);

			Logger::log_info("Size of image: %llu\n", image.get_size());

			VkFormat format = VK_FORMAT_R8G8B8A8_SRGB;
			for (cgltf_material* mat = data->materials; mat != data->materials + data->materials_count; ++mat) {
				if (mat->normal_texture.texture == texture) {
					format = VK_FORMAT_R8G8B8A8_UNORM;
					Logger::log_info("Using Linear\n");
					break;
				}
			}

			Sampler sampler = SamplerBuilder()
				.with_extent_2d(extent.width, extent.height)
				.with_format(format)
				.with_tiling(VK_IMAGE_TILING_OPTIMAL)
				.with_initial_layout(VK_IMAGE_LAYOUT_UNDEFINED)
				.with_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT)
				.with_mip_mapping()
				.build(device)
			;

			sampler.copy_to_image(image.get_size(), image.get_buffer());

			textures.add(move(sampler));

			delete[] filename;
		}

		++i;
	}

	delete[] base_path;
}


Gltf::Gltf(Device* device, const char* path)
	: device{ device }, path{ path }
{
	cgltf_options options = cgltf_options();
	cgltf_result r = cgltf_parse_file(&options, path, &data);
	if (r > 0) {
		LOG_ERROR("gltf: could not load model, cgltf_error %d. path: %s", r, path);
		exit(1);
	}

	cgltf_load_buffers(&options, data, path);

	for (cgltf_mesh* mesh = data->meshes; mesh != data->meshes + data->meshes_count; ++mesh) {
		meshes.add(GltfMesh(this, mesh));
	}
}

Gltf::~Gltf()
{
	cgltf_free(data);
}


} // namespace renderer
