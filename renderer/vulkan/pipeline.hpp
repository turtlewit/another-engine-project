#pragma once

#include <vulkan/vulkan.h>
#include <spirv_reflect.h>

#include <vector.hpp>

#include <render_pass.hpp>
#include <device.hpp>
#include "util.hpp"

namespace renderer {

class Device;
class Swapchain;

//VkShaderModule load_shader(Device* device, const char* filepath);

struct ShaderModule {
	VkShaderModule shader_module;
	SpvReflectShaderModule spv_module;
};

class Pipeline;

class SpecializationInfoBuilder {
public:
	template<typename T>
	SpecializationInfoBuilder& with_constant(const T& constant)
	{
		VkSpecializationMapEntry entry {
			.constantID = u32(entries.get_size()),
			.offset = u32(data.get_size()),
			.size = sizeof(T),
		};

		entries.add(entry);
		data.resize_uninitialized(data.get_size() + sizeof(T));
		*reinterpret_cast<T*>(data.get_buffer() + entry.offset) = constant;

		info = VkSpecializationInfo {
			.mapEntryCount = u32(entries.get_size()),
			.pMapEntries = entries.get_buffer(),
			.dataSize = u32(data.get_size()),
			.pData = data.get_buffer(),
		};

		return *this;
	}

	const VkSpecializationInfo* build() const
	{
		if (entries.get_size() == 0)
			return nullptr;

		return &info;
	}

	SpecializationInfoBuilder() = default;
	SpecializationInfoBuilder(const SpecializationInfoBuilder&) = delete;
	SpecializationInfoBuilder(SpecializationInfoBuilder&&) = default;

private:
	Vector<VkSpecializationMapEntry> entries{};
	Vector<uint8_t> data{};
	VkSpecializationInfo info{};
};

class PipelineBuilder {
	friend Pipeline;
public:
	PipelineBuilder& with_shader(Device* device, const char* path, const SpecializationInfoBuilder& specialization_info = SpecializationInfoBuilder{});
	PipelineBuilder& with_shader(Device* device, const void* data, size_t size, const SpecializationInfoBuilder& specialization_info = SpecializationInfoBuilder{});
	PipelineBuilder& with_shader(Device* device, VkShaderModule shader_module, SpvReflectShaderModule spv_module, const SpecializationInfoBuilder& specialization_info = SpecializationInfoBuilder{});
	PipelineBuilder& with_vertex_binding(uint32_t binding, uint32_t stride, VkVertexInputRate input_rate = VK_VERTEX_INPUT_RATE_VERTEX);
	PipelineBuilder& with_vertex_attribute(uint32_t location, uint32_t binding, VkFormat format, uint32_t offset);
	PipelineBuilder& with_attachments(uint32_t n);
	PipelineBuilder& with_attachments(uint32_t n, VkPipelineColorBlendAttachmentState blend_attachment);
	PipelineBuilder& with_depth_stencil(VkBool32 read = true, VkBool32 write = true);
	PipelineBuilder& with_subpass(uint32_t subpass);
	PipelineBuilder& with_rasterization_samples(VkSampleCountFlagBits flags);
	PipelineBuilder& with_sample_shading(float min_shading);
	PipelineBuilder& with_ccw();
	PipelineBuilder& with_viewports(SimpleBuffer<VkViewport> viewports, SimpleBuffer<VkRect2D> scissors);

	VkPipeline build(Device* device, VkPipelineLayout pipeline_layout, VkRenderPass render_pass, VkPipeline base_pipeline = VK_NULL_HANDLE);
	PipelineBuilder();
	PipelineBuilder(const PipelineBuilder&) = default;

private:
	Vector<VkPipelineShaderStageCreateInfo> shader_stages;
	Vector<VkVertexInputBindingDescription> vertex_input_binding_descriptions;
	Vector<VkVertexInputAttributeDescription> vertex_attribute_descriptions;
	VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info;
	VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info;
	VkPipelineViewportStateCreateInfo viewport_state_create_info;
	VkPipelineRasterizationStateCreateInfo rasterization_state_create_info;
	VkPipelineMultisampleStateCreateInfo multisample_state_create_info;
	VkPipelineColorBlendAttachmentState color_blend_attachment_state;
	VkPipelineColorBlendStateCreateInfo color_blend_state_create_info;
	VkPipelineDepthStencilStateCreateInfo depth_stencil_state;
	Vector<VkPipelineColorBlendAttachmentState> color_blend_states;

	bool default_attachments = true;
	bool depth_stencil = false;
	uint32_t subpass = 0;
	Vector<ShaderModule> shader_modules;
	bool default_viewport = true;
	SimpleBuffer<VkViewport> viewports{};
	SimpleBuffer<VkRect2D> scissors{};
};

class DescriptorSetLayoutBuilder {
public:
	DescriptorSetLayoutBuilder& with_uniform_buffer(VkShaderStageFlags stages, uint32_t descriptor_count = 1);
	DescriptorSetLayoutBuilder& with_storage_buffer(VkShaderStageFlags stages, uint32_t descriptor_count = 1);
	DescriptorSetLayoutBuilder& with_sampler(VkShaderStageFlags stages, uint32_t descriptor_count = 1);
	DescriptorSetLayoutBuilder& with_input_attachment(VkShaderStageFlags stages, uint32_t descriptor_count = 1);
	VkDescriptorSetLayout build(Device* device);
private:
	uint32_t current_binding = 0;
	Vector<VkDescriptorSetLayoutBinding> bindings;
};

VkPipelineLayout create_pipeline_layout(Device* device, const Vector<VkDescriptorSetLayout>& descriptor_set_layouts = {}, const Vector<VkPushConstantRange>& push_constant_ranges = {});

class Pipeline {
public:
	VkPipeline& get_inner() { return pipeline; }

	const Vector<VkDescriptorSetLayout>& get_descriptor_set_layouts() { return descriptor_set_layouts; };
	VkPipelineLayout& get_pipeline_layout() { return pipeline_layout; }

	void reset(VkRenderPass render_pass, VkPipeline base_pipeline = VK_NULL_HANDLE);

	Pipeline()
		: device{ nullptr }
	{
	}

	Pipeline(Device* device, 
	         PipelineBuilder builder, 
		 VkRenderPass render_pass,
		 VkPipeline base_pipeline = VK_NULL_HANDLE)
		: device{ device }
		, builder{ builder }
		, descriptor_set_layouts{}
		, descriptor_set_builders{}
		, pipeline_layout{}
		, pipeline{}
	{
		create_descriptor_set_layouts();
		pipeline = builder.build(device, pipeline_layout, render_pass, base_pipeline);
	}

	Pipeline(Pipeline&& other)
		: device{ other.device }
		, builder{ other.builder }
		, descriptor_set_layouts{ other.descriptor_set_layouts }
		, descriptor_set_builders{ other.descriptor_set_builders }
		, pipeline_layout{ other.pipeline_layout }
		, pipeline{ other.pipeline }
	{
		other.pipeline = VK_NULL_HANDLE;
		other.device = nullptr;
	}

	Pipeline& operator=(Pipeline&& other)
	{
		if (&other == this)
			return *this;

		this->~Pipeline();
		::new(this) Pipeline(move(other));

		return *this;
	}

	~Pipeline();


private:
	Device* device;

	PipelineBuilder builder;
	Vector<VkDescriptorSetLayout> descriptor_set_layouts;
	Vector<DescriptorSetLayoutBuilder> descriptor_set_builders;
	VkPipelineLayout pipeline_layout;
	VkPipeline pipeline;

	void create_descriptor_set_layouts();
};

} // namespace renderer
