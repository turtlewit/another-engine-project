#include "texture.hpp"
#include "device.hpp"

namespace renderer {

Vector<Texture> Texture::create_simple_textures(Device* device, const Vector<Image>& images)
{
	Vector<SharedDeviceMemory> memories = Memory::allocate_image_memory(
			device, images, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	Vector<Texture> textures{};
	textures.reserve(images.get_size());

	for (size_t i = 0; i < images.get_size(); ++i) {

		VkImageView image_view = images[i].create_simple_image_view(device);

		VkSamplerCreateInfo sampler_create_info{};
		sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		sampler_create_info.pNext = nullptr;
		sampler_create_info.flags = 0;
		sampler_create_info.magFilter = VK_FILTER_LINEAR;
		sampler_create_info.minFilter = VK_FILTER_LINEAR;
		sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		sampler_create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler_create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler_create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		sampler_create_info.mipLodBias = 0.0f;
		sampler_create_info.anisotropyEnable = VK_FALSE;
		sampler_create_info.maxAnisotropy = 0;
		sampler_create_info.compareEnable = VK_FALSE;
		sampler_create_info.compareOp = VK_COMPARE_OP_NEVER;
		sampler_create_info.minLod = 0.0f;
		sampler_create_info.maxLod = 0.0f;
		sampler_create_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		sampler_create_info.unnormalizedCoordinates = VK_FALSE; // normalized: [0,1]. unnormalized: [0, extent]

		VkSampler sampler;
		vkCreateSampler(device->get_inner(), &sampler_create_info, nullptr, &sampler);

		textures.add(Texture(device, images[i].image, image_view, memories[i], sampler));
	}

	return textures;
}

Texture::~Texture()
{
	vkDestroySampler(device->get_inner(), sampler, nullptr);
	vkDestroyImageView(device->get_inner(), view, nullptr);
	vkDestroyImage(device->get_inner(), image.image, nullptr);
}

} // namespace renderer
