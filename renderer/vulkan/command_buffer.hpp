#pragma once

#include <vulkan/vulkan.h>
#include <vector.hpp>

namespace renderer {

class Device;
class RenderPass;
class CommandPool;

class CommandBuffer {
public:
	VkCommandBuffer get_inner() { return command_buffer; }

	void begin(VkCommandBufferUsageFlags usage = 0);
	void begin_render_pass(RenderPass* render_pass, uint32_t framebuffer);
	void begin_render_pass(RenderPass* render_pass, uint32_t framebuffer, VkExtent2D render_area);
	void end_render_pass();
	void copy_buffer(VkBuffer src, VkBuffer dst, const SimpleBuffer<VkBufferCopy>& regions);
	void copy_buffer(VkBuffer src, VkBuffer dst, VkDeviceSize size);
	void end();
	void free(Device* device);

	CommandBuffer(CommandPool* pool, VkCommandBuffer command_buffer, uint32_t reset_id)
		: pool{ pool }, command_buffer{ command_buffer }, reset_id{ reset_id }
	{
	}

	CommandBuffer(const CommandBuffer& other) = default;
	CommandBuffer& operator=(const CommandBuffer& other) = default;

private:
	CommandPool* pool;

	VkCommandBuffer command_buffer;
	uint32_t reset_id;
};

class CommandPool {
public:
	static CommandPool create_command_pool(Device* device, uint32_t queue_family_index);

	VkCommandPool get_inner() { return command_pool; }

	CommandBuffer allocate_command_buffer(VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	Vector<CommandBuffer> allocate_command_buffers(uint32_t count, VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);

	CommandBuffer begin_single_use_command_buffer();

	void reset(VkCommandPoolResetFlags flags = VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);

	CommandPool(Device* device, VkCommandPool command_pool)
		: device{ device }, command_pool{ command_pool }, reset_id{ 1 }
	{
	}

	CommandPool(CommandPool&& other)
		: device{ other.device }, command_pool{ other.command_pool }, reset_id{ other.reset_id }
	{
		other.command_pool = VK_NULL_HANDLE;
	}

	~CommandPool();

private:
	Device* device;

	VkCommandPool command_pool;
	uint32_t reset_id;

};

} // namespace renderer
