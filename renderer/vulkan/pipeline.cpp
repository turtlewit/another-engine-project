#include <cstdio>
#include <stdexcept>
#include <algorithm>

#include "pipeline.hpp"
#include "device.hpp"

#include "simple_buffer.hpp"
#include "swapchain.hpp"
#include "util.hpp"
#include "logger.hpp"
#include "deferred_pipeline.hpp"

namespace renderer {

void load_shader(Device* device, const void* data, size_t size, VkShaderModule& shader_module, SpvReflectShaderModule& spv_module)
{
	VkShaderModuleCreateInfo shader_module_create_info{};
	shader_module_create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shader_module_create_info.codeSize = size;
	shader_module_create_info.pCode = reinterpret_cast<const uint32_t*>(data);

	ASSERT_VK_SUCCESS(vkCreateShaderModule(device->get_inner(), &shader_module_create_info, nullptr, &shader_module));

	SpvReflectResult result;
	if ((result = spvReflectCreateShaderModule(size, data, &spv_module)) != SPV_REFLECT_RESULT_SUCCESS) {
		LOG_ERROR("spvReflectCreateShaderModule returned %d\n", result);
		exit(result);
	}
}

void load_shader(Device* device, const char* filepath, VkShaderModule& shader_module, SpvReflectShaderModule& spv_module)
{
	SimpleBuffer<uint8_t> bytes = File::read_whole_file(filepath);
	load_shader(device, bytes.get_buffer(), bytes.get_size(), shader_module, spv_module);
}

PipelineBuilder& PipelineBuilder::with_shader(Device* device, const char* path, const SpecializationInfoBuilder& specialization_info)
{
	VkShaderModule shader_module;
	SpvReflectShaderModule spv_module;
	load_shader(device, path, shader_module, spv_module);

	return with_shader(device, shader_module, spv_module, specialization_info);
}

PipelineBuilder& PipelineBuilder::with_shader(Device* device, const void* data, size_t size, const SpecializationInfoBuilder& specialization_info)
{
	VkShaderModule shader_module;
	SpvReflectShaderModule spv_module;
	load_shader(device, data, size, shader_module, spv_module);

	return with_shader(device, shader_module, spv_module, specialization_info);
}

PipelineBuilder& PipelineBuilder::with_shader(Device* device, VkShaderModule shader_module, SpvReflectShaderModule spv_module, const SpecializationInfoBuilder& specialization_info)
{
	shader_stages.add(VkPipelineShaderStageCreateInfo{
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = static_cast<VkShaderStageFlagBits>(spv_module.shader_stage),
		.module = shader_module,
		.pName = spv_module.entry_point_name,
		.pSpecializationInfo = specialization_info.build(),
	});

	if (spv_module.shader_stage == SPV_REFLECT_SHADER_STAGE_VERTEX_BIT) {
		Vector<uint32_t> strides{};
		strides.resize(spv_module.input_variable_count);

		// get total strides
		for (size_t i = 0; i < spv_module.input_variable_count; ++i) {
			SpvReflectInterfaceVariable* var = spv_module.input_variables[i];

			// TODO: input matrices?
			strides[var->location] = (var->numeric.scalar.width / 8) * var->numeric.vector.component_count;
		}

		with_vertex_binding(0, strides.sum());

		// get offsets from strides
		for (size_t i = 0; i < spv_module.input_variable_count; ++i) {
			SpvReflectInterfaceVariable* var = spv_module.input_variables[i];
			with_vertex_attribute(var->location, 0, static_cast<VkFormat>(var->format), strides.sum_to(var->location));
		}
	} else if (spv_module.shader_stage == SPV_REFLECT_SHADER_STAGE_FRAGMENT_BIT) {
		with_attachments(spv_module.output_variable_count);
	}

	//Logger::log_info("Shader Info:\n");
	//Logger::print_info("      from shader module: input count %lu\n", spv_module.input_variable_count);
	//for (size_t i = 0; i < spv_module.input_variable_count; ++i) {
	//	SpvReflectInterfaceVariable* var = spv_module.input_variables[i];
	//	Logger::print_info(
	//	           "      Input Variable:\n"
	//	           "          Name:     %s\n"
	//	           "          Location: %lu\n"
	//	           "          Format:   %d\n",
	//	           var->name, var->location, var->format
	//	);
	//}

	shader_modules.add(ShaderModule{ shader_module, spv_module });
	return *this;
}

PipelineBuilder& PipelineBuilder::with_vertex_binding(uint32_t binding, uint32_t stride, VkVertexInputRate input_rate)
{
	vertex_input_binding_descriptions.add(VkVertexInputBindingDescription{
			.binding = binding,
			.stride = stride,
			.inputRate = input_rate
	});
	return *this;
}

PipelineBuilder& PipelineBuilder::with_vertex_attribute(uint32_t location, uint32_t binding, VkFormat format, uint32_t offset)
{
	vertex_attribute_descriptions.add(VkVertexInputAttributeDescription{
			.location = location,
			.binding = binding,
			.format = format,
			.offset = offset
	});
	return *this;
}

PipelineBuilder& PipelineBuilder::with_attachments(uint32_t n)
{
	default_attachments = false;

	color_blend_states.add(color_blend_attachment_state, n);

	return *this;
}

PipelineBuilder& PipelineBuilder::with_attachments(uint32_t n, VkPipelineColorBlendAttachmentState blend_attachment)
{
	default_attachments = false;

	color_blend_states.add(blend_attachment, n);

	return *this;
}

PipelineBuilder& PipelineBuilder::with_depth_stencil(VkBool32 read, VkBool32 write)
{
	depth_stencil = true;
	depth_stencil_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depth_stencil_state.depthTestEnable = read;
	depth_stencil_state.depthWriteEnable = write;
	depth_stencil_state.depthCompareOp = VK_COMPARE_OP_LESS;
	depth_stencil_state.depthBoundsTestEnable = VK_FALSE;
	depth_stencil_state.stencilTestEnable = VK_FALSE;
	depth_stencil_state.front = {};
	depth_stencil_state.back = {};
	depth_stencil_state.minDepthBounds = 0.0f;
	depth_stencil_state.maxDepthBounds = 1.0f;

	return *this;
}

PipelineBuilder& PipelineBuilder::with_subpass(uint32_t subpass_idx)
{
	subpass = subpass_idx;
	return *this;
}

PipelineBuilder& PipelineBuilder::with_rasterization_samples(VkSampleCountFlagBits flags)
{
	multisample_state_create_info.rasterizationSamples = flags;
	return *this;
}

PipelineBuilder& PipelineBuilder::with_sample_shading(float min_shading)
{
	multisample_state_create_info.sampleShadingEnable = VK_TRUE;
	multisample_state_create_info.minSampleShading = min_shading;
	return *this;
}

PipelineBuilder& PipelineBuilder::with_ccw()
{
	rasterization_state_create_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	return *this;
}

PipelineBuilder& PipelineBuilder::with_viewports(SimpleBuffer<VkViewport> new_viewports, SimpleBuffer<VkRect2D> new_scissors)
{
	default_viewport = false;
	viewports = viewports + new_viewports;
	scissors = scissors + new_scissors;
	return *this;
}

VkPipeline PipelineBuilder::build(Device* device, VkPipelineLayout pipeline_layout, VkRenderPass render_pass, VkPipeline base_pipeline)
{
	Swapchain* swapchain = device->get_swapchain();
	viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	if (default_viewport) {
		viewport_state_create_info.viewportCount = 1;
		viewport_state_create_info.pViewports = &swapchain->get_viewport();
		viewport_state_create_info.scissorCount = 1;
		viewport_state_create_info.pScissors = &swapchain->get_scissor();
	} else {
		viewport_state_create_info.viewportCount = viewports.get_size();
		viewport_state_create_info.pViewports = viewports.get_buffer();
		viewport_state_create_info.scissorCount = scissors.get_size();
		viewport_state_create_info.pScissors = scissors.get_buffer();
	}

	VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info{};
	vertex_input_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertex_input_state_create_info.vertexBindingDescriptionCount = vertex_input_binding_descriptions.get_size();
	vertex_input_state_create_info.pVertexBindingDescriptions = vertex_input_binding_descriptions.get_buffer();
	vertex_input_state_create_info.vertexAttributeDescriptionCount = vertex_attribute_descriptions.get_size();
	vertex_input_state_create_info.pVertexAttributeDescriptions = vertex_attribute_descriptions.get_buffer();

	if (default_attachments) {
		color_blend_state_create_info.attachmentCount = 1;
		color_blend_state_create_info.pAttachments = &color_blend_attachment_state;
	} else {
		color_blend_state_create_info.attachmentCount = color_blend_states.get_size();
		color_blend_state_create_info.pAttachments = color_blend_states.get_buffer();
	}

	VkGraphicsPipelineCreateInfo pipeline_create_info{};
	pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipeline_create_info.stageCount = shader_stages.get_size();
	pipeline_create_info.pStages = shader_stages.get_buffer();
	pipeline_create_info.pVertexInputState = &vertex_input_state_create_info;
	pipeline_create_info.pInputAssemblyState = &input_assembly_state_create_info;
	pipeline_create_info.pViewportState = &viewport_state_create_info;
	pipeline_create_info.pRasterizationState = &rasterization_state_create_info;
	pipeline_create_info.pMultisampleState = &multisample_state_create_info;
	pipeline_create_info.pDepthStencilState = nullptr;
	if (depth_stencil)
		pipeline_create_info.pDepthStencilState = &depth_stencil_state;
	pipeline_create_info.pColorBlendState = &color_blend_state_create_info;
	pipeline_create_info.pDynamicState = nullptr;
	pipeline_create_info.layout = pipeline_layout;
	pipeline_create_info.renderPass = render_pass;
	pipeline_create_info.subpass = subpass;
	pipeline_create_info.basePipelineHandle = base_pipeline;
	pipeline_create_info.basePipelineIndex = 0;

	VkPipeline pipeline;
	vkCreateGraphicsPipelines(device->get_inner(), VK_NULL_HANDLE, 1, &pipeline_create_info, nullptr, &pipeline);

	return pipeline;
}

PipelineBuilder::PipelineBuilder()
	: shader_stages{}
	, vertex_attribute_descriptions{}
	, vertex_input_state_create_info{}
	, input_assembly_state_create_info{}
	, viewport_state_create_info{}
	, rasterization_state_create_info{}
	, multisample_state_create_info{}
	, color_blend_attachment_state{}
	, color_blend_state_create_info{}
	, depth_stencil_state{}
	, color_blend_states{}
{

	input_assembly_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	input_assembly_state_create_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	input_assembly_state_create_info.primitiveRestartEnable = VK_FALSE;

	rasterization_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterization_state_create_info.depthClampEnable = VK_FALSE;
	rasterization_state_create_info.rasterizerDiscardEnable = VK_FALSE;
	rasterization_state_create_info.polygonMode = VK_POLYGON_MODE_FILL;
	rasterization_state_create_info.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterization_state_create_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterization_state_create_info.depthBiasEnable = VK_FALSE;
	rasterization_state_create_info.depthBiasConstantFactor = 0.0f;
	rasterization_state_create_info.depthBiasClamp = 0.0f;
	rasterization_state_create_info.depthBiasSlopeFactor = 0.0f;
	rasterization_state_create_info.lineWidth = 1.0f;

	multisample_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisample_state_create_info.sampleShadingEnable = VK_FALSE;
	multisample_state_create_info.minSampleShading = 1.0f;
	multisample_state_create_info.pSampleMask = nullptr;
	multisample_state_create_info.alphaToCoverageEnable = VK_FALSE;
	multisample_state_create_info.alphaToOneEnable = VK_FALSE;

	color_blend_attachment_state.blendEnable = VK_FALSE;
	color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
	color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
	color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
	color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
	color_blend_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	color_blend_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	color_blend_state_create_info.logicOpEnable = VK_FALSE;
	color_blend_state_create_info.logicOp = VK_LOGIC_OP_COPY;
}

DescriptorSetLayoutBuilder& DescriptorSetLayoutBuilder::with_uniform_buffer(VkShaderStageFlags stages, uint32_t descriptor_count)
{
	bindings.add(VkDescriptorSetLayoutBinding{
			.binding = current_binding++,
			.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.descriptorCount = descriptor_count,
			.stageFlags = stages,
			.pImmutableSamplers = nullptr
	});
	return *this;
}

DescriptorSetLayoutBuilder& DescriptorSetLayoutBuilder::with_storage_buffer(VkShaderStageFlags stages, uint32_t descriptor_count)
{
	bindings.add(VkDescriptorSetLayoutBinding{
		.binding = current_binding++,
		.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
		.descriptorCount = descriptor_count,
		.stageFlags = stages,
		.pImmutableSamplers = nullptr
	});
	return *this;
}

DescriptorSetLayoutBuilder& DescriptorSetLayoutBuilder::with_sampler(VkShaderStageFlags stages, uint32_t descriptor_count)
{
	bindings.add(VkDescriptorSetLayoutBinding{
		.binding = current_binding++,
		.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		.descriptorCount = descriptor_count,
		.stageFlags = stages,
		.pImmutableSamplers = nullptr
	});
	return *this;
}

DescriptorSetLayoutBuilder& DescriptorSetLayoutBuilder::with_input_attachment(VkShaderStageFlags stages, uint32_t descriptor_count)
{
	bindings.add(VkDescriptorSetLayoutBinding{
		.binding = current_binding++,
		.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
		.descriptorCount = descriptor_count,
		.stageFlags = stages,
		.pImmutableSamplers = nullptr
	});
	return *this;
}

VkDescriptorSetLayout DescriptorSetLayoutBuilder::build(Device* device)
{
	VkDescriptorSetLayoutCreateInfo descriptor_set_layout_create_info{};
	descriptor_set_layout_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptor_set_layout_create_info.bindingCount = bindings.get_size();
	descriptor_set_layout_create_info.pBindings = bindings.get_buffer();

	VkDescriptorSetLayout layout;
	vkCreateDescriptorSetLayout(device->get_inner(), &descriptor_set_layout_create_info, nullptr, &layout);
	return layout;
}

VkPipelineLayout create_pipeline_layout(Device* device, const Vector<VkDescriptorSetLayout>& descriptor_set_layouts, const Vector<VkPushConstantRange>& push_constant_ranges)
{
	VkPipelineLayoutCreateInfo pipeline_layout_create_info{};
	pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipeline_layout_create_info.setLayoutCount = descriptor_set_layouts.get_size();
	pipeline_layout_create_info.pSetLayouts = descriptor_set_layouts.get_buffer();
	pipeline_layout_create_info.pushConstantRangeCount = push_constant_ranges.get_size();
	pipeline_layout_create_info.pPushConstantRanges = push_constant_ranges.get_buffer();

	VkPipelineLayout pipeline_layout;
	vkCreatePipelineLayout(device->get_inner(), &pipeline_layout_create_info, nullptr, &pipeline_layout);
	return pipeline_layout;
}

void Pipeline::reset(VkRenderPass render_pass, VkPipeline base_pipeline)
{
	vkDestroyPipeline(device->get_inner(), pipeline, nullptr);
	pipeline = builder.build(device, pipeline_layout, render_pass, base_pipeline);
}

struct DescriptorInfo {
	SpvReflectDescriptorBinding* binding;
	VkShaderStageFlags shader_stages;
};

bool sort_infos(const DescriptorInfo& a, const DescriptorInfo& b)
{
	return a.binding->binding < b.binding->binding;
}

void Pipeline::create_descriptor_set_layouts()
{
	Vector<Vector<DescriptorInfo>> sets;
	Vector<VkPushConstantRange> push_constant_ranges;
	for (auto& shader_module : builder.shader_modules) {
		SpvReflectShaderModule spv_module = shader_module.spv_module;

		for (uint32_t i = 0; i < spv_module.descriptor_set_count; ++i) {
			SpvReflectDescriptorSet set = spv_module.descriptor_sets[i];

			if (sets.get_size() <= set.set) {
				sets.resize(set.set + 1);
			}

			Vector<DescriptorInfo>& descriptor_infos = sets[set.set];

			for (SpvReflectDescriptorBinding** p_binding = set.bindings; p_binding != set.bindings + set.binding_count; ++p_binding) {
				SpvReflectDescriptorBinding* binding = *p_binding;
				if (descriptor_infos.get_size() <= binding->binding)
					descriptor_infos.resize(binding->binding + 1);

				DescriptorInfo& descriptor_info = descriptor_infos[binding->binding];

				descriptor_info.binding = binding;
				descriptor_info.shader_stages |= static_cast<VkShaderStageFlags>(spv_module.shader_stage);
			}
		}

		for (uint32_t i = 0; i < spv_module.push_constant_block_count; ++i) {
			SpvReflectBlockVariable push_constant = spv_module.push_constant_blocks[i];
			VkPushConstantRange range {
				.stageFlags = static_cast<VkShaderStageFlags>(spv_module.shader_stage),
				.offset = push_constant.offset,
				.size = push_constant.size,
			};

			push_constant_ranges.add(range);
		}
	}

	descriptor_set_builders.resize(sets.get_size());

	for (size_t i = 0; i < sets.get_size(); ++i) {
		Vector<DescriptorInfo>& descriptor_infos = sets[i];
		DescriptorSetLayoutBuilder& builder = descriptor_set_builders[i];

		std::sort(descriptor_infos.begin(), descriptor_infos.end(), sort_infos);

		for (auto& info : descriptor_infos) {
			switch (info.binding->descriptor_type) {
				case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
					builder.with_uniform_buffer(info.shader_stages, info.binding->count);
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER:
					builder.with_storage_buffer(info.shader_stages, info.binding->count);
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
					builder.with_sampler(info.shader_stages, info.binding->count);
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
					builder.with_input_attachment(info.shader_stages, info.binding->count);
					break;
				default:
					LOG_ERROR("Unimplemented descriptor type %d", info.binding->descriptor_type);
					exit(1);
			}
		}

		descriptor_set_layouts.add(builder.build(device));
	}

	pipeline_layout = create_pipeline_layout(device, descriptor_set_layouts, push_constant_ranges);
}

Pipeline::~Pipeline()
{
	if (device != nullptr && pipeline != VK_NULL_HANDLE) {
		for (auto& shader_module : builder.shader_modules) {
			vkDestroyShaderModule(device->get_inner(), shader_module.shader_module, nullptr);
			spvReflectDestroyShaderModule(&shader_module.spv_module);
		}

		vkDestroyPipeline(device->get_inner(), pipeline, nullptr);
		vkDestroyPipelineLayout(device->get_inner(), pipeline_layout, nullptr);

		for (VkDescriptorSetLayout descriptor_set_layout : descriptor_set_layouts)
			vkDestroyDescriptorSetLayout(device->get_inner(), descriptor_set_layout, nullptr);
	}
}

} // namespace renderer
