#include <cmath>
#include <cstring>
#include <algorithm>

#include "memory.hpp"
#include "device.hpp"
#include "sampler.hpp"
#include "logger.hpp"

namespace renderer {

SharedDeviceMemory::~SharedDeviceMemory()
{
	if (!device || !data)
		return;

	--data->references;

	if (data->references == 0) {
		vkFreeMemory(device->get_inner(), data->memory, nullptr);
		delete data;
	}
}

Buffer::~Buffer()
{
	if (device && buffer != VK_NULL_HANDLE)
		vkDestroyBuffer(device->get_inner(), buffer, nullptr);
}


uint32_t Memory::find_memory_type(VkPhysicalDevice physical_device, VkMemoryPropertyFlags properties, uint32_t memory_type_bits)
{
	VkPhysicalDeviceMemoryProperties memory_properties;
	vkGetPhysicalDeviceMemoryProperties(physical_device, &memory_properties);

	bool found = false;
	uint32_t type_index = 0;

	for (uint32_t i = 0; i < memory_properties.memoryTypeCount; ++i) {
		bool correct_type = (memory_type_bits & (1 << i)) > 0;
		bool has_properties = (memory_properties.memoryTypes[i].propertyFlags & properties) == properties;
		if (correct_type && has_properties) {
			type_index = i;
			found = true;
			break;
		}
	}
	
	if (!found)
		throw; // TODO error handling;

	return type_index;
}

SharedDeviceMemory Memory::allocate_image_memory(Device* device, VkImage image, VkFormat format, VkMemoryPropertyFlags properties)
{
	VkMemoryRequirements memory_requirements;
	vkGetImageMemoryRequirements(device->get_inner(), image, &memory_requirements);

	VkDeviceSize memory_type_bits = memory_requirements.memoryTypeBits;
	uint32_t allocation_size = memory_requirements.size;

	uint32_t memory_type = find_memory_type(device->get_physical_device(), properties, memory_type_bits);

	VkDeviceMemory memory = allocate_memory(device, allocation_size, memory_type);

	ASSERT_VK_SUCCESS(vkBindImageMemory(device->get_inner(), image, memory, 0));

	return SharedDeviceMemory(device, memory);
}

Vector<SharedDeviceMemory> Memory::allocate_image_memory(Device* device, const Vector<Image>& images, VkMemoryPropertyFlags properties)
{
	VkDeviceSize allocation_size = 0;
	Vector<VkDeviceSize> offsets;
	offsets.reserve(images.get_size());
	uint32_t memory_type_bits = 0;

	// To obey alignment requirements, each offset must be a multiple of its alignment
	// and bigger than the current total allocation size. This is done as such:
	// 	offset = ceil(allocation_size / alignment) * alignment
	for (Image& image : images) {
		VkMemoryRequirements memory_requirements;
		vkGetImageMemoryRequirements(device->get_inner(), image.image, &memory_requirements);
		memory_type_bits |= memory_requirements.memoryTypeBits;
		VkDeviceSize offset;

		if (allocation_size == 0)
			offset = 0;
		else
			offset = get_aligned_size(allocation_size, memory_requirements.alignment);

		offsets.add(offset);

		allocation_size = offset + memory_requirements.size;
	}

	uint32_t memory_type = find_memory_type(device->get_physical_device(), properties, memory_type_bits);

	VkDeviceMemory memory = allocate_memory(device, allocation_size, memory_type);

	for (size_t i = 0; i < images.get_size(); ++i)
		ASSERT_VK_SUCCESS(vkBindImageMemory(device->get_inner(), images[i].image, memory, offsets[i]));

	Vector<SharedDeviceMemory> shared_device_memories{};
	shared_device_memories.reserve(images.get_size());

	shared_device_memories.add(SharedDeviceMemory(device, memory));
	for (size_t i = 1; i < images.get_size(); ++i) {
		shared_device_memories.add(SharedDeviceMemory(shared_device_memories[0], offsets[i]));
	}

	return shared_device_memories;
}

SharedDeviceBuffer Memory::create_device_local_vertex_buffer(Device* device, size_t size, const void* data)
{
	VkDeviceSize allocation_size = static_cast<VkDeviceSize>(size);

	// Create staging buffer & memory.
	VkBuffer staging_buffer = create_buffer(device, 
			allocation_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			{ device->get_queue_families().graphics });

	VkMemoryRequirements staging_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), staging_buffer, &staging_memory_requirements);

	allocation_size = max(allocation_size, staging_memory_requirements.size);

	uint32_t staging_memory_type = find_memory_type(device->get_physical_device(), 
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			staging_memory_requirements.memoryTypeBits);

	VkDeviceMemory staging_memory = allocate_memory(device, allocation_size, staging_memory_type);

	bind_buffer_memory(device, staging_buffer, staging_memory);

	// Create vertex buffer & memory
	VkBuffer vertex_buffer = create_buffer(device,
			allocation_size,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			{ device->get_queue_families().graphics });

	VkMemoryRequirements vertex_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), vertex_buffer, &vertex_memory_requirements);

	uint32_t vertex_memory_type = find_memory_type(device->get_physical_device(),
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			vertex_memory_requirements.memoryTypeBits);

	VkDeviceMemory vertex_memory = allocate_memory(device, allocation_size, vertex_memory_type);

	bind_buffer_memory(device, vertex_buffer, vertex_memory);

	// Copy data to staging buffer.
	copy_to_buffer(device, staging_memory, size, data);

	// Copy staging buffer to vertex buffer.
	CommandBuffer transfer_buffer = device->get_transfer_pool().allocate_command_buffer();

	transfer_buffer.begin();
	transfer_buffer.copy_buffer(staging_buffer, vertex_buffer, size);
	transfer_buffer.end();

	device->get_graphics_queue().submit({ transfer_buffer.get_inner() });
	device->get_graphics_queue().wait();

	transfer_buffer.free(device);

	vkDestroyBuffer(device->get_inner(), staging_buffer, nullptr);
	vkFreeMemory(device->get_inner(), staging_memory, nullptr);

	return SharedDeviceBuffer{ SharedDeviceMemory(device, vertex_memory), Buffer(device, vertex_buffer) };
}

void Memory::create_device_local_vertex_index_buffer(Device* device, size_t vertex_size, const void* vertex_data, size_t index_size, const void* index_data, SharedDeviceBuffer& out_vertex_buffer, SharedDeviceBuffer& out_index_buffer)
{
	VkDeviceSize allocation_size = static_cast<VkDeviceSize>(vertex_size + index_size);

	// Create staging buffer & memory.
	VkBuffer staging_buffer = create_buffer(device, 
			allocation_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			{ device->get_queue_families().graphics });

	VkMemoryRequirements staging_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), staging_buffer, &staging_memory_requirements);

	allocation_size = max(allocation_size, staging_memory_requirements.size);

	uint32_t staging_memory_type = find_memory_type(device->get_physical_device(), 
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			staging_memory_requirements.memoryTypeBits);

	VkDeviceMemory staging_memory = allocate_memory(device, allocation_size, staging_memory_type);

	bind_buffer_memory(device, staging_buffer, staging_memory);

	// Create vertex buffer & memory
	VkBuffer vertex_buffer = create_buffer(device,
			vertex_size,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			{ device->get_queue_families().graphics });

	VkBuffer index_buffer = create_buffer(device,
			index_size,
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			{ device->get_queue_families().graphics });

	VkMemoryRequirements vertex_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), vertex_buffer, &vertex_memory_requirements);

	size_t vertex_allocation_size = max(vertex_size, usize(vertex_memory_requirements.size));

	VkDeviceSize alignment = vertex_memory_requirements.alignment;

	VkMemoryRequirements index_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), index_buffer, &index_memory_requirements);

	size_t index_allocation_size = max(index_size, usize(index_memory_requirements.size));

	uint32_t vertex_index_memory_type = find_memory_type(device->get_physical_device(),
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			vertex_memory_requirements.memoryTypeBits | vertex_memory_requirements.memoryTypeBits);

	VkDeviceMemory vertex_index_memory = allocate_memory(device, get_aligned_size(vertex_allocation_size, alignment) + index_allocation_size, vertex_index_memory_type);

	bind_buffer_memory(device, vertex_buffer, vertex_index_memory);
	bind_buffer_memory(device, index_buffer, vertex_index_memory, get_aligned_size(vertex_allocation_size, alignment));

	// Copy data to staging buffer.
	copy_to_buffer(device, staging_memory, vertex_size, vertex_data);
	copy_to_buffer(device, staging_memory, index_size, index_data, vertex_size);

	// Copy staging buffer to vertex buffer.
	CommandBuffer transfer_buffer = device->get_transfer_pool().allocate_command_buffer();

	transfer_buffer.begin();
	transfer_buffer.copy_buffer(staging_buffer, vertex_buffer, vertex_size);
	transfer_buffer.copy_buffer(staging_buffer, index_buffer, { { vertex_size, 0, index_size } });
	transfer_buffer.end();

	device->get_graphics_queue().submit({ transfer_buffer.get_inner() });
	device->get_graphics_queue().wait();

	transfer_buffer.free(device);

	vkDestroyBuffer(device->get_inner(), staging_buffer, nullptr);
	vkFreeMemory(device->get_inner(), staging_memory, nullptr);

	SharedDeviceMemory mem = SharedDeviceMemory(device, vertex_index_memory);
	out_vertex_buffer = SharedDeviceBuffer{ mem, Buffer(device, vertex_buffer) };
	out_index_buffer = SharedDeviceBuffer{ mem, Buffer(device, index_buffer) };
}

SharedDeviceBuffer Memory::allocate_descriptor_memory(Device* device, VkDeviceSize size, VkBufferUsageFlags buffer_usage, VkMemoryPropertyFlags memory_properties)
{
	VkBuffer buffer = create_buffer(device, size, buffer_usage, VK_SHARING_MODE_EXCLUSIVE, { device->get_queue_families().graphics });

	VkMemoryRequirements buffer_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), buffer, &buffer_memory_requirements);

	uint32_t buffer_memory_type = find_memory_type(device->get_physical_device(), 
			memory_properties,
			buffer_memory_requirements.memoryTypeBits);

	VkDeviceMemory memory = allocate_memory(device, max(size, buffer_memory_requirements.size), buffer_memory_type);

	bind_buffer_memory(device, buffer, memory);

	return SharedDeviceBuffer{ SharedDeviceMemory(device, memory), Buffer(device, buffer) };
}

SharedDeviceBuffer Memory::allocate_descriptor_array_memory(Device* device, VkDeviceSize descriptor_size, VkDeviceSize count, VkBufferUsageFlags buffer_usage, VkMemoryPropertyFlags memory_properties)
{
	VkDeviceSize size = get_descriptor_offset_alignment(device, descriptor_size) * count;
	return Memory::allocate_descriptor_memory(device, size, buffer_usage, memory_properties);
}

VkDeviceMemory Memory::allocate_memory(Device* device, VkDeviceSize allocation_size, uint32_t memory_type_index)
{
	VkMemoryAllocateInfo alloc_info;
	alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	alloc_info.pNext = nullptr;
	alloc_info.allocationSize = allocation_size;
	alloc_info.memoryTypeIndex = memory_type_index;

	VkDeviceMemory memory;
	ASSERT_VK_SUCCESS(vkAllocateMemory(device->get_inner(), &alloc_info, nullptr, &memory));

	return memory;
}

VkBuffer Memory::create_buffer(Device* device, VkDeviceSize size, VkBufferUsageFlags usage, VkSharingMode sharing_mode, const SimpleBuffer<uint32_t>& queue_family_indices)
{
	VkBufferCreateInfo buffer_create_info{};
	buffer_create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	buffer_create_info.size = size;
	buffer_create_info.usage = usage;
	buffer_create_info.sharingMode = sharing_mode;
	buffer_create_info.queueFamilyIndexCount = queue_family_indices.get_size();
	buffer_create_info.pQueueFamilyIndices = queue_family_indices.get_buffer();

	VkBuffer buffer;
	ASSERT_VK_SUCCESS(vkCreateBuffer(device->get_inner(), &buffer_create_info, nullptr, &buffer));

	return buffer;
}

void Memory::bind_buffer_memory(Device* device, VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize offset)
{
	ASSERT_VK_SUCCESS(vkBindBufferMemory(device->get_inner(), buffer, memory, offset));
}

void Memory::copy_to_buffer(Device* device, VkDeviceMemory memory, const SimpleBuffer<uint8_t>& data, VkDeviceSize offset)
{
	copy_to_buffer(device, memory, data.get_size(), data.get_buffer(), offset);
}

void Memory::copy_to_buffer(Device* device, VkDeviceMemory memory, size_t size, const void* data, VkDeviceSize offset)
{
	void* mapped_memory;
	ASSERT_VK_SUCCESS(vkMapMemory(device->get_inner(), memory, offset, size, 0, &mapped_memory));
	std::memcpy(mapped_memory, data, size);
	vkUnmapMemory(device->get_inner(), memory);
}

void Memory::copy_to_image_staged(Device* device, VkImage dst_image, VkImageSubresourceLayers subresource, VkExtent3D extent, size_t size, const void* data)
{
	VkDeviceSize allocation_size = static_cast<VkDeviceSize>(size);

	VkBuffer staging_buffer = create_buffer(
			device,
			allocation_size,
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			{ device->get_queue_families().graphics }
	);

	VkMemoryRequirements staging_memory_requirements;
	vkGetBufferMemoryRequirements(device->get_inner(), staging_buffer, &staging_memory_requirements);

	allocation_size = max(allocation_size, staging_memory_requirements.size);

	uint32_t staging_memory_type = find_memory_type(device->get_physical_device(), 
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			staging_memory_requirements.memoryTypeBits);

	VkDeviceMemory staging_memory = allocate_memory(device, allocation_size, staging_memory_type);

	bind_buffer_memory(device, staging_buffer, staging_memory);

	copy_to_buffer(device, staging_memory, size, data);

	CommandBuffer transfer_buffer = device->get_transfer_pool().allocate_command_buffer();

	transfer_buffer.begin();
	VkBufferImageCopy region{
		.bufferOffset = 0,
		.bufferRowLength = 0,
		.bufferImageHeight = 0,
		.imageSubresource = subresource,
		.imageOffset = {0, 0, 0},
		.imageExtent = extent
	};
	vkCmdCopyBufferToImage(
		transfer_buffer.get_inner(),
		staging_buffer,
		dst_image,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &region
	);

	transfer_buffer.end();

	device->get_graphics_queue().submit({ transfer_buffer.get_inner() });
	device->get_graphics_queue().wait();

	transfer_buffer.free(device);

	vkDestroyBuffer(device->get_inner(), staging_buffer, nullptr);
	vkFreeMemory(device->get_inner(), staging_memory, nullptr);
}

VkDeviceSize Memory::get_descriptor_offset_alignment(Device* device, VkDeviceSize size)
{
	VkPhysicalDeviceProperties properties;
	vkGetPhysicalDeviceProperties(device->get_physical_device(), &properties);

	return get_aligned_size(size, properties.limits.minUniformBufferOffsetAlignment);
}

void Memory::transition_image_layout(Device* device, Sampler& sampler, VkImageLayout old_layout, VkImageLayout new_layout, VkAccessFlags old_access_mask, VkAccessFlags new_access_mask)
{
	CommandBuffer transfer_buffer = device->get_transfer_pool().allocate_command_buffer();

	transfer_buffer.begin();

	VkImageMemoryBarrier barrier{
		.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		.srcAccessMask = old_access_mask,
		.dstAccessMask = new_access_mask,
		.oldLayout = old_layout,
		.newLayout = new_layout,
		.srcQueueFamilyIndex = device->get_queue_families().graphics,
		.dstQueueFamilyIndex = device->get_queue_families().graphics,
		.image = sampler.image,
		.subresourceRange = sampler.subresource_range,
	};

	VkPipelineStageFlags src_stage;
	VkPipelineStageFlags dst_stage;

	if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED)
		src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	else
		LOG_WARNING("Unhandled src layout: %d\n", old_layout);

	if (new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	else if (new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
		dst_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	else
		LOG_WARNING("Unhandled dst layout: %d\n", new_layout)

	vkCmdPipelineBarrier(
		transfer_buffer.get_inner(),
		src_stage, dst_stage,
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);


	transfer_buffer.end();

	device->get_graphics_queue().submit({ transfer_buffer.get_inner() });
	device->get_graphics_queue().wait();

	transfer_buffer.free(device);
}

} // namespace renderer
