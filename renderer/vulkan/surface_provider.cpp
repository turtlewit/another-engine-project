#include <SDL.h>
#include <SDL_video.h>
#include <SDL_vulkan.h>
#include <SDL_surface.h>

#include <vulkan/vulkan.h>

#include "surface_provider.hpp"
#include "instance.hpp"
#include "logger.hpp"
#include "util.hpp"

namespace renderer {

Surface SurfaceProvider::create_surface(Instance* instance)
{
	VkSurfaceKHR surface;

	SDL_Vulkan_CreateSurface(window, instance->get_inner(), &surface);

	VkExtent2D surface_extent;
	int w, h;

	SDL_Vulkan_GetDrawableSize(window, &w, &h);
	surface_extent.width = u32(w);
	surface_extent.height = u32(h);

	return Surface(instance, surface, surface_extent);
}

SimpleBuffer<const char*> SurfaceProvider::get_instance_extensions()
{
	unsigned int instance_extension_count;
	SDL_Vulkan_GetInstanceExtensions(window, &instance_extension_count, nullptr);

	SimpleBuffer<const char*> instance_extensions{};
	instance_extensions.resize(instance_extension_count);
	if (SDL_Vulkan_GetInstanceExtensions(window, &instance_extension_count, instance_extensions.get_buffer()) != SDL_TRUE) {
		LOG_ERROR("Could not get SDL Vulkan Instance Extensions\n");
	}

	return instance_extensions;
}

VkExtent2D SurfaceProvider::get_extent()
{
	int w, h;

	SDL_Vulkan_GetDrawableSize(window, &w, &h);

	return { u32(w), u32(h) };
}

} // namespace renderer
