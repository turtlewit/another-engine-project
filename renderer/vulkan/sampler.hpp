#pragma once

#include <memory.hpp>
#include "util.hpp"

namespace renderer {

class Device;

class Sampler {
public:
	VkImage image;
	VkFormat format;
	VkImageView image_view;
	VkSampler sampler;
	VkImageSubresourceRange subresource_range;
	VkExtent3D extent;

	Sampler() = default;

	Sampler(Device* device, SharedDeviceMemory memory, VkImage image, VkFormat format, VkImageView image_view, VkSampler sampler, VkImageSubresourceRange subresource_range, VkExtent3D extent)
		: image{ image }, format{ format }, image_view{ image_view }, sampler{ sampler }, device{ device }, memory{ memory }, subresource_range{ subresource_range }, extent{ extent }
	{
	}

	Sampler(const Sampler& other) = delete;

	Sampler(Sampler&& other)
		: image{ other.image }, format{ other.format }, image_view{ other.image_view }, sampler{ other.sampler }, device{ other.device }, memory{ other.memory }, subresource_range{ other.subresource_range }
	{
		other.device = nullptr;
	}

	Sampler& operator=(Sampler&& other)
	{
		this->~Sampler();
		::new(this) Sampler(move(other));
		return *this;
	}

	void copy_to_image(size_t size, void* data, VkImageLayout final_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VkAccessFlags = VK_ACCESS_SHADER_READ_BIT);

	~Sampler();
private:
	Device* device;
	SharedDeviceMemory memory;
};

class SamplerBuilder {
public:
	SamplerBuilder& with_extent_2d(uint32_t width, uint32_t height);
	SamplerBuilder& with_format(VkFormat format);
	SamplerBuilder& with_tiling(VkImageTiling tiling);
	SamplerBuilder& with_initial_layout(VkImageLayout layout);
	SamplerBuilder& with_usage(VkImageUsageFlags flags);
	SamplerBuilder& with_sharing_mode(VkSharingMode mode);
	SamplerBuilder& with_samples(VkSampleCountFlagBits flags);
	SamplerBuilder& with_aspect(VkImageAspectFlagBits flags);

	// sampler things
	SamplerBuilder& with_min_mag_filter(VkFilter min_filter, VkFilter mag_filter);
	SamplerBuilder& with_address_mode(VkSamplerAddressMode mode);
	SamplerBuilder& with_anisotropy(float max_samples);
	SamplerBuilder& with_border_color(VkBorderColor color);
	SamplerBuilder& with_unnormalized_coordinates();

	SamplerBuilder& with_mip_mapping();

	Sampler build(Device* device);

	SamplerBuilder();

	VkImageCreateInfo image_create_info;
	VkImageViewCreateInfo image_view_create_info;
	VkSamplerCreateInfo sampler_create_info;
};

} // namespace renderer
