#include <cstdlib>

#include <vulkan/vulkan.h>

#include "logger.hpp"
#include "device.hpp"
#include "instance.hpp"
#include "swapchain.hpp"
#include "util.hpp"

namespace renderer {

Device::Device(Instance* instance, VkPhysicalDevice physical_device, VkDevice device, DeviceQueueFamilyIndices queue_families, uint32_t max_frames_in_flight)
	: instance{ instance }, physical_device{ physical_device }, device{ device }, queue_families{ queue_families }
	, swapchain{ new Swapchain(Swapchain::create_swapchain(this, instance->get_surface(), max_frames_in_flight)) }
	, graphics_queue{ DeviceQueue::get_device_queue(this, queue_families.graphics, 0) }
	, present_queue{ DeviceQueue::get_device_queue(this, queue_families.present, 0) }
	, transfer_pool{ CommandPool::create_command_pool(this, queue_families.graphics) }
	, max_frames_in_flight{ max_frames_in_flight }
{
	Logger::log_info("Swapchain Image Count: %d\n      Max Frames In Flight: %d\n", swapchain->get_image_count(), max_frames_in_flight);
}


Device Device::create_device(Surface* surface, uint32_t max_frames_in_flight)
{
	Instance* instance = surface->get_instance();
	VkPhysicalDevice physical_device;

	// Pick Physical Device
	{
		SimpleBuffer<VkPhysicalDevice> physical_devices = instance->enumerate_physical_devices();

		for (size_t i = 0; i < physical_devices.get_size(); ++i) {
			VkPhysicalDevice pd = physical_devices.get_buffer()[i];

			VkPhysicalDeviceProperties properties;
			vkGetPhysicalDeviceProperties(pd, &properties);

			if (properties.apiVersion > VK_MAKE_VERSION(1, 2, 0)) {
				physical_device = pd;
				Logger::log_info("Device Properties:\n      API Version:    %d.%d.%d\n      Driver Version: %d.%d.%d\n      Name:           %s\n", 
						VK_VERSION_MAJOR(properties.apiVersion), VK_VERSION_MINOR(properties.apiVersion), VK_VERSION_PATCH(properties.apiVersion),
						VK_VERSION_MAJOR(properties.driverVersion), VK_VERSION_MINOR(properties.driverVersion), VK_VERSION_PATCH(properties.driverVersion),
						properties.deviceName
				);
				break;
			}
		}

		if (physical_device == VK_NULL_HANDLE) {
			LOG_ERROR("No suitable physical device found.\nPlease ensure your graphics drivers are up to date.\n");
			exit(1);
		}
	}

	DeviceQueueFamilyIndices queue_families{};

	// Create Queue Families
	{
		uint32_t queue_family_property_count;
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_property_count, nullptr);

		SimpleBuffer<VkQueueFamilyProperties> queue_family_properties{};
		queue_family_properties.resize(queue_family_property_count);
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_property_count, queue_family_properties.get_buffer());

		bool found_graphics = false;
		bool found_present = false;

		for (uint32_t current_index = 0; current_index < queue_family_property_count; ++current_index) {
			VkQueueFamilyProperties props = queue_family_properties.get_buffer()[current_index];

			bool supports_graphics = (props.queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT;
			VkBool32 supports_presentation;
			vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, current_index, surface->get_inner(), &supports_presentation);

			if (supports_graphics && !found_graphics) {
				queue_families.graphics = current_index;
				found_graphics = true;
			}

			if (supports_presentation && !found_present) {
				queue_families.present = current_index;
				found_present = true;
			}

			if (found_graphics && found_present)
				break;
		}

		if (!(found_graphics && found_present)) {
			LOG_ERROR("Could not find queue queue families.\n");
			exit(1);
		}
	}

	VkDevice vk_device;

	// Create Device
	{
		Vector<const char*> device_extensions{};
		device_extensions.add("VK_KHR_swapchain");

		Vector<VkExtensionProperties> extension_properties{};
		uint32_t extension_property_count;

		ASSERT_VK_SUCCESS(vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &extension_property_count, nullptr));
		extension_properties.resize_uninitialized(extension_property_count);
		ASSERT_VK_SUCCESS(vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &extension_property_count, extension_properties.get_buffer()));

		//bool found = false;
		//for (const auto& extension : extension_properties) {
		//	if (strcmp("VK_AMD_mixed_attachment_samples", extension.extensionName) == 0) {
		//		found = true;
		//		device_extensions.add("VK_AMD_mixed_attachment_samples");
		//		break;
		//	} else if (strcmp("VK_NV_framebuffer_mixed_samples", extension.extensionName) == 0) {
		//		found = true;
		//		device_extensions.add("VK_NV_framebuffer_mixed_samples");
		//		break;
		//	}
		//}

		//if (!found) {
		//	LOG_WARNING("Mixed framebuffer / attachment samples unsupported. App may crash if multisampling is enabled...");
		//}

		Logger::log_info("Enabled extensions:\n");
		for (uint32_t i = 0; i < device_extensions.get_size(); ++i) {
			Logger::print_info("      %s\n", device_extensions[i]);
		}

		uint32_t queue_count;
		const float queue_priorities[1] = { 1.0f };
		SimpleBuffer<VkDeviceQueueCreateInfo> queue_create_infos;

		VkDeviceQueueCreateInfo queue_create_info_common{};
		queue_create_info_common.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queue_create_info_common.queueCount = 1;
		queue_create_info_common.pQueuePriorities = queue_priorities;

		if (queue_families.graphics != queue_families.present) {
			queue_count = 2;

			VkDeviceQueueCreateInfo graphics_info = queue_create_info_common;
			graphics_info.queueFamilyIndex = queue_families.graphics;

			VkDeviceQueueCreateInfo present_info = queue_create_info_common;
			present_info.queueFamilyIndex = queue_families.present;

			queue_create_infos = { graphics_info, present_info };

		} else {
			queue_count = 1;

			VkDeviceQueueCreateInfo queue_create_info = queue_create_info_common;
			queue_create_info.queueFamilyIndex = queue_families.graphics;

			queue_create_infos = { queue_create_info };
		}

		VkDeviceCreateInfo device_create_info{};
		device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		device_create_info.queueCreateInfoCount = queue_count;
		device_create_info.pQueueCreateInfos = queue_create_infos.get_buffer();
		device_create_info.enabledExtensionCount = device_extensions.get_size();
		device_create_info.ppEnabledExtensionNames = device_extensions.get_buffer();

		Logger::log_info("Enabled features:\n");

#define PRINT_FEATURE(feature) Logger::print_info("      " #feature "\n"); feature

		VkPhysicalDeviceFeatures features{};
		PRINT_FEATURE(features.vertexPipelineStoresAndAtomics = VK_TRUE);
		PRINT_FEATURE(features.fragmentStoresAndAtomics = VK_TRUE);
		PRINT_FEATURE(features.shaderInt64 = VK_TRUE);
		PRINT_FEATURE(features.samplerAnisotropy = VK_TRUE);
		PRINT_FEATURE(features.sampleRateShading = VK_TRUE);
		PRINT_FEATURE(features.fillModeNonSolid = VK_TRUE);

		VkPhysicalDeviceVulkan11Features features11{};
		features11.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
		PRINT_FEATURE(features11.storageBuffer16BitAccess = VK_TRUE);

		VkPhysicalDeviceVulkan12Features features12{};
		features12.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
		features12.pNext = &features11;
		PRINT_FEATURE(features12.shaderFloat16 = VK_TRUE);
		PRINT_FEATURE(features12.shaderBufferInt64Atomics = VK_TRUE);

		VkPhysicalDeviceFeatures2 features2;
		features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
		features2.features = features;
		features2.pNext = &features12;

		device_create_info.pNext = &features2;

		VkResult result;
		ASSERT_VK_SUCCESS(vkCreateDevice(physical_device, &device_create_info, nullptr, &vk_device));
	}

	return Device(instance, physical_device, vk_device, queue_families, max_frames_in_flight);
}

VkSurfaceCapabilitiesKHR Device::get_physical_device_surface_capabilities(Surface* surface)
{
	VkSurfaceCapabilitiesKHR capabilities;
	ASSERT_VK_SUCCESS(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface->get_inner(), &capabilities));
	return capabilities;
}

SimpleBuffer<VkSurfaceFormatKHR> Device::get_physical_device_surface_formats(Surface* surface)
{
	uint32_t surface_format_count;
	ASSERT_VK_SUCCESS(vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface->get_inner(), &surface_format_count, nullptr));

	SimpleBuffer<VkSurfaceFormatKHR> surface_formats{};
	surface_formats.resize(surface_format_count);

	ASSERT_VK_SUCCESS(vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface->get_inner(), &surface_format_count, surface_formats.get_buffer()));

	return surface_formats;
}

SimpleBuffer<VkPresentModeKHR> Device::get_physical_device_surface_present_modes(Surface* surface)
{
	uint32_t present_mode_count;
	ASSERT_VK_SUCCESS(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface->get_inner(), &present_mode_count, nullptr));

	SimpleBuffer<VkPresentModeKHR> present_modes{};
	present_modes.resize(present_mode_count);

	ASSERT_VK_SUCCESS(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface->get_inner(), &present_mode_count, present_modes.get_buffer()));

	return present_modes;
}

void Device::recreate_swapchain()
{
	{
		auto drop = move(transfer_pool);
	}

	delete swapchain;

	swapchain = new Swapchain(Swapchain::create_swapchain(this, instance->get_surface(), max_frames_in_flight));
}

Device::~Device()
{
	drop(transfer_pool);

	for (RenderPass* pass : render_passes)
		delete pass;

	delete swapchain;
	vkDestroyDevice(device, nullptr);
}

} // namespace renderer
