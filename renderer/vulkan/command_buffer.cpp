#include <simple_buffer.hpp>

#include "command_buffer.hpp"
#include "device.hpp"
#include "render_pass.hpp"
#include "swapchain.hpp"

namespace renderer {

void CommandBuffer::begin(VkCommandBufferUsageFlags usage)
{
	VkCommandBufferBeginInfo command_buffer_begin_info{};
	command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	command_buffer_begin_info.flags = usage;

	// TODO error checking
	vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);
}

void CommandBuffer::begin_render_pass(RenderPass* render_pass, uint32_t framebuffer)
{
	Swapchain* swapchain = render_pass->get_device()->get_swapchain();
	begin_render_pass(render_pass, framebuffer, swapchain->get_extent());
}

void CommandBuffer::begin_render_pass(RenderPass* render_pass, uint32_t framebuffer, VkExtent2D render_area)
{
	VkRenderPassBeginInfo render_pass_begin_info{};
	render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	render_pass_begin_info.renderPass = render_pass->get_inner();
	render_pass_begin_info.framebuffer = render_pass->get_framebuffers()[framebuffer];
	render_pass_begin_info.renderArea.offset = { 0, 0 };
	render_pass_begin_info.renderArea.extent = render_area;
	render_pass_begin_info.clearValueCount = render_pass->get_clear_values().get_size();
	render_pass_begin_info.pClearValues = render_pass->get_clear_values().get_buffer();

	vkCmdBeginRenderPass(command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
}

void CommandBuffer::end_render_pass()
{
	vkCmdEndRenderPass(command_buffer);
}

void CommandBuffer::copy_buffer(VkBuffer src, VkBuffer dst, const SimpleBuffer<VkBufferCopy>& regions)
{
	vkCmdCopyBuffer(command_buffer, src, dst, regions.get_size(), regions.get_buffer());
}

void CommandBuffer::copy_buffer(VkBuffer src, VkBuffer dst, VkDeviceSize size)
{
	copy_buffer(src, dst, { {0, 0, size} });
}

void CommandBuffer::end()
{
	// TODO error checking
	vkEndCommandBuffer(command_buffer);
}

void CommandBuffer::free(Device* device)
{
	vkFreeCommandBuffers(device->get_inner(), pool->get_inner(), 1, &command_buffer);
}

CommandPool CommandPool::create_command_pool(Device* device, uint32_t queue_family_index)
{
	VkCommandPoolCreateInfo command_pool_create_info{};
	command_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	//command_pool_create_info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
	command_pool_create_info.queueFamilyIndex = queue_family_index;

	VkCommandPool command_pool;
	vkCreateCommandPool(device->get_inner(), &command_pool_create_info, nullptr, &command_pool);

	return CommandPool(device, command_pool);
}

CommandBuffer CommandPool::allocate_command_buffer(VkCommandBufferLevel level)
{
	VkCommandBufferAllocateInfo command_buffer_allocate_info{};
	command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	command_buffer_allocate_info.commandPool = command_pool;
	command_buffer_allocate_info.level = level;
	command_buffer_allocate_info.commandBufferCount = 1;
	
	VkCommandBuffer command_buffer;
	vkAllocateCommandBuffers(device->get_inner(), &command_buffer_allocate_info, &command_buffer);

	return CommandBuffer(this, command_buffer, reset_id);
}

Vector<CommandBuffer> CommandPool::allocate_command_buffers(uint32_t count, VkCommandBufferLevel level)
{
	VkCommandBufferAllocateInfo command_buffer_allocate_info{};
	command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	command_buffer_allocate_info.commandPool = command_pool;
	command_buffer_allocate_info.level = level;
	command_buffer_allocate_info.commandBufferCount = count;
	
	SimpleBuffer<VkCommandBuffer> vk_command_buffers;
	vk_command_buffers.resize(count);
	vkAllocateCommandBuffers(device->get_inner(), &command_buffer_allocate_info, vk_command_buffers.get_buffer());

	Vector<CommandBuffer> command_buffers{};
	command_buffers.reserve(count);

	for (size_t i = 0; i < vk_command_buffers.get_size(); ++i)
	{
		command_buffers.add(CommandBuffer(this, vk_command_buffers.get_buffer()[i], reset_id));
	}

	return command_buffers;
}

CommandBuffer CommandPool::begin_single_use_command_buffer()
{
	CommandBuffer cb = allocate_command_buffer(VK_COMMAND_BUFFER_LEVEL_PRIMARY);
	cb.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	return cb;
}

void CommandPool::reset(VkCommandPoolResetFlags flags)
{
	vkResetCommandPool(device->get_inner(), command_pool, flags);
}

CommandPool::~CommandPool()
{
	if (command_pool != VK_NULL_HANDLE)
		vkDestroyCommandPool(device->get_inner(), command_pool, nullptr);
}

} // namespace renderer
