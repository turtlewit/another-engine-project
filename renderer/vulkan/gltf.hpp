#pragma once

#include <glm/glm.hpp>
#include <glm/ext/quaternion_float.hpp>
#include <vulkan/vulkan.h>
#include <vector.hpp>

#include "memory.hpp"
#include "aabb.hpp"

extern "C" {
	struct cgltf_data;
	struct cgltf_mesh;
}

namespace renderer {

class Device;
class Gltf;

class GltfMesh {
public:
	GltfMesh(Gltf* gltf, cgltf_mesh* mesh) : gltf{ gltf }, mesh{ mesh }
	{
	}

	GltfMesh(const GltfMesh&) = delete;
	GltfMesh(GltfMesh&&) = default;

	uint32_t get_vertex_count();
	SharedDeviceBuffer get_vertex_buffer();

	uint32_t get_index_count();
	SharedDeviceBuffer get_index_buffer();

	VkIndexType get_index_type();

	glm::vec3 get_position();
	glm::quat get_rotation();
	glm::vec4 get_color();
	float get_metallic();
	float get_roughness();
	bool has_transmission();
	AABB get_aabb();

	bool get_albedo_texture(size_t& index);
	bool get_normal_texture(size_t& index);

public:
	Gltf* gltf;
	cgltf_mesh* mesh;

	SharedDeviceBuffer vertex_buffer{};
	SharedDeviceBuffer index_buffer{};

	bool initialized = false;

	void create_vertex_index_buffer();
};

class Gltf {
	friend GltfMesh;
public:
	Gltf(Device* device, const char* path);

	const Vector<GltfMesh>& get_meshes() { return meshes; }
	void get_textures(Vector<Sampler>& textures, Vector<size_t>& indices);

	~Gltf();
private:
	Device* device;
	const char* path;
	cgltf_data* data;

	Vector<GltfMesh> meshes{};
};

} // namespace renderer
