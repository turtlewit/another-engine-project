#pragma once

#include <simple_buffer.hpp>

#include <vulkan/vulkan.h>

#include "render_pass.hpp"
#include "device_queue.hpp"
#include "command_buffer.hpp"

namespace renderer {

class Instance;
class Surface;
class Swapchain;

struct DeviceQueueFamilyIndices {
	uint32_t graphics;
	uint32_t present;
};

class Device {
public:
	static Device create_device(Surface* surface, uint32_t max_frames_in_flight);

	VkDevice get_inner() { return device; }
	VkPhysicalDevice get_physical_device() { return physical_device; }
	Instance* get_instance() { return instance; }
	DeviceQueueFamilyIndices get_queue_families() { return queue_families; }
	Swapchain* get_swapchain() { return swapchain; }
	Vector<RenderPass*>& get_render_passes() { return render_passes; }
	DeviceQueue& get_graphics_queue() { return graphics_queue; }
	DeviceQueue& get_present_queue() { return present_queue; }
	CommandPool& get_transfer_pool() { return transfer_pool; }

	VkSurfaceCapabilitiesKHR get_physical_device_surface_capabilities(Surface* surface);
	SimpleBuffer<VkSurfaceFormatKHR> get_physical_device_surface_formats(Surface* surface);
	SimpleBuffer<VkPresentModeKHR> get_physical_device_surface_present_modes(Surface* surface);

	void recreate_swapchain();

	Device(Instance* instance, VkPhysicalDevice physical_device, VkDevice device, DeviceQueueFamilyIndices queue_families, uint32_t max_frames_in_flight);

	~Device();

private:
	Instance* instance;

	VkPhysicalDevice physical_device;
	VkDevice device;
	DeviceQueueFamilyIndices queue_families;

	Swapchain* swapchain;
	Vector<RenderPass*> render_passes;

	DeviceQueue graphics_queue;
	DeviceQueue present_queue;

	CommandPool transfer_pool;
	uint32_t max_frames_in_flight;
};

} // namespace renderer
