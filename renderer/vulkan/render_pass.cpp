#include "render_pass.hpp"
#include "device.hpp"
#include "swapchain.hpp"
#include "logger.hpp"

namespace renderer {

RenderPassAttachment RenderPassAttachment::create_swapchain_color_attachment(Swapchain* swapchain)
{
	// Not initialized via barrier, so the initial layout is undefined.
	// Meant to be presented to the swapchain, so the final layout needs to be
	// PRESENT_SRC_KHR
	return create_color_attachment(swapchain->get_surface_format().format, swapchain->get_image_views(), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
}

RenderPassAttachment RenderPassAttachment::create_color_attachment(VkFormat format, const Vector<VkImageView>& image_views, VkImageLayout initial_layout, VkImageLayout final_layout, VkBool32 may_alias, VkSampleCountFlagBits sample_count, VkAttachmentLoadOp load_op, VkAttachmentStoreOp store_op)
{
	VkAttachmentDescription attachment_description;
	attachment_description.flags = 0;
	if (may_alias)
		attachment_description.flags = VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT;
	attachment_description.format = format;
	attachment_description.samples = sample_count;

	attachment_description.loadOp = load_op;
	attachment_description.storeOp = store_op;

	// Not a stencil, so we don't care.
	attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

	attachment_description.initialLayout = initial_layout;
	attachment_description.finalLayout = final_layout;

	// Attachment layout should be COLOR_ATTACHMENT_OPTIMAL
	return RenderPassAttachment{ attachment_description,
	                             image_views, RenderPassAttachmentType::COLOR };
}

RenderPassAttachment RenderPassAttachment::create_depth_attachment(VkFormat format, VkImageView image_view, VkImageLayout initial_layout, VkImageLayout final_layout, VkBool32 may_alias, VkSampleCountFlagBits sample_count, VkAttachmentLoadOp load_op, VkAttachmentStoreOp store_op)
{
	VkAttachmentDescription attachment_description;
	attachment_description.flags = 0;
	if (may_alias)
		attachment_description.flags = VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT;
	attachment_description.format = format;
	attachment_description.samples = sample_count;

	attachment_description.loadOp = load_op;
	attachment_description.storeOp = store_op;

	attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

	attachment_description.initialLayout = initial_layout;
	attachment_description.finalLayout = final_layout;

	Vector<VkImageView> image_views;
	image_views.add(image_view);
	// Attachment layout should be COLOR_ATTACHMENT_OPTIMAL
	return RenderPassAttachment{ attachment_description, 
	                             image_views, RenderPassAttachmentType::DEPTH_STENCIL };
}

RenderPassAttachment RenderPassAttachment::create_input_attachment(VkFormat format, const Vector<VkImageView>& image_views, VkImageLayout initial_layout, VkImageLayout final_layout, VkBool32 may_alias, VkSampleCountFlagBits sample_count, VkAttachmentLoadOp load_op, VkAttachmentStoreOp store_op)
{
	VkAttachmentDescription attachment_description;
	attachment_description.flags = 0;
	if (may_alias)
		attachment_description.flags = VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT;
	attachment_description.format = format;
	attachment_description.samples = sample_count;

	attachment_description.loadOp = load_op;
	attachment_description.storeOp = store_op;

	attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

	attachment_description.initialLayout = initial_layout;
	attachment_description.finalLayout = final_layout;

	// Attachment layout should be COLOR_ATTACHMENT_OPTIMAL
	return RenderPassAttachment{ attachment_description,
	                             image_views, RenderPassAttachmentType::INPUT };
}

RenderPassBuilder& RenderPassBuilder::with_subpasses(size_t num)
{
	subpasses.resize(num);
	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_attachment(RenderPassAttachment attachment)
{
	attachments.add(attachment);
	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_depth_attachment(RenderPassAttachment attachment)
{
	attachments.add(attachment);

	VkSubpassDependency dependency{};
	
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	dependency.dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	dependency.dependencyFlags = 0;

	dependencies.add(dependency);
	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_input_attachment_reference(uint32_t subpass_idx, VkAttachmentReference attachment)
{
	auto& subpass = subpasses[subpass_idx];

	subpass.input_attachment_references.add(attachment);
	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_color_attachment_reference(uint32_t subpass_idx, VkAttachmentReference attachment)
{
	auto& subpass = subpasses[subpass_idx];

	subpass.color_attachment_references.add(attachment);

	clear_values.resize(attachment.attachment + 1);
	clear_values[attachment.attachment] = VkClearValue{ .color = VkClearColorValue{ 0.0f, 0.0f, 0.0f, 1.0f } };

	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_depth_attachment_reference(uint32_t subpass_idx, VkAttachmentReference attachment)
{
	auto& subpass = subpasses[subpass_idx];

	subpass.depth_attachment_references.add(attachment);

	clear_values.resize(attachment.attachment + 1);
	clear_values[attachment.attachment] = VkClearValue{ .depthStencil = VkClearDepthStencilValue{ 1.0f, 0 } };

	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_resolve_attachment_reference(uint32_t subpass_idx, VkAttachmentReference attachment)
{
	auto& subpass = subpasses[subpass_idx];

	subpass.resolve_attachment_references.add(attachment);

	clear_values.resize(attachment.attachment + 1);
	clear_values[attachment.attachment] = VkClearValue{ .color = VkClearColorValue{ 0.0f, 0.0f, 0.0f, 1.0f } };

	return *this;
}

RenderPassBuilder& RenderPassBuilder::with_dependency(VkSubpassDependency dependency)
{
	dependencies.add(dependency);
	return *this;
}

RenderPass RenderPassBuilder::build(Device* device)
{
	Swapchain* swapchain = device->get_swapchain();
	return build(device, swapchain->get_extent().width, swapchain->get_extent().height, swapchain->get_image_count());
}

RenderPass RenderPassBuilder::build(Device* device, uint32_t framebuffer_width, uint32_t framebuffer_height, uint32_t framebuffer_count)
{
	Vector<VkAttachmentDescription> attachment_descriptions{};
	attachment_descriptions.reserve(attachments.get_size());

	for (auto& attachment : attachments)
		attachment_descriptions.add(attachment.attachment_description);

	Vector<VkSubpassDescription> subpass_descriptions{};
	subpass_descriptions.reserve(subpasses.get_size());
	for (auto& subpass : subpasses) {
		subpass.subpass_description.inputAttachmentCount = subpass.input_attachment_references.get_size();
		subpass.subpass_description.pInputAttachments = subpass.input_attachment_references.get_buffer();
		subpass.subpass_description.colorAttachmentCount = subpass.color_attachment_references.get_size();
		subpass.subpass_description.pColorAttachments = subpass.color_attachment_references.get_buffer();
		subpass.subpass_description.pDepthStencilAttachment = subpass.depth_attachment_references.get_buffer();
		subpass.subpass_description.pResolveAttachments = subpass.resolve_attachment_references.get_buffer();
		subpass_descriptions.add(subpass.subpass_description);
	}

	VkRenderPassCreateInfo render_pass_create_info{};
	render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	render_pass_create_info.pNext = nullptr;
	render_pass_create_info.flags = 0;
	render_pass_create_info.attachmentCount = attachment_descriptions.get_size();
	render_pass_create_info.pAttachments = attachment_descriptions.get_buffer();
	render_pass_create_info.subpassCount = subpass_descriptions.get_size();
	render_pass_create_info.pSubpasses = subpass_descriptions.get_buffer();
	render_pass_create_info.dependencyCount = dependencies.get_size();
	render_pass_create_info.pDependencies = dependencies.get_buffer();

	VkRenderPass render_pass;
	ASSERT_VK_SUCCESS(vkCreateRenderPass(device->get_inner(), &render_pass_create_info, nullptr, &render_pass));

	Vector<VkFramebuffer> framebuffers{};
	framebuffers.reserve(framebuffer_count);

	for (size_t i = 0; i < framebuffer_count; ++i) {
		Vector<VkImageView> views{};

		for (auto& attachment : attachments) {
			views.add(attachment.image_views[i % attachment.image_views.get_size()]);
		}

		VkFramebufferCreateInfo framebuffer_create_info{};
		framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebuffer_create_info.pNext = nullptr;
		framebuffer_create_info.flags = 0;
		framebuffer_create_info.renderPass = render_pass;
		framebuffer_create_info.attachmentCount = views.get_size();
		framebuffer_create_info.pAttachments = views.get_buffer();
		framebuffer_create_info.width = framebuffer_width;
		framebuffer_create_info.height = framebuffer_height;
		framebuffer_create_info.layers = 1;

		VkFramebuffer framebuffer;
		ASSERT_VK_SUCCESS(vkCreateFramebuffer(device->get_inner(), &framebuffer_create_info, nullptr, &framebuffer));

		framebuffers.add(framebuffer);
	}

	return RenderPass(device, render_pass, RenderPassType::PRIMARY, framebuffers, clear_values);
}

RenderPass::~RenderPass()
{
	for (VkFramebuffer& framebuffer : framebuffers)
		vkDestroyFramebuffer(device->get_inner(), framebuffer, nullptr);

	vkDestroyRenderPass(device->get_inner(), render_pass, nullptr);
}


} // namespace renderer
