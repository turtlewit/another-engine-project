#include "descriptor.hpp"
#include "device.hpp"

namespace renderer {

void DescriptorSet::write_uniform_buffer(uint32_t binding, 
                                         uint32_t array_element, 
                                         VkBuffer buffer, 
                                         VkDeviceSize offset, 
                                         VkDeviceSize range)
{
	write_descriptors(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, binding, array_element, 1, {{ buffer, offset, range }});
}

void DescriptorSet::write_storage_buffer(uint32_t binding,
                                         uint32_t array_element, 
                                         VkBuffer buffer, 
                                         VkDeviceSize offset, 
                                         VkDeviceSize range)
{
	write_descriptors(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, binding, array_element, 1, {{ buffer, offset, range }});
}

void DescriptorSet::write_sampler(uint32_t binding,
                                  uint32_t array_element, 
                                  VkDescriptorImageInfo image_info)
{
	write_descriptors(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, binding, array_element, 1, {}, { image_info });
}

void DescriptorSet::write_storage_buffer(uint32_t binding,
                                         uint32_t array_element, 
                                         VkDescriptorBufferInfo buffer_info,
                                         VkBuffer buffer,
                                         VkDeviceSize offset,
                                         VkDeviceSize range)
{
	write_descriptors(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, binding, array_element, 1, {{ buffer, offset, range }}, {});
}

void DescriptorSet::write_descriptors(VkDescriptorType descriptor_type,
                                     uint32_t binding,
                                     uint32_t array_element, 
                                     uint32_t descriptor_count, 
                                     const SimpleBuffer<VkDescriptorBufferInfo>& buffer_info,
                                     const SimpleBuffer<VkDescriptorImageInfo>& image_info)
{
	VkWriteDescriptorSet write{};
	write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	write.dstSet = descriptor_set;
	write.dstBinding = binding;
	write.dstArrayElement = array_element;
	write.descriptorCount = descriptor_count;
	write.descriptorType = descriptor_type;
	write.pBufferInfo = buffer_info.get_buffer();
	write.pImageInfo = image_info.get_buffer();

	vkUpdateDescriptorSets(device->get_inner(), 1, &write, 0, nullptr);
}

DescriptorPool* DescriptorPool::create_descriptor_pool(Device* device, uint32_t max_sets, const SimpleBuffer<VkDescriptorPoolSize>& descriptor_pool_sizes)
{
	VkDescriptorPoolCreateInfo descriptor_pool_create_info{};
	descriptor_pool_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descriptor_pool_create_info.maxSets = max_sets;
	descriptor_pool_create_info.poolSizeCount = descriptor_pool_sizes.get_size();
	descriptor_pool_create_info.pPoolSizes = descriptor_pool_sizes.get_buffer();

	VkDescriptorPool descriptor_pool;
	vkCreateDescriptorPool(device->get_inner(), &descriptor_pool_create_info, nullptr, &descriptor_pool);

	return new DescriptorPool(device, descriptor_pool);
}

Vector<DescriptorSet> DescriptorPool::allocate_descriptor_sets(const Vector<VkDescriptorSetLayout>& layouts)
{
	VkDescriptorSetAllocateInfo allocate_info{};
	allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocate_info.descriptorPool = descriptor_pool;
	allocate_info.descriptorSetCount = layouts.get_size();
	allocate_info.pSetLayouts = layouts.get_buffer();

	Vector<VkDescriptorSet> vk_descriptor_sets;
	vk_descriptor_sets.resize_uninitialized(layouts.get_size());
	vkAllocateDescriptorSets(device->get_inner(), &allocate_info, vk_descriptor_sets.get_buffer());

	Vector<DescriptorSet> descriptor_sets{};
	descriptor_sets.reserve(vk_descriptor_sets.get_size());

	for (size_t i = 0; i < vk_descriptor_sets.get_size(); ++i) {
		descriptor_sets.add(DescriptorSet{ device, vk_descriptor_sets[i] });
	}

	return descriptor_sets;
}

DescriptorPool::~DescriptorPool()
{
	if (descriptor_pool != VK_NULL_HANDLE)
		vkDestroyDescriptorPool(device->get_inner(), descriptor_pool, nullptr);
}

} // namespace renderer
