#pragma once

#include <vulkan/vulkan.h>

#include <simple_buffer.hpp>
#include <vector.hpp>

namespace renderer {

class Device;

class DescriptorSet {
public:
	static inline SimpleBuffer<VkDescriptorSet> get_raw_descriptor_set_array(const Vector<DescriptorSet>& sets) {
		SimpleBuffer<VkDescriptorSet> raw_sets;
		raw_sets.resize(sets.get_size());

		for (size_t i = 0; i < sets.get_size(); ++i) {
			raw_sets.get_buffer()[i] = sets[i].descriptor_set;
		}

		return raw_sets;
	}

	VkDescriptorSet& get_inner() { return descriptor_set; }

	void write_uniform_buffer(uint32_t binding, 
	                          uint32_t array_element, 
	                          VkBuffer buffer, 
	                          VkDeviceSize offset, 
	                          VkDeviceSize range);

	void write_storage_buffer(uint32_t binding,
	                          uint32_t array_element, 
	                          VkBuffer buffer, 
	                          VkDeviceSize offset, 
	                          VkDeviceSize range);

	void write_sampler(uint32_t binding,
	                   uint32_t array_element,
	                   VkDescriptorImageInfo image_info);

	void write_storage_buffer(uint32_t binding,
	                          uint32_t array_element,
	                          VkDescriptorBufferInfo buffer_info,
	                          VkBuffer buffer,
	                          VkDeviceSize offset,
	                          VkDeviceSize range);

	void write_descriptors(VkDescriptorType descriptor_type,
	                       uint32_t binding,
	                       uint32_t array_element, 
	                       uint32_t descriptor_count, 
	                       const SimpleBuffer<VkDescriptorBufferInfo>& buffer_info,
	                       const SimpleBuffer<VkDescriptorImageInfo>& image_info = SimpleBuffer<VkDescriptorImageInfo>{});

	void free();

	DescriptorSet(Device* device, VkDescriptorSet descriptor_set)
		: device{ device }, descriptor_set{ descriptor_set }
	{
	}

private:
	Device* device;

	VkDescriptorSet descriptor_set;
};

class DescriptorPool {
public:
	static DescriptorPool* create_descriptor_pool(Device* device, uint32_t max_sets, const SimpleBuffer<VkDescriptorPoolSize>& descriptor_pool_sizes);

	Vector<DescriptorSet> allocate_descriptor_sets(const Vector<VkDescriptorSetLayout>& layouts);

	VkDescriptorPool get_inner() { return descriptor_pool; }

	DescriptorPool()
		: device{ nullptr }, descriptor_pool{ VK_NULL_HANDLE }
	{
	}

	DescriptorPool(Device* device, VkDescriptorPool descriptor_pool)
		: device{ device }, descriptor_pool{ descriptor_pool }
	{
	}

	DescriptorPool(DescriptorPool&& from)
		: device{ from.device }, descriptor_pool{ from.descriptor_pool }
	{
		from.descriptor_pool = VK_NULL_HANDLE;
	}

	~DescriptorPool();

private:
	Device* device;

	VkDescriptorPool descriptor_pool;
};

} // namespace renderer
