#pragma once

#include <atomic>
#include <cmath>

#include <vulkan/vulkan.h>
#include <vector.hpp>
#include <simple_buffer.hpp>

#include "image.hpp"
#include "util.hpp"

namespace renderer {

class Device;
class Sampler;

class SharedDeviceMemory {
public:
	VkDeviceMemory get_memory() { return data->memory; }
	VkDeviceSize get_offset() { return offset; }

	SharedDeviceMemory()
		: device{ nullptr }, data{ nullptr }, offset{ 0 }
	{
	}

	SharedDeviceMemory(Device* device, VkDeviceMemory memory)
		: device{ device }, data{ nullptr }, offset{ 0 }
	{
		data = new Data{ 1, memory };
	}

	SharedDeviceMemory(const SharedDeviceMemory& from)
		: device{ from.device }, data{ from.data }, offset{ 0 }
	{
		++data->references;
	}

	SharedDeviceMemory(const SharedDeviceMemory& clone_from, VkDeviceSize offset)
		: device{ clone_from.device }, data{ clone_from.data }, offset{ offset }
	{
		++data->references;
	}

	SharedDeviceMemory(SharedDeviceMemory&& from)
		: device{ from.device }, data{ from.data }, offset{ from.offset }
	{
		from.data = nullptr;
	}

	SharedDeviceMemory& operator=(SharedDeviceMemory&& from)
	{
		if (&from == this)
			return *this;

		if (from.data != data)
			this->~SharedDeviceMemory();

		::new(this) SharedDeviceMemory(move(from));

		return *this;
	}

	~SharedDeviceMemory();

private:
	struct Data {
		std::atomic_size_t references;
		VkDeviceMemory memory;
	};

	Device* device;
	Data* data;
	VkDeviceSize offset;
};

class Buffer {
public:

	VkBuffer& get_inner() { return buffer; }

	Buffer()
		: device{ nullptr }, buffer{ VK_NULL_HANDLE }
	{
	}

	Buffer(Device* device, VkBuffer buffer)
		: device{ device }, buffer{ buffer }
	{
	}

	Buffer(Buffer&& other)
		: device{ other.device }, buffer{ other.buffer }
	{
		other.buffer = VK_NULL_HANDLE;
	}

	Buffer& operator=(Buffer&& other)
	{
		this->~Buffer();
		device = other.device;
		buffer = other.buffer;
		other.buffer = VK_NULL_HANDLE;
		other.device = nullptr;
		return *this;
	}

	~Buffer();

private:
	Device* device;

	VkBuffer buffer;
};

struct SharedDeviceBuffer {
	SharedDeviceMemory memory;
	Buffer buffer;
};

class Memory {
public:
	static uint32_t find_memory_type(
		VkPhysicalDevice      physical_device, 
		VkMemoryPropertyFlags properties, 
		uint32_t              memory_type_bits);

	static SharedDeviceMemory allocate_image_memory(
		Device*               device, 
		VkImage               image,
		VkFormat              format, 
		VkMemoryPropertyFlags properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	static Vector<SharedDeviceMemory> allocate_image_memory(
		Device*               device, 
		const Vector<Image>&  image, 
		VkMemoryPropertyFlags properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	static SharedDeviceBuffer create_device_local_vertex_buffer(
		Device*                      device, 
		const SimpleBuffer<uint8_t>& data) 
	{
		return create_device_local_vertex_buffer(device, data.get_size(), data.get_buffer());
	}

	static SharedDeviceBuffer create_device_local_vertex_buffer(
		Device*     device, 
		size_t      size, 
		const void* data);

	static void create_device_local_vertex_index_buffer(
		Device*             device,
		size_t              vertex_size,
		const void*         vertex_data,
		size_t              index_size,
		const void*         index_data,
		SharedDeviceBuffer& out_vertex_buffer,
		SharedDeviceBuffer& out_index_buffer);

	static SharedDeviceBuffer allocate_descriptor_memory(
		Device*               device, 
		VkDeviceSize          size, 
		VkBufferUsageFlags    buffer_usage      = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | 
		                                          VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 
		VkMemoryPropertyFlags memory_properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | 
		                                          VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	static SharedDeviceBuffer allocate_descriptor_array_memory(
		Device*               device, 
		VkDeviceSize          size, 
		VkDeviceSize          count, 
		VkBufferUsageFlags    buffer_usage      = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | 
		                                          VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 
		VkMemoryPropertyFlags memory_properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | 
		                                          VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	static VkDeviceMemory allocate_memory(
		Device*      device, 
		VkDeviceSize allocation_size, 
		uint32_t     memory_type_index);

	static VkBuffer create_buffer(
		Device*                       device, 
		VkDeviceSize                  size, 
		VkBufferUsageFlags            usage, 
		VkSharingMode                 sharing_mode, 
		const SimpleBuffer<uint32_t>& queue_family_indices);

	static void bind_buffer_memory(
		Device*        device, 
		VkBuffer       buffer, 
		VkDeviceMemory memory, 
		VkDeviceSize   offset = 0);

	static void copy_to_buffer(
		Device*                      device, 
		VkDeviceMemory               memory, 
		const SimpleBuffer<uint8_t>& data, 
		VkDeviceSize                 offset = 0);

	static void copy_to_buffer(
		Device*        device, 
		VkDeviceMemory memory, 
		size_t         size, 
		const void*    data, 
		VkDeviceSize   offset = 0);

	static void copy_to_image_staged(
		Device*                  device,
		VkImage                  dst_image,
		VkImageSubresourceLayers subresource,
		VkExtent3D               extent,
		size_t                   size,
		const void*              data);

	static VkDeviceSize get_descriptor_offset_alignment(
		Device*      device, 
		VkDeviceSize size);

	static VkDeviceSize get_aligned_size(VkDeviceSize size, VkDeviceSize alignment)
	{
		return alignment * ceil_div(size, alignment);
	}

	static void transition_image_layout(
		Device*       device,
		Sampler&      sampler, 
		VkImageLayout old_layout, 
		VkImageLayout new_layout,
		VkAccessFlags old_access_mask,
		VkAccessFlags new_access_mask);
};

} // namespace renderer
