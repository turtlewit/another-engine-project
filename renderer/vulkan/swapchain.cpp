#include <simple_buffer.hpp>

#include "swapchain.hpp"
#include "device.hpp"
#include "surface.hpp"
#include "texture.hpp"
#include "logger.hpp"
#include "instance.hpp"
#include "util.hpp"

using namespace renderer;

namespace {

VkSurfaceFormatKHR choose_surface_format(const SimpleBuffer<VkSurfaceFormatKHR>& formats)
{
	if (formats.get_size() == 0) {
		LOG_ERROR("No surface formats available.\n");
		exit(1);
	}

	for (size_t i = 0; i < formats.get_size(); ++i) {
		VkSurfaceFormatKHR current_format = formats.get_buffer()[i];

		bool is_bgra8_srgb = current_format.format == VK_FORMAT_B8G8R8A8_SRGB;
		bool is_nonlinear = current_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

		if (is_bgra8_srgb && is_nonlinear)
			return current_format;
	}

	LOG_WARNING("Could not get BGRA8_SRGB surface format. Output may not look correct...");

	return formats.get_buffer()[0];
}

VkPresentModeKHR PREFERRED_PRESENT_MODE = VK_PRESENT_MODE_FIFO_KHR;
VkPresentModeKHR choose_present_mode(const SimpleBuffer<VkPresentModeKHR>& modes)
{
	for (size_t i = 0; i < modes.get_size(); ++i) {
		if (PREFERRED_PRESENT_MODE == modes.get_buffer()[i])
			return PREFERRED_PRESENT_MODE;
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

template <typename T>
inline T clamp(T val, T low, T high)
{
	return (val > low ? val : low) < high ? val : high;
}

} // anonymous namespace



namespace renderer {

void Swapchain::set_preferred_present_mode(VkPresentModeKHR present_mode)
{
	PREFERRED_PRESENT_MODE = present_mode;
}

Swapchain Swapchain::create_swapchain(Device* device, Surface* surface, uint32_t max_frames_in_flight)
{
	VkSurfaceCapabilitiesKHR surface_caps = device->get_physical_device_surface_capabilities(surface);

	//VkExtent2D surface_extent = surface->get_extent();
	VkExtent2D surface_extent = device->get_instance()->get_extent();
	surface_extent.width = clamp(surface_extent.width, surface_caps.minImageExtent.width, surface_caps.maxImageExtent.width);
	surface_extent.height = clamp(surface_extent.height, surface_caps.minImageExtent.height, surface_caps.maxImageExtent.height);

	SimpleBuffer<VkSurfaceFormatKHR> available_formats = device->get_physical_device_surface_formats(surface);
	VkSurfaceFormatKHR surface_format = choose_surface_format(available_formats);

	SimpleBuffer<VkPresentModeKHR> available_present_modes = device->get_physical_device_surface_present_modes(surface);
	VkPresentModeKHR present_mode = choose_present_mode(available_present_modes);

	uint32_t image_count = surface_caps.minImageCount + 1;
	if (image_count > surface_caps.maxImageCount && surface_caps.maxImageCount > 0)
		image_count = surface_caps.maxImageCount;

	const uint32_t queue_family_count = 1;
	uint32_t queue_families[queue_family_count] = {
		device->get_queue_families().present,
	};

	VkSharingMode image_sharing_mode = device->get_queue_families().present == device->get_queue_families().graphics ? VK_SHARING_MODE_EXCLUSIVE : VK_SHARING_MODE_CONCURRENT;

	VkSwapchainCreateInfoKHR swapchain_create_info{};
	swapchain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchain_create_info.flags = 0;
	swapchain_create_info.surface = surface->get_inner();
	swapchain_create_info.minImageCount = image_count;
	swapchain_create_info.imageFormat = surface_format.format;
	swapchain_create_info.imageColorSpace = surface_format.colorSpace;
	swapchain_create_info.imageExtent = surface_extent;
	swapchain_create_info.imageArrayLayers = 1;
	swapchain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchain_create_info.imageSharingMode = image_sharing_mode;
	swapchain_create_info.queueFamilyIndexCount = queue_family_count;
	swapchain_create_info.pQueueFamilyIndices = queue_families;
	swapchain_create_info.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchain_create_info.presentMode = present_mode;
	swapchain_create_info.clipped = VK_FALSE;
	swapchain_create_info.oldSwapchain = VK_NULL_HANDLE;

	VkSwapchainKHR swapchain;
	ASSERT_VK_SUCCESS(vkCreateSwapchainKHR(device->get_inner(), &swapchain_create_info, nullptr, &swapchain));

	// images
	uint32_t swapchain_image_count;
	ASSERT_VK_SUCCESS(vkGetSwapchainImagesKHR(device->get_inner(), swapchain, &swapchain_image_count, nullptr));

	Vector<VkImage> swapchain_images{};
	swapchain_images.resize_uninitialized(swapchain_image_count);

	ASSERT_VK_SUCCESS(vkGetSwapchainImagesKHR(device->get_inner(), swapchain, &swapchain_image_count, swapchain_images.get_buffer()));

	Vector<Image> images{};
	Vector<VkImageView> image_views{};

	images.reserve(swapchain_image_count);
	image_views.reserve(images.get_size());

	for (VkImage& vk_image : swapchain_images) {
		Image image{ vk_image, surface_format.format };
		images.add(image);
		image_views.add(image.create_simple_image_view(device));
	}

	VkViewport viewport;
	viewport.x = 0;
	viewport.y = 0;
	viewport.width = surface_extent.width;
	viewport.height = surface_extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor;
	scissor.offset = VkOffset2D{ .x = 0, .y = 0 };
	scissor.extent = surface_extent;

	return Swapchain(device, surface, swapchain, surface_extent, viewport, scissor, surface_format, 
	                 max_frames_in_flight, images, image_views);
}

VkResult Swapchain::acquire_next_image(VkSemaphore signal_semaphore)
{
	VkResult result = vkAcquireNextImageKHR(device->get_inner(), swapchain, UINT64_MAX, signal_semaphore, VK_NULL_HANDLE, &current_image_index);
	return result;
}

VkResult Swapchain::present(VkSemaphore wait_semaphore)
{
	return device->get_present_queue().present({ wait_semaphore }, { swapchain }, { current_image_index });
}

Swapchain::Swapchain(Device* device, 
                     Surface* surface, 
                     VkSwapchainKHR swapchain, 
                     VkExtent2D extent, 
                     VkViewport viewport,
                     VkRect2D scissor,
                     VkSurfaceFormatKHR surface_format, 
                     uint32_t max_frames_in_flight,
                     Vector<Image>& images,
                     Vector<VkImageView>& image_views)
	: device{ device }
	, surface{ surface }
	, swapchain{ swapchain }
	, extent{ extent }
	, viewport{ viewport }
	, scissor{ scissor }
	, surface_format{ surface_format }
	, max_frames_in_flight{ max_frames_in_flight }
	, images{ move(images) }
	, image_views{ move(image_views) }
	, current_image_index{ u32(images.get_size() - 1) }
{
}

Swapchain::~Swapchain()
{
	for (VkImageView& image_view : image_views)
		vkDestroyImageView(device->get_inner(), image_view, nullptr);

	vkDestroySwapchainKHR(device->get_inner(), swapchain, nullptr);
}

} // namespace renderer
