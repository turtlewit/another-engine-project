#include "image.hpp"
#include "device.hpp"

namespace renderer {

VkImageView Image::create_simple_image_view(Device* device) const
{
	VkComponentMapping image_view_component_mapping{};
	image_view_component_mapping.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_component_mapping.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_component_mapping.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_component_mapping.a = VK_COMPONENT_SWIZZLE_IDENTITY;

	VkImageSubresourceRange image_view_subresource_range{};
	image_view_subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	image_view_subresource_range.baseMipLevel = 0;
	image_view_subresource_range.levelCount = 1;
	image_view_subresource_range.baseArrayLayer = 0;
	image_view_subresource_range.layerCount = 1;

	VkImageViewCreateInfo image_view_create_info{};
	image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	image_view_create_info.pNext = nullptr;
	image_view_create_info.flags = 0;
	image_view_create_info.image = image;
	image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	image_view_create_info.format = format;
	image_view_create_info.components = image_view_component_mapping;
	image_view_create_info.subresourceRange = image_view_subresource_range;

	VkImageView image_view;
	vkCreateImageView(device->get_inner(), &image_view_create_info, nullptr, &image_view);

	return image_view;
}

} // namespace renderer
