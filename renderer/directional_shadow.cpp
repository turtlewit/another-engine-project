#include <cmath>

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtx/string_cast.hpp>

#include <device.hpp>
#include <render_pass.hpp>
#include <memory.hpp>
#include <descriptor.hpp>
#include <swapchain.hpp>

#include "directional_shadow.hpp"
#include "util.hpp"
#include "shaders.hpp"

using namespace renderer;

glm::mat4 build_light_frustum(const Camera& camera, const glm::mat4& camera_to_light, float max_shadow_distance)
{
	float half_fovy = glm::radians(camera.fov) / 2.0f;
	float half_fovx = (glm::radians(camera.fov) * camera.aspect) / 2.0f;
	//Logger::log_info("half_fovx: %f\n", half_fovx);
	//Logger::log_info("half_fovy: %f\n", half_fovy);

	glm::vec3 camera_origin = camera_to_light * glm::vec4(0, 0, 0, 1);

	float right  = max_shadow_distance * tan(half_fovx);
	float bottom = max_shadow_distance * tan(half_fovy);

	glm::vec3 bottom_right = camera_to_light * glm::vec4( right,  bottom, -max_shadow_distance, 1.0f);
	glm::vec3 bottom_left  = camera_to_light * glm::vec4(-right,  bottom, -max_shadow_distance, 1.0f);
	glm::vec3 top_right    = camera_to_light * glm::vec4( right, -bottom, -max_shadow_distance, 1.0f);
	glm::vec3 top_left     = camera_to_light * glm::vec4(-right, -bottom, -max_shadow_distance, 1.0f);

	float max_right  = -INFINITY;
	float min_left   =  INFINITY;
	float max_bottom = -INFINITY;
	float min_top    =  INFINITY;
	float max_far    = -INFINITY;
	float min_near   =  INFINITY;

	for (const auto& pos : { camera_origin, bottom_right, bottom_left, top_right, top_left }) {
		max_right  = max(max_right,  pos.x);
		min_left   = min(min_left,   pos.x);
		max_bottom = max(max_bottom, pos.y);
		min_top    = min(min_top,    pos.y);
		max_far    = max(max_far,   -pos.z); // TODO: not so sure about this one, boss
		min_near   = min(min_near,  -pos.z);
	}

	//Logger::log_info("Left: %f, Right: %f, Bottom: %f, Top: %f, Near: %f, Far: %f\n",
	//		min_left, max_right, max_bottom, min_top, -max_far, -min_near);
	//Logger::log_info("Camera origin: %s\n", glm::to_string(camera_origin).c_str());

	return glm::orthoZO(min_left, max_right, -max_bottom, -min_top, -max_far, -min_near);
}

size_t DirectionalShadowPipeline::shadow_map_size = 4096 * 4;

DirectionalShadowPipeline::DirectionalShadowPipeline(
	renderer::Device*    device,
	World*               world,
	DirectionalLight::ID directional_light_id,
	float                optimized)
	: device{ device }
	, world{ world }
	, directional_light_id{ directional_light_id }
	, optimized{ optimized }
{
	Swapchain* swapchain = device->get_swapchain();

	common_uniform_memories.reserve(swapchain->get_max_frames_in_flight());
	for (uint32_t i = 0; i < swapchain->get_max_frames_in_flight(); ++i)
		common_uniform_memories.add(Memory::allocate_descriptor_memory(device, sizeof(CommonUniform)));

	per_object_uniform_memories.resize(swapchain->get_max_frames_in_flight());

	shadow_map = SamplerBuilder()
		.with_extent_2d(shadow_map_size, shadow_map_size)
		.with_format(VK_FORMAT_D32_SFLOAT)
		.with_tiling(VK_IMAGE_TILING_OPTIMAL)
		.with_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
		.with_aspect(VK_IMAGE_ASPECT_DEPTH_BIT)
		.with_min_mag_filter(VK_FILTER_NEAREST, VK_FILTER_NEAREST)
		.build(device)
	;

	// create render pass
	{
		RenderPassBuilder render_pass_builder{}; render_pass_builder
			.with_subpasses(1)
			.with_attachment(
				RenderPassAttachment::create_depth_attachment(
					shadow_map.format,
					shadow_map.image_view,
					VK_IMAGE_LAYOUT_UNDEFINED,
					VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					VK_FALSE,
					VK_SAMPLE_COUNT_1_BIT,
					VK_ATTACHMENT_LOAD_OP_CLEAR,
					VK_ATTACHMENT_STORE_OP_STORE
				)
			)
			.with_depth_attachment_reference(0, { 0, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL })
		;

		render_pass = new RenderPass(render_pass_builder.build(device, shadow_map_size, shadow_map_size, 1));
	}

	// create_pipeline
	{
		PipelineBuilder builder{}; builder
			.with_shader(device, deferred_pass_glsl_vert_spv_data, deferred_pass_glsl_vert_spv_size)
			.with_depth_stencil()
			.with_attachments(0) // TODO: hmmst, check UnorderedVector::add(element, 0)
			.with_viewports(
				{
					VkViewport{ .width = f32(shadow_map_size), .height = f32(shadow_map_size), .minDepth = 0.0f, .maxDepth = 1.0f }
				},
				{
					VkRect2D{ .offset = {}, .extent = VkExtent2D{ u32(shadow_map_size), u32(shadow_map_size) } }
				}
			)
		;

		pipeline = Pipeline(device, builder, render_pass->get_inner());
	}
}

void DirectionalShadowPipeline::draw(DescriptorPool& descriptor_pool, CommandBuffer& command_buffer, uint32_t image_index)
{
	auto extent = device->get_swapchain()->get_extent();

	const auto& directional_light = get_light();

	// update descriptor sets
	Vector<DescriptorSet> descriptor_sets;
	{
		glm::mat4 light_projection;
		CommonUniform common_uniform {
			.screen_size = glm::uvec2(extent.width, extent.height)
		};

		Memory::copy_to_buffer(device, common_uniform_memories[image_index].memory.get_memory(), sizeof(CommonUniform), &common_uniform, 0);

		SimpleBuffer<uint8_t> frame_data;
		frame_data.resize(world->get_size<MeshInstance>() * Memory::get_descriptor_offset_alignment(device, sizeof(PerObjectUniform)));

		Vector<VkDescriptorSetLayout> descriptor_set_layouts{};
		descriptor_set_layouts.reserve(world->get_size<MeshInstance>() + 1 /* common uniform */);

		descriptor_set_layouts.add(pipeline.get_descriptor_set_layouts()[0]);

		auto& per_object_uniform_memory = per_object_uniform_memories[image_index] = SharedDeviceBuffer{};
		per_object_uniform_memory = Memory::allocate_descriptor_array_memory(device, sizeof(PerObjectUniform), world->get_size<MeshInstance>());

		uint8_t* buffer = frame_data.get_buffer();
		VkDeviceSize alignment = Memory::get_descriptor_offset_alignment(device, sizeof(PerObjectUniform));
		camera_to_light = glm::translate(glm::identity<glm::mat4>(), glm::vec3(0, 0, directional_light.max_shadow_distance / 2.0));

		camera_to_light = glm::lookAt(
			glm::vec3(0), 
			glm::vec3(world->camera.model_matrix * glm::vec4(directional_light.direction, 0.0)), 
			glm::vec3(0, 1, 0)
		) * camera_to_light;

		camera_to_light = build_light_frustum(world->camera, camera_to_light, directional_light.max_shadow_distance) * 
			camera_to_light;

		glm::mat4 light_cam = camera_to_light * world->camera.model_matrix;

		for (const auto& mesh_instance : world->get_components<MeshInstance>()) {
			// we don't really care about anything else in the uniform
			// TODO: we should actually made a real shader for this
			*reinterpret_cast<PerObjectUniform*>(buffer) = PerObjectUniform {
				.mvp = light_cam * mesh_instance.model_matrix,
			};

			buffer += alignment;

			descriptor_set_layouts.add(pipeline.get_descriptor_set_layouts()[1]);
		}

		Memory::copy_to_buffer(device, per_object_uniform_memory.memory.get_memory(), frame_data.get_size(), frame_data.get_buffer(), 0);

		// write descriptor memory

		descriptor_sets = descriptor_pool.allocate_descriptor_sets(descriptor_set_layouts);

		descriptor_sets[0].write_uniform_buffer(0, 0, common_uniform_memories[image_index].buffer.get_inner(), 0, sizeof(CommonUniform));

		VkDeviceSize offset = 0;
		for (uint32_t i = 1; i < descriptor_sets.get_size(); ++i) {
			descriptor_sets[i].write_uniform_buffer(0, 0, per_object_uniform_memory.buffer.get_inner(), offset, sizeof(PerObjectUniform));
			offset += Memory::get_descriptor_offset_alignment(device, sizeof(PerObjectUniform));
		}
	}

	command_buffer.begin_render_pass(render_pass, 0, VkExtent2D{ u32(shadow_map_size), u32(shadow_map_size) });

	vkCmdBindPipeline(command_buffer.get_inner(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.get_inner());

	vkCmdBindDescriptorSets(
		command_buffer.get_inner(),
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		pipeline.get_pipeline_layout(),
		0,
		1,
		&descriptor_sets[0].get_inner(),
		0,
		nullptr
	);

	for (uint32_t i = 0; i < world->get_size<MeshInstance>(); ++i) {
		auto& mesh = world->get_components<MeshInstance>()[i];

		// for now, transparent geometry does not cast a shadow;
		if (mesh.color.w < 1.0)
			continue;

		auto descriptor_set = descriptor_sets[i + 1].get_inner();

		vkCmdBindDescriptorSets(
			command_buffer.get_inner(),
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			pipeline.get_pipeline_layout(),
			1,
			1,
			&descriptor_set,
			0,
			nullptr
		);

		world->get_component<MeshComponent>(mesh.mesh)->mesh.bind_and_draw(command_buffer);
	}

	command_buffer.end_render_pass();

}

SimpleBuffer<VkDescriptorPoolSize> DirectionalShadowPipeline::get_descriptor_pool_sizes()
{
	return {
		VkDescriptorPoolSize{ .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = 1 }, // common buffer
		VkDescriptorPoolSize{ .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 2 }, // common buffer
		VkDescriptorPoolSize{ .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = u32(world->get_size<MeshInstance>()) }, // per-object
	};
}

uint32_t DirectionalShadowPipeline::get_max_descriptor_sets()
{
	return 1 + world->get_size<MeshInstance>();
}

DirectionalShadowPipeline::~DirectionalShadowPipeline()
{
	drop(pipeline);
	delete render_pass;
}

DirectionalLight& DirectionalShadowPipeline::get_light()
{
	DirectionalLight* light = world->get_component<DirectionalLight>(directional_light_id);

	if (light == nullptr) {
		LOG_ERROR("Directional light corresponding to DirectionalShadowPipeline has been deleted!");
		exit(1);
	}

	return *light;
}
