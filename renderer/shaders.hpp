#pragma once

#include <glm/glm.hpp>
#include <vulkan/vulkan.h>

extern const unsigned char deferred_pass_glsl_vert_spv_data[];
extern const size_t deferred_pass_glsl_vert_spv_size;
extern const unsigned char deferred_pass_glsl_frag_spv_data[];
extern const size_t deferred_pass_glsl_frag_spv_size;
extern const unsigned char transparent_pass_glsl_frag_spv_data[];
extern const size_t transparent_pass_glsl_frag_spv_size;
extern const unsigned char lighting_pass_glsl_vert_spv_data[];
extern const size_t lighting_pass_glsl_vert_spv_size;
extern const unsigned char lighting_pass_glsl_frag_spv_data[];
extern const size_t lighting_pass_glsl_frag_spv_size;

constexpr auto MAX_POINT_LIGHTS = 32;
constexpr auto MAX_DIRECTIONAL_LIGHTS = 32;

struct CommonUniform {
	glm::mat4 view;
	glm::mat4 projection;
	glm::uvec2 screen_size;
};

struct PerObjectUniform {
	glm::mat4 model;
	glm::mat4 modelview;
	glm::mat4 mvp;
	glm::vec4 color;
	float metallic;
	float roughness;
	uint32_t flags = 0;
};

struct ShaderPointLight {
	alignas(16) glm::vec3 position;
	alignas(16) glm::vec3 color;
	float power;
};

struct ShaderDirectionalLight {
	alignas(16) glm::vec3 direction;
	alignas(16) glm::vec3 color;
	float power;
	VkBool32 has_shadow = VK_FALSE;
};

struct alignas(16) ShaderDirectionalShadow {
	glm::mat4 light_space;
	float bias;
};

struct LightingUniform {
	ShaderPointLight point_lights[MAX_POINT_LIGHTS];
	ShaderDirectionalLight directional_lights[MAX_DIRECTIONAL_LIGHTS];
	ShaderDirectionalShadow directional_shadows[MAX_DIRECTIONAL_LIGHTS];
	glm::uvec2 screen_size;
	float ambient;
	uint32_t mode;
};
