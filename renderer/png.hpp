#pragma once

#include <vulkan/vulkan.h>
#include <simple_buffer.hpp>

namespace renderer {

SimpleBuffer<uint8_t> load_png(const char* filepath, VkExtent2D& extent);

} // namespace renderer
