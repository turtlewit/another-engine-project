#pragma once

#include <glm/glm.hpp>

#include <device.hpp>
#include <sampler.hpp>

struct GBuffer {
	static GBuffer create_gbuffer(renderer::Device* device, uint32_t width, uint32_t height, VkSampleCountFlagBits sample_count)
	{
		renderer::SamplerBuilder builder = renderer::SamplerBuilder()
			.with_extent_2d(width, height)
			.with_format(VK_FORMAT_R16G16B16A16_SFLOAT)
			.with_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT)
			.with_samples(sample_count)
		;

		return GBuffer{
			builder.build(device),
			builder.build(device),
			builder.with_format(VK_FORMAT_R8G8B8A8_UNORM).build(device),
		};
	}

	renderer::Sampler position;
	renderer::Sampler normal;
	renderer::Sampler albedo;
};

struct ABuffer {
	static constexpr uint32_t MAX_TRANSPARENT_LAYERS = 9;

	static constexpr VkDeviceSize get_data_size(uint32_t screen_width, uint32_t screen_height)
	{
		return sizeof(ABuffer) * screen_width * screen_height * MAX_TRANSPARENT_LAYERS;
	}

	static constexpr VkDeviceSize get_nodes_size(uint32_t screen_width, uint32_t screen_height)
	{
		return sizeof(uint32_t) + sizeof(uint32_t)
			+ (sizeof(uint64_t) * screen_width * screen_height) 
			+ (sizeof(uint64_t) * screen_width * screen_height * MAX_TRANSPARENT_LAYERS);
	}

	uint32_t color;
	uint32_t flags;
	uint16_t normal;
	uint32_t position;
};
