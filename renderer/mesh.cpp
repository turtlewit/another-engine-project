#include <glm/glm.hpp>
#include <device.hpp>

#include "mesh.hpp"

using namespace renderer;

Mesh::Mesh(Device* device, SimpleBuffer<Vertex>&& vertices)
	: device{ device }
	, vertex_count{ sizeof(vertices) }
	, index_count{ 0 }
	, vertex_buffer{}
	, type{ MeshType::Vertices }
{
	vertex_buffer = Memory::create_device_local_vertex_buffer(device, vertices.get_size() * sizeof(Vertex), vertices.get_buffer());
}

void Mesh::bind_and_draw(CommandBuffer& command_buffer)
{
	switch (type) {
		case MeshType::Vertices: {
			VkDeviceSize offset = 0;
			vkCmdBindVertexBuffers(command_buffer.get_inner(), 0, 1, &vertex_buffer.buffer.get_inner(), &offset);
			vkCmdDraw(command_buffer.get_inner(), vertex_count, 1, 0, 0);
		} break;

		case MeshType::Indices: {
			VkDeviceSize offset = 0;
			vkCmdBindVertexBuffers(command_buffer.get_inner(), 0, 1, &vertex_buffer.buffer.get_inner(), &offset);
			vkCmdBindIndexBuffer(command_buffer.get_inner(), index_buffer.buffer.get_inner(), offset, index_type);
			vkCmdDrawIndexed(command_buffer.get_inner(), index_count, 1, 0, 0, 0);
		} break;
	}
}
