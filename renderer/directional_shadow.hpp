#pragma once

#include <glm/glm.hpp>

#include <vector.hpp>
#include <sampler.hpp>
#include <pipeline.hpp>

#include "component.hpp"
#include "world.hpp"

glm::mat4 build_light_frustum(const Camera& camera, const glm::mat4& camera_to_light, float max_shadow_distance);

namespace renderer {
	class Device;
	class RenderPass;
	class DescriptorPool;
	struct SharedDeviceBuffer;
};

class DirectionalShadowPipeline {
public:
	static size_t shadow_map_size;

	static void enable_directional_shadow(
			renderer::Device* device, 
			World* world, 
			DirectionalLight& light, 
			float bias, 
			float max_shadow_distance, 
			float optimized = true)
	{
		DirectionalShadowPipeline* dsp = new DirectionalShadowPipeline(device, world, light.id, optimized);
		light.directional_shadow_pipeline = dsp;
		light.bias = bias;
		light.max_shadow_distance = max_shadow_distance;
	}
	
	void draw(renderer::DescriptorPool& descriptor_pool, renderer::CommandBuffer& command_buffer, uint32_t image_index);

	SimpleBuffer<VkDescriptorPoolSize> get_descriptor_pool_sizes();
	uint32_t get_max_descriptor_sets();

	renderer::Sampler& get_shadow_map() { return shadow_map; }
	const glm::mat4& get_light_space() { return camera_to_light; }

	~DirectionalShadowPipeline();

private:
	DirectionalShadowPipeline(
		renderer::Device*    device,
		World*               world,
		DirectionalLight::ID directional_light,
		float                optimized);

	renderer::Device* device;
	World* world;
	DirectionalLight::ID directional_light_id;
	float optimized;

	renderer::RenderPass* render_pass;
	renderer::Pipeline pipeline{};

	renderer::Sampler shadow_map{};

	Vector<renderer::SharedDeviceBuffer> common_uniform_memories{};
	Vector<renderer::SharedDeviceBuffer> per_object_uniform_memories{};

	glm::mat4 camera_to_light;

	DirectionalLight& get_light();
};
