#include <algorithm>
#include <glm/glm.hpp>

#include <device.hpp>
#include <descriptor.hpp>
#include <command_buffer.hpp>
#include <world.hpp>
#include <quad.hpp>
#include <swapchain.hpp>
#include <logger.hpp>
#include <util.hpp>

#include "deferred_pipeline.hpp"
#include "gbuffer.hpp"
#include "shaders.hpp"
#include "directional_shadow.hpp"

using namespace renderer;

VkSampleCountFlags DeferredPipeline::allowed_samples = 0b0001; // default to 1 samples

void DeferredPipeline::initialize()
{
	VkPhysicalDeviceProperties props{};
	vkGetPhysicalDeviceProperties(device->get_physical_device(), &props);

	// choose number of multisampling samples
	sample_count = static_cast<VkSampleCountFlagBits>(
		pow(2, u32(ceil(log2(u32(props.limits.framebufferColorSampleCounts & props.limits.framebufferDepthSampleCounts & allowed_samples) + 1))) - 1)
	);
	Logger::log_info("Using sample count = %x\n", sample_count);

	// set up quad vertex buffer
	SimpleBuffer<uint8_t> vertex_data = SimpleBuffer<Quad2D>{ UNIT_QUAD };
	vertex_buffer = Memory::create_device_local_vertex_buffer(device, vertex_data);

	Swapchain* swapchain = device->get_swapchain();

	// allocate memory for static descriptor sets
	descriptor_memories.reserve(swapchain->get_max_frames_in_flight());
	common_uniform_memories.reserve(swapchain->get_max_frames_in_flight());
	for (uint32_t i = 0; i < swapchain->get_max_frames_in_flight(); ++i) {
		descriptor_memories.add(Memory::allocate_descriptor_memory(device, sizeof(LightingUniform)));
		common_uniform_memories.add(Memory::allocate_descriptor_memory(device, sizeof(CommonUniform)));
	}

	// later, this will be used to store the per-object descriptor memories
	// right now, it just fills it with empty SharedDeviceBuffer's
	per_object_uniform_memories.resize(swapchain->get_max_frames_in_flight());

	// default sampler for object without color / normal textures
	default_sampler = SamplerBuilder()
		.with_extent_2d(1, 1)
		.with_format(VK_FORMAT_R8_SRGB)
		.with_tiling(VK_IMAGE_TILING_OPTIMAL)
		.with_usage(VK_IMAGE_USAGE_SAMPLED_BIT)
		.build(device)
	;

	Memory::transition_image_layout(device, default_sampler, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 0, VK_ACCESS_SHADER_READ_BIT);

	handle_resize();
}

void DeferredPipeline::handle_resize()
{
	Swapchain* swapchain = device->get_swapchain();
	VkExtent2D extent = swapchain->get_extent();

	// create gbuffers
	{
		gbuffers->clear();
		for (uint32_t i = 0; i < swapchain->get_max_frames_in_flight(); ++i) {
			gbuffers->add(GBuffer::create_gbuffer(device, extent.width, extent.height, sample_count));
		}
	}

	// create depth buffer
	{
		SamplerBuilder builder = SamplerBuilder()
			.with_extent_2d(extent.width, extent.height)
			.with_format(VK_FORMAT_D32_SFLOAT) // TODO: find supported format
			.with_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
			.with_tiling(VK_IMAGE_TILING_OPTIMAL)
			.with_aspect(VK_IMAGE_ASPECT_DEPTH_BIT)
			//.with_samples(sample_count)
		;

		depth_buffer = builder.build(device);
	}

	// create color target
	color_target = SamplerBuilder()
		.with_extent_2d(extent.width, extent.height)
		.with_format(swapchain->get_surface_format().format)
		.with_usage(VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
		.with_tiling(VK_IMAGE_TILING_OPTIMAL)
		.with_aspect(VK_IMAGE_ASPECT_COLOR_BIT)
		//.with_samples(sample_count)
		.build(device)
	;

	// create render pass
	{
		Vector<Vector<VkImageView>> v_image_views;
		v_image_views.resize(3);
		for (uint32_t i = 0; i < swapchain->get_max_frames_in_flight(); ++i) {
			v_image_views[0].add((*gbuffers)[i].position.image_view);
			v_image_views[1].add((*gbuffers)[i].normal.image_view);
			v_image_views[2].add((*gbuffers)[i].albedo.image_view);
		}

		Vector<VkImageView> color_target_image_views{};
		color_target_image_views.add(color_target.image_view);

		RenderPassBuilder render_pass_builder{}; render_pass_builder
			.with_subpasses(3)
			
			// attachments

			// gbuffer
			.with_attachment(
				RenderPassAttachment::create_color_attachment(
					(*gbuffers)[0].position.format,
					v_image_views[0],
					VK_IMAGE_LAYOUT_UNDEFINED,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					VK_TRUE,
					sample_count
				)
			)
			.with_attachment(
				RenderPassAttachment::create_color_attachment(
					(*gbuffers)[0].normal.format,
					v_image_views[1],
					VK_IMAGE_LAYOUT_UNDEFINED,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					VK_TRUE,
					sample_count
				)
			)
			.with_attachment(
				RenderPassAttachment::create_color_attachment(
					(*gbuffers)[0].albedo.format,
					v_image_views[2],
					VK_IMAGE_LAYOUT_UNDEFINED,
					VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					VK_TRUE,
					sample_count
				)
			)

			// depth target
			.with_depth_attachment(
				RenderPassAttachment::create_depth_attachment(
					depth_buffer.format,
					depth_buffer.image_view,
					VK_IMAGE_LAYOUT_UNDEFINED,
					VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
					VK_TRUE,
					sample_count
				)
			)

			// color target
			//.with_attachment(
			//	RenderPassAttachment::create_color_attachment(
			//		color_target.format,
			//		color_target_image_views,
			//		VK_IMAGE_LAYOUT_UNDEFINED,
			//		VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			//		VK_TRUE
			//	)
			//)

			// swapchain image
			.with_attachment(
				RenderPassAttachment::create_swapchain_color_attachment(
					swapchain
				)
			)

			// subpass 0
			.with_color_attachment_reference(  0, { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_color_attachment_reference(  0, { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_color_attachment_reference(  0, { 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_depth_attachment_reference(  0, { 3, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL })

			// subpass 1
			.with_color_attachment_reference(  1, { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_color_attachment_reference(  1, { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_color_attachment_reference(  1, { 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_depth_attachment_reference(  1, { 3, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL })

			// subpass 2
			.with_input_attachment_reference(  2, { 0, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL })
			.with_input_attachment_reference(  2, { 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL })
			.with_input_attachment_reference(  2, { 2, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL })
			//.with_color_attachment_reference(  2, { 4, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			//.with_resolve_attachment_reference(2, { 5, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })
			.with_color_attachment_reference(2, { 4, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL })

			// dependencies
			.with_dependency(
				VkSubpassDependency { // transparent shader needs depth buffer to be finished
					.srcSubpass = VK_SUBPASS_EXTERNAL,
					.dstSubpass = 0,
					.srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
					.srcAccessMask = 0,
					.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT |
					                 VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
					                 VK_ACCESS_MEMORY_WRITE_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency { // transparent shader needs depth buffer to be finished
					.srcSubpass = VK_SUBPASS_EXTERNAL,
					.dstSubpass = 1,
					.srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
					.srcAccessMask = 0,
					.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT |
					                 VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
					                 VK_ACCESS_MEMORY_WRITE_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency { // transparent shader needs depth buffer to be finished
					.srcSubpass = VK_SUBPASS_EXTERNAL,
					.dstSubpass = 2,
					.srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
					.srcAccessMask = 0,
					.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT |
					                 VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
					                 VK_ACCESS_MEMORY_WRITE_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency { // transparent shader needs depth buffer to be finished
					.srcSubpass = 0,
					.dstSubpass = 1,
					.srcStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
					                VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
					                VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
					.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
					.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency { // lighting shader needs both to be finished
					.srcSubpass = 0,
					.dstSubpass = 2,
					.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
					.dstAccessMask = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency { // lighting shader needs both to be finished
					.srcSubpass = 0,
					.dstSubpass = 2,
					.srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
					.dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency {
					.srcSubpass = 1,
					.dstSubpass = 2,
					.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
					.dstAccessMask = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
			.with_dependency(
				VkSubpassDependency { // lighting shader needs both to be finished
					.srcSubpass = 1,
					.dstSubpass = 2,
					.srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
					.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
					.dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
					//.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
				}
			)
		;

		render_pass = new RenderPass(render_pass_builder.build(device));
	}

	{
		PipelineBuilder builder{}; builder
			.with_shader(device, deferred_pass_glsl_vert_spv_data, deferred_pass_glsl_vert_spv_size)
			.with_shader(device, deferred_pass_glsl_frag_spv_data, deferred_pass_glsl_frag_spv_size)
			.with_depth_stencil()
			.with_subpass(0)
			//.with_rasterization_samples(sample_count)
		;

		opaque_pipeline = Pipeline(device, builder, render_pass->get_inner());
	}

	{
		PipelineBuilder builder{}; builder
			.with_shader(device, deferred_pass_glsl_vert_spv_data, deferred_pass_glsl_vert_spv_size)
			.with_shader(device, transparent_pass_glsl_frag_spv_data, transparent_pass_glsl_frag_spv_size)
			.with_depth_stencil(true, false) // don't write to depth buffer
			.with_subpass(1)
			//.with_rasterization_samples(sample_count)
		;

		transparent_pipeline = Pipeline(device, builder, render_pass->get_inner());
	}

	{
		SpecializationInfoBuilder specinfo{};
			specinfo.with_constant(i32(sample_count));

		PipelineBuilder builder{}; builder
			.with_shader(device, lighting_pass_glsl_vert_spv_data, lighting_pass_glsl_vert_spv_size)
			.with_shader(device, lighting_pass_glsl_frag_spv_data, lighting_pass_glsl_frag_spv_size, specinfo)
			.with_subpass(2)
		;

		lighting_pipeline = Pipeline(device, builder, render_pass->get_inner());
	}
}

void DeferredPipeline::draw(DescriptorPool& descriptor_pool, CommandBuffer& command_buffer, uint32_t image_index)
{
	auto extent = device->get_swapchain()->get_extent();


	// update geometry uniforms
	Vector<DescriptorSet> geometry_descriptor_sets;
	{
		// update common uniform
		CommonUniform common_uniform {
			.view = world->camera.model_matrix,
			.projection = world->camera.projection_matrix,
			.screen_size = glm::uvec2(extent.width, extent.height),
		};

		Memory::copy_to_buffer(device, common_uniform_memories[image_index].memory.get_memory(), sizeof(CommonUniform), &common_uniform, 0);

		// create descriptor set layout vector for later
		SimpleBuffer<uint8_t> frame_data;
		frame_data.resize(world->get_size<MeshInstance>() * Memory::get_descriptor_offset_alignment(device, sizeof(PerObjectUniform)));

		Vector<VkDescriptorSetLayout> descriptor_set_layouts{};
		descriptor_set_layouts.reserve(world->get_size<MeshInstance>() + 1 /* common uniform */);

		descriptor_set_layouts.add(opaque_pipeline.get_descriptor_set_layouts()[0]);

		// update per object uniforms
		auto& per_object_uniform_memory = per_object_uniform_memories[image_index] = SharedDeviceBuffer{}; // deallocate memory
		//per_object_uniform_memory = Memory::allocate_descriptor_memory(device, sizeof(PerObjectUniform) * world->mesh_instances.get_size());
		per_object_uniform_memory = Memory::allocate_descriptor_array_memory(device, sizeof(PerObjectUniform), world->get_size<MeshInstance>());

		{
			uint8_t* buffer = frame_data.get_buffer();
			const glm::mat4& view = world->camera.model_matrix;
			const glm::mat4& view_projection = world->camera.view_projection;
			VkDeviceSize alignment = Memory::get_descriptor_offset_alignment(device, sizeof(PerObjectUniform));
			for (const MeshInstance& mesh_instance : world->get_components<MeshInstance>()) {
				glm::vec4 color = mesh_instance.color;

				uint32_t flags = 0;

				float metallic = 0.0f;
				float roughness = 1.0f;

				if (mesh_instance.material > 0) {
					auto& material = *world->get_component<Material>(mesh_instance.material);
					color = material.color;
					metallic = material.metallic;
					roughness = material.roughness;

					if (material.color_texture > 0) {
						flags |= 1; // TODO: keep in line with shader, move to header
					}
					if (material.normal_texture > 0) {
						flags |= (1 << 1); // TODO: keep in line with shader, move to header
					}
					if (material.has_transmission) {
						flags |= (1 << 2);
					}
				}

				*reinterpret_cast<PerObjectUniform*>(buffer) = PerObjectUniform {
					mesh_instance.model_matrix,
					view * mesh_instance.model_matrix,
					view_projection * mesh_instance.model_matrix,
					color,
					metallic,
					roughness,
					flags,
				};

				buffer += alignment;

				// add descriptor set to vector while we're at it
				descriptor_set_layouts.add(opaque_pipeline.get_descriptor_set_layouts()[1]);
			}

			Memory::copy_to_buffer(device, per_object_uniform_memory.memory.get_memory(), frame_data.get_size(), frame_data.get_buffer(), 0);
		}


		// write descriptor memory

		geometry_descriptor_sets = descriptor_pool.allocate_descriptor_sets(descriptor_set_layouts);

		{
			geometry_descriptor_sets[0].write_uniform_buffer(0, 0, common_uniform_memories[image_index].buffer.get_inner(), 0, sizeof(CommonUniform));
			geometry_descriptor_sets[0].write_storage_buffer(1, 0, (*abuffer_heads)[image_index].buffer.get_inner(), 0, 
					ABuffer::get_nodes_size(extent.width, extent.height));
			geometry_descriptor_sets[0].write_storage_buffer(2, 0, (*abuffers)[image_index].buffer.get_inner(), 0, ABuffer::get_data_size(extent.width, extent.height));

			VkDeviceSize offset = 0;
			for (uint32_t i = 1; i < geometry_descriptor_sets.get_size(); ++i) {
				geometry_descriptor_sets[i].write_uniform_buffer(0, 0, per_object_uniform_memory.buffer.get_inner(), offset, sizeof(PerObjectUniform));

				auto& mesh_instance = world->get_components<MeshInstance>()[i - 1];
				auto material = world->get_component<Material>(mesh_instance.material);

				auto color_texture = material ? world->get_component<Texture>(material->color_texture) : nullptr;
				auto normal_texture = material ? world->get_component<Texture>(material->normal_texture) : nullptr;

				if (color_texture)
					geometry_descriptor_sets[i].write_sampler(1, 0, { .sampler = color_texture->texture.sampler, .imageView = color_texture->texture.image_view, .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL });
				else
					geometry_descriptor_sets[i].write_sampler(1, 0, { .sampler = default_sampler.sampler, .imageView = default_sampler.image_view, .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL });

				if (normal_texture)
					geometry_descriptor_sets[i].write_sampler(2, 0, { .sampler = normal_texture->texture.sampler, .imageView = normal_texture->texture.image_view, .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL });
				else 
					geometry_descriptor_sets[i].write_sampler(2, 0, { .sampler = default_sampler.sampler, .imageView = default_sampler.image_view, .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL });

				offset += Memory::get_descriptor_offset_alignment(device, sizeof(PerObjectUniform));
			}
		}

		//uint32_t free_node = extent.width * extent.height;
		uint32_t free_node = extent.width * extent.height;
		uint32_t size = free_node + (extent.width * extent.height * ABuffer::MAX_TRANSPARENT_LAYERS);
		vkCmdFillBuffer(command_buffer.get_inner(), (*abuffer_heads)[image_index].buffer.get_inner(), sizeof(uint32_t) * 2, sizeof(uint64_t) * free_node, 0);
		vkCmdUpdateBuffer(command_buffer.get_inner(), (*abuffer_heads)[image_index].buffer.get_inner(), 0, sizeof(uint32_t), &free_node);
		vkCmdUpdateBuffer(command_buffer.get_inner(), (*abuffer_heads)[image_index].buffer.get_inner(), sizeof(uint32_t), sizeof(uint32_t), &size);
	}


	// update lighting uniforms
	auto lighting_descriptor_sets = descriptor_pool.allocate_descriptor_sets(lighting_pipeline.get_descriptor_set_layouts());
	{
		// update lighting uniform
		LightingUniform lighting_uniform = LightingUniform{}; // clear lights
		lighting_uniform.ambient = ambient;
		lighting_uniform.screen_size = glm::uvec2(extent.width, extent.height);
		lighting_uniform.mode = shading_mode;

		// initialize the shadow maps to the default sampler
		Vector<VkDescriptorImageInfo> shadow_maps{};
		shadow_maps.add(VkDescriptorImageInfo{ 
				.sampler = default_sampler.sampler, 
				.imageView = default_sampler.image_view, 
				.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL 
		}, MAX_DIRECTIONAL_LIGHTS);

		for (size_t i = 0; i < min(usize(MAX_POINT_LIGHTS), world->get_size<PointLight>()); ++i) {
			const PointLight& point_light = world->get_components<PointLight>()[i];
			ShaderPointLight& light = lighting_uniform.point_lights[i];

			light.position = glm::vec3(world->camera.model_matrix * glm::vec4(point_light.position, 1.0));
			light.color = point_light.color;
			light.power = point_light.power;
		}

		for (size_t i = 0; i < min(usize(MAX_DIRECTIONAL_LIGHTS), world->get_size<DirectionalLight>()); ++i) {
			const DirectionalLight& directional_light = world->get_components<DirectionalLight>()[i];
			ShaderDirectionalLight& light = lighting_uniform.directional_lights[i];

			light.direction = glm::vec3(world->camera.model_matrix * glm::vec4(directional_light.direction, 0.0));
			light.color = directional_light.color;
			light.power = directional_light.power;

			if (directional_light.directional_shadow_pipeline) {
				ShaderDirectionalShadow& shadow = lighting_uniform.directional_shadows[i];
				DirectionalShadowPipeline& dsp = *directional_light.directional_shadow_pipeline;
				light.has_shadow = VK_TRUE;
				shadow.light_space = dsp.get_light_space();
				shadow.bias = directional_light.bias;

				Sampler& shadow_map = dsp.get_shadow_map();
				shadow_maps[i] = VkDescriptorImageInfo {
					.sampler = shadow_map.sampler,
					.imageView = shadow_map.image_view,
					.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
				};
			}
		}

		// copy to descriptor memory
		Memory::copy_to_buffer(device, descriptor_memories[image_index].memory.get_memory(), sizeof(LightingUniform), &lighting_uniform, 0);


		// write descriptor memory
		{
			lighting_descriptor_sets[0].write_descriptors(
				VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
				0,
				0,
				3,
				{},
				{
					{
						//.sampler = (*gbuffers)[image_index].position.sampler,
						.imageView = (*gbuffers)[image_index].position.image_view,
						.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					},
					{
						//.sampler = (*gbuffers)[image_index].normal.sampler,
						.imageView = (*gbuffers)[image_index].normal.image_view,
						.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					},
					{
						//.sampler = (*gbuffers)[image_index].albedo.sampler,
						.imageView = (*gbuffers)[image_index].albedo.image_view,
						.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
					},
				}
			);
			lighting_descriptor_sets[0].write_uniform_buffer(3, 0, descriptor_memories[image_index].buffer.get_inner(), 0, sizeof(LightingUniform));
			auto extent = device->get_swapchain()->get_extent();

			lighting_descriptor_sets[0].write_descriptors(
				VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				4,
				0,
				shadow_maps.get_size(),
				{},
				{ shadow_maps.get_size(), shadow_maps.get_buffer() }
			);

			lighting_descriptor_sets[0].write_storage_buffer(5, 0, (*abuffer_heads)[image_index].buffer.get_inner(), 0, 
					ABuffer::get_nodes_size(extent.width, extent.height));
			lighting_descriptor_sets[0].write_storage_buffer(6, 0, (*abuffers)[image_index].buffer.get_inner(), 0, ABuffer::get_data_size(extent.width, extent.height));
				
		}
	}

	{
		VkMemoryBarrier memory_barrier = {
			.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
			.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT,
		};
		vkCmdPipelineBarrier(
			command_buffer.get_inner(),
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
			0,
			1, &memory_barrier,
			0, nullptr,
			0, nullptr
		);
	}


	// drawing
	command_buffer.begin_render_pass(render_pass, device->get_swapchain()->get_current_image_index());


	// opaque subpass
	{
		vkCmdBindPipeline(command_buffer.get_inner(), VK_PIPELINE_BIND_POINT_GRAPHICS, opaque_pipeline.get_inner());

		vkCmdBindDescriptorSets(
			command_buffer.get_inner(),
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			opaque_pipeline.get_pipeline_layout(),
			0,
			1,
			&geometry_descriptor_sets[0].get_inner(),
			0,
			nullptr
		);

		for (uint32_t i = 0; i < world->get_size<MeshInstance>(); ++i) {
			auto& mesh = world->get_components<MeshInstance>()[i];

			//if (mesh.color.w < 1.0)
			//	continue;

			auto descriptor_set = geometry_descriptor_sets[i + 1].get_inner();

			vkCmdBindDescriptorSets(
				command_buffer.get_inner(),
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				opaque_pipeline.get_pipeline_layout(),
				1,
				1,
				&descriptor_set,
				0,
				nullptr
			);

			world->get_component<MeshComponent>(mesh.mesh)->mesh.bind_and_draw(command_buffer);
		}
	}

	vkCmdNextSubpass(command_buffer.get_inner(), VK_SUBPASS_CONTENTS_INLINE);

	// transparent subpass
	{
		vkCmdBindPipeline(command_buffer.get_inner(), VK_PIPELINE_BIND_POINT_GRAPHICS, transparent_pipeline.get_inner());

		for (uint32_t i = 0; i < world->get_size<MeshInstance>(); ++i) {
			auto& mesh = world->get_components<MeshInstance>()[i];

			//if (mesh.color.w == 1.0)
			//	continue;

			auto descriptor_set = geometry_descriptor_sets[i + 1].get_inner();

			vkCmdBindDescriptorSets(
				command_buffer.get_inner(),
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				transparent_pipeline.get_pipeline_layout(),
				1,
				1,
				&descriptor_set,
				0,
				nullptr
			);

			world->get_component<MeshComponent>(mesh.mesh)->mesh.bind_and_draw(command_buffer);
		}
	}

	vkCmdNextSubpass(command_buffer.get_inner(), VK_SUBPASS_CONTENTS_INLINE);

	// lighting subpass
	{
		vkCmdBindPipeline(command_buffer.get_inner(), VK_PIPELINE_BIND_POINT_GRAPHICS, lighting_pipeline.get_inner());
		VkDeviceSize offset = 0;
		vkCmdBindVertexBuffers(command_buffer.get_inner(), 0, 1, &vertex_buffer.buffer.get_inner(), &offset);

		const auto& raw_sets = DescriptorSet::get_raw_descriptor_set_array(lighting_descriptor_sets);
		vkCmdBindDescriptorSets(
			command_buffer.get_inner(),
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			lighting_pipeline.get_pipeline_layout(),
			0,
			1,
			raw_sets.get_buffer(),
			0,
			nullptr
		);

		vkCmdDraw(command_buffer.get_inner(), 6, 1, 0, 0);
	}


	command_buffer.end_render_pass();
}

void DeferredPipeline::destroy_render_pass()
{
	drop(opaque_pipeline);
	drop(transparent_pipeline);
	drop(lighting_pipeline);

	delete render_pass;
}

SimpleBuffer<VkDescriptorPoolSize> DeferredPipeline::get_descriptor_pool_sizes()
{
	return {
		VkDescriptorPoolSize{ .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = 1 }, // common buffer
		VkDescriptorPoolSize{ .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 2 }, // common buffer
		VkDescriptorPoolSize{ .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = u32(world->get_size<MeshInstance>()) }, // per-object
		{ .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, .descriptorCount = 2 * u32(world->get_size<MeshInstance>()) }, // color, normal textures
		{ .type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,       .descriptorCount = 3 },
		{ .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,         .descriptorCount = 1 },
		{ .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,         .descriptorCount = 2 },
		{ .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, .descriptorCount = MAX_DIRECTIONAL_LIGHTS },
		{ .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = MAX_DIRECTIONAL_LIGHTS },
	};
}

uint32_t DeferredPipeline::get_max_descriptor_sets()
{
	return 1 + world->get_size<MeshInstance>() + 1;
}

DeferredPipeline::~DeferredPipeline()
{
	delete gbuffers;
	delete render_pass;
}
