#pragma once

#include <glm/fwd.hpp>
#include <simple_buffer.hpp>
#include <memory.hpp>
#include <util.hpp>
#include <aabb.hpp>

namespace renderer {
	class Device;
	class CommandBuffer;
};

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 uv;
	glm::vec3 tangent;
	glm::vec3 bitangent;
};

class Mesh {
public:
	enum class MeshType {
		Vertices,
		Indices,
	};
	Mesh(renderer::Device* device, SimpleBuffer<Vertex>&& vertices);
	
	Mesh(
		renderer::Device*              device, 
		uint32_t                       vertex_count,
		renderer::SharedDeviceBuffer&& vertex_buffer,
		uint32_t                       index_count, 
		renderer::SharedDeviceBuffer&& index_buffer,
		VkIndexType                    index_type)
		: device{ device }
		, vertex_count{ vertex_count }
		, index_count{ index_count }
		, vertex_buffer{ move(vertex_buffer) }
		, index_buffer{ move(index_buffer) }
		, type{ MeshType::Indices }
		, index_type{ index_type }
	{
	}

	void bind_and_draw(renderer::CommandBuffer& command_buffer);
private:
	renderer::Device* device;
	uint32_t vertex_count;
	uint32_t index_count;
	renderer::SharedDeviceBuffer vertex_buffer;
	renderer::SharedDeviceBuffer index_buffer;
	MeshType type;
	VkIndexType index_type;
};
