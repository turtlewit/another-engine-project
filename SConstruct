#!/usr/bin/env python

import os
import sys

env = Environment()
env["ENV"]["TERM"] = os.environ["TERM"]
env['OSXCROSS_ROOT'] = os.environ.get('OSXCROSS_ROOT', '/opt/osxcross')

host_platform = sys.platform
if sys.platform in ['win32', 'msys']:
    host_platform = 'windows'

opts = Variables([], ARGUMENTS)

AddOption(
        '--release',
        action='store_true',
        dest='release',
        default=False,
        help="build release"
)

opts.Add(EnumVariable(
    'target',
    'Target Platform',
    host_platform,
    allowed_values=('linux', 'darwin', 'windows'),
    ignorecase=2
))

opts.Add(BoolVariable(
    'release',
    'Build Release',
    GetOption('release')
))

opts.Update(env)
Help(opts.GenerateHelpText(env))

if env['target'] == 'linux':
    env.Append(CXXFLAGS=['-std=c++17', '-fconcepts-ts'])

    if env['release'] == True:
        env.Append(CCFLAGS=['-O3', '-flto', '-s'])
    else:
        env.Append(CCFLAGS=['-Og', '-g', '-fmax-errors=1'])

elif env['target'] == 'windows':
    if host_platform == 'windows':
        env.Append(CCFLAGS=['/std:c++latest'])

        if env['release'] == True:
            env.Append(CCFLAGS=['/MT'])
        else:
            env.Append(CCFLAGS=['/MTd'])
    else:
        env['CC'] = 'x86_64-w64-mingw32-gcc'
        env['CXX'] = 'x86_64-w64-mingw32-g++'
        env['AR'] = "x86_64-w64-mingw32-ar"
        env['RANLIB'] = "x86_64-w64-mingw32-ranlib"
        env['LINK'] = "x86_64-w64-mingw32-g++"
        env['ENV']['PKG_CONFIG_PATH'] = '/usr/x86_64-w64-mingw32/lib/pkgconfig'

        env.Append(CXXFLAGS=['-std=c++20'])
        env.Append(LINKFLAGS=[
            '--static',
            '-Wl,--no-undefined',
            '-static-libgcc',
            '-static-libstdc++',
        ])

        if env['release'] == True:
            env.Append(CCFLAGS=['-O3', '-flto', '-s'])
        else:
            env.Append(CCFLAGS=['-Og', '-g'])

elif env['target'] == 'darwin':
    if host_platform != 'darwin':
        env['CC']    = env['OSXCROSS_ROOT'] + '/bin/o64-clang'
        env['CXX']    = env['OSXCROSS_ROOT'] + '/bin/o64-clang++'
        env['AR']     = env['OSXCROSS_ROOT'] + '/bin/x86_64-apple-darwin19-ar'
        env['RANLIB'] = env['OSXCROSS_ROOT'] + '/bin/x86_64-apple-darwin19-ranlib'
        env['LINK']   = env['OSXCROSS_ROOT'] + '/bin/o64-clang++'

    env.Append(CXXFLAGS=['-std=c++20'])

    if env['release'] == True:
        env.Append(CCFLAGS=['-O3', '-flto', '-s'])
    else:
        env.Append(CCFLAGS=['-Og', '-g', '-fmax-errors=1'])

abspath = env.Dir('.').abspath + '/'

env.Append(CPPPATH=[
    '.',
    abspath + '.',
    abspath + 'core',
    abspath + 'renderer',
    abspath + 'renderer/vulkan',
    abspath + 'ecs',
    abspath + 'thirdparty',
    abspath + 'thirdparty/spirv-reflect',
    abspath + 'thirdparty/spirv-reflect/include',
])

if host_platform != 'windows':
    env.Append(CPPPATH=['/usr/include/SDL2/'])

Export('env')

def add_sources(self, *files):
    for f in files:
        self.Append(sources=[self.Object(f)])

env.__class__.add_sources = add_sources

SConscript('core/SCsub',       variant_dir='build/' + env['target'] + '/core')
SConscript('renderer/SCsub',   variant_dir='build/' + env['target'] + '/renderer')
SConscript('ecs/SCsub',        variant_dir='build/' + env['target'] + '/ecs')
SConscript('thirdparty/SCsub', variant_dir='build/' + env['target'] + '/thirdparty')

env['libengine'] = env.StaticLibrary(target='build/' + env['target'] + '/libengine', source=env['sources'])

env.Append(LIBS=['SDL2', 'm', 'z'])
if env['target'] == 'windows':
    env.Append(LIBS=['vulkan-1.dll', 'SDL2main'])
    env.ParseConfig("pkg-config sdl2 --static --libs --cflags")
    env.Append(CCFLAGS=['-DSPNG_STATIC'])
else:
    env.Append(LIBS=['vulkan'])

Return('env')
