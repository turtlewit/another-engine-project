#include <csignal>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>

#include <logger.hpp>

#include "main.hpp"
#include "directional_shadow.hpp"

#ifdef _WIN32
#include <windows.h>
#endif

bool running = true;
int shading_mode = 0;
bool resized = false;

void FlightSynchronization::initialize(Device* device)
{
	for (uint32_t i = 0; i < Deferred::MAX_FRAMES_IN_FLIGHT; ++i) {
		image_available_semaphores.add(Semaphore::create_semaphore(device));
		render_finished_semaphores.add(Semaphore::create_semaphore(device));
		fences.add(Fence::create_fence(device, VK_FENCE_CREATE_SIGNALED_BIT));
	}
}

Deferred::Deferred()
	: sdl_window{}
	, window{ sdl_window.window }
	, provider{ window }
	, instance{ Instance::create_instance(provider, MAX_FRAMES_IN_FLIGHT) }
	, device{ instance.get_device() }
	, swapchain{ device->get_swapchain() }
	, command_pools{}
	, command_buffers{}
	, flight_sync{ device }
	, image_fences{}
	, abuffer_heads{}
	, abuffers{}
	, world{}
	, deferred_pipeline{}
{
	create_command_buffers();

	descriptor_pools.resize(MAX_FRAMES_IN_FLIGHT);

	//descriptor_buffers.resize(MAX_FRAMES_IN_FLIGHT);

	image_fences.reserve(swapchain->get_image_count());

	for (size_t i = 0; i < swapchain->get_image_count(); ++i)
		image_fences.add(nullptr);

	allocate_gbuffer_abuffer();

	deferred_pipeline = new DeferredPipeline(device, &world, &abuffer_heads, &abuffers);
	deferred_pipeline->set_shading_mode(shading_mode);
}

void Deferred::run()
{
	EventUserData event_user_data {
		.world = &world,
	};
	SDL_SetEventFilter(event_filter, &event_user_data);


	VkExtent2D swapchain_extent = swapchain->get_extent();

	uint32_t current_frame = 0;

	// Scene setup
	world = World{ 
		.camera = Camera::create_camera(
			60, f64_div(swapchain_extent.width, swapchain_extent.height), 
			0.1, 100.0) 
	};

	world.camera.position = glm::vec3(0, -.15, 2);
	world.camera.update_model_matrix();
	world.add_component(FlyCamera {
	});

	{
		auto light = world.add_component(DirectionalLight{ .direction = glm::normalize(glm::vec3( 1.0, 1.0, 1.0 )), .color = glm::vec3(1), .power = 1.0 });
		DirectionalShadowPipeline::enable_directional_shadow(device, &world, *world.get_component<DirectionalLight>(light), 0.001f, 10.0f);
	}
	{
		auto light = world.add_component(DirectionalLight{ .direction = glm::normalize(glm::vec3( 2.0, 2.0, 0.5 )), .color = glm::vec3(1), .power = 0.5 });
		DirectionalShadowPipeline::enable_directional_shadow(device, &world, *world.get_component<DirectionalLight>(light), 0.001f, 10.0f);
	}
	//world.add_component(PointLight{ .position = glm::vec3( 0.0, -1,  1.2), .color = glm::vec3(1, 1, 1), .power = 0.15 });
	//world.add_component(PointLight{ .position = glm::vec3(-0.45, -0.5,  .55), .color = glm::vec3(.5, .5, .5), .power = 0.15 });
	//world.add_component(PointLight{ .position = glm::vec3( 0.45, -0.4, -.55), .color = glm::vec3(.5, .5, .5), .power = 0.15 });
	//world.add_component(PointLight{ .position = glm::vec3(-0.45, -0.4, -.55), .color = glm::vec3(.5, .5, .5), .power = 0.15 });
	//world.add_component(PointLight{ .position = glm::vec3(0.0, -0.35, 0.0), .color = glm::vec3(1, 1, 1), .power = 0.25 });
	//for (float i = 0; i < M_PI * 2.0; i += M_PI / 2.0) {
	//	PointLight::ID id = world.add_component(PointLight{ .position = glm::vec3(-0.0, -1.5, 0.5), .color = glm::vec3(.5, .5, .5), .power = 0.25 });
	//	world.add_component(SpinLight{ .light = id, .current_angle = i, .speed = 3.14f });
	//}

	{
		//MeshComponent::ID mesh = world.meshes.add_component({ .mesh = Mesh(device, {
		//	{ { 0.0, -0.5, 0.0}, { 0.0,  0.0,  1.0}, {0.5, 0.5} }, /*     * 0   */
		//	{ { 0.5,  0.0, 0.0}, { 0.0,  0.0,  1.0}, {1.0, 0.0} }, /*    / \    */
		//	{ {-0.5,  0.0, 0.0}, { 0.0,  0.0,  1.0}, {0.0, 0.0} }, /* 2 * - * 1 */
		//	{ { 0.0, -0.5, 0.0}, { 0.0,  0.0, -1.0}, {0.5, 0.5} }, /*     * 0   */
		//	{ {-0.5,  0.0, 0.0}, { 0.0,  0.0, -1.0}, {0.0, 0.0} }, /*    / \    */
		//	{ { 0.5,  0.0, 0.0}, { 0.0,  0.0, -1.0}, {1.0, 0.0} }, /* 1 * - * 2 */
		//})});

		MeshComponent::ID quad = world.add_component(MeshComponent{ .mesh = Mesh(device, {
			{ {-0.5, -0.5, 0.0}, { 0.0,  0.0,  1.0}, {0.0, 0.0} }, /*   * 0   */
			{ { 0.5,  0.5, 0.0}, { 0.0,  0.0,  1.0}, {1.0, 1.0} }, /*   |\    */
			{ {-0.5,  0.5, 0.0}, { 0.0,  0.0,  1.0}, {0.0, 1.0} }, /* 2 *-* 1 */
			{ {-0.5, -0.5, 0.0}, { 0.0,  0.0,  1.0}, {0.0, 0.0} }, /* 0 *-* 1 */
			{ { 0.5, -0.5, 0.0}, { 0.0,  0.0,  1.0}, {1.0, 0.0} }, /*    \|   */
			{ { 0.5,  0.5, 0.0}, { 0.0,  0.0,  1.0}, {1.0, 1.0} }, /*     * 2 */
		})});

		//{
		//	MeshInstance::ID id = world.mesh_instances.add_component({ .mesh = mesh, .color = glm::vec4(0.3f, 1.0f, 0.3f, 0.55) });
		//	MeshInstance& mi = *world.mesh_instances.get_component(id);
		//	mi.position = glm::vec3(0.0, .25, 0.5);
		//	mi.rotation.y = M_PI / 4.0;
		//	mi.update_basis();
		//	mi.update_model_matrix();
		//}
		//{
		//	MeshInstance::ID id = world.mesh_instances.add_component({ .mesh = mesh, .color = glm::vec4(0.3f, 0.3f, 1.0f, 0.55) });
		//	MeshInstance& mi = *world.mesh_instances.get_component(id);
		//	mi.position = glm::vec3(0.00, .35, 0.35);
		//	mi.rotation.y = -M_PI / 4.0;
		//	mi.update_basis();
		//	mi.update_model_matrix();
		//}


		//for (uint32_t i = 1; i < 5; ++i) {
		//	MeshInstance::ID id = world.mesh_instances.add_component({ .mesh = mesh, .color = glm::vec4(1.0f, 0.3f, 0.3f, 0.5f) });
		//	MeshInstance& mi = *world.mesh_instances.get_component(id);
		//	mi.position = glm::vec3(0.00, 0.0, f32(i) / 5.0);
		//	mi.update_model_matrix();
		//}

		{
			MeshInstance::ID id = world.add_component(MeshInstance{ .mesh = quad, .color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f) });
			MeshInstance& mi = *world.get_component<MeshInstance>(id);
			mi.position.y = 0.45;
			mi.rotation.x = M_PI / 2.0;
			mi.scale = glm::vec3(10.0, 10.0, 10.0);
			mi.update_basis();
			mi.update_model_matrix();
		}

		//world.mesh_instances.add_component({ .mesh = mesh, .color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f) });
	}

	world.load_gltf(device, "/home/kat/misc/Models/thirdparty/lamp/modified/lamp.gltf",
		glm::vec3(0, -.26, 0), glm::vec3(), glm::vec3(0.010,  0.010,  0.010));
//	world.load_gltf(device, "/home/kat/misc/Models/gold/gold.gltf",
//		glm::vec3(0, -.26, 0), glm::vec3(), glm::vec3(0.5, 0.5, 0.5));

	world.systems.add(fly_camera_system);

	
	// render pass setup


	size_t frame = 0;

	Transform3D camera_pivot{};

	// main loop
	while (running) {
		swapchain_extent = swapchain->get_extent();


		{ // Input
			// wait for events to be processed
			while (SDL_PollEvent(NULL)) {}
		}



		{ // Update
			for (auto system : world.systems) {
				system(world);
			};

			for (auto& light : world.get_components<PointLight>()) {
				light.position = glm::rotateY(light.position, f32(1.0 / 120.0));
			}

			//for (auto& light : world.get_components<DirectionalLight>()) {
			//	light.direction = glm::rotateY(light.direction, f32(1.0 / 120.0) * 0.5f);
			//}
		}



		{ // Post-Update
			world.get_components<InputEvent>().clear();
		}


		// Render

		flight_sync.fences[current_frame].wait();

		if (resized) {
			resized = false;
			recreate_swapchain();
		}

		uint32_t iterations = 0;
		while (swapchain->acquire_next_image(flight_sync.image_available_semaphores[current_frame].get_inner()) != VK_SUCCESS) {
			if (iterations > 10) {
				LOG_ERROR("Failed to recreate swapchain, aborting.\n");
				return;
			}

			recreate_swapchain();
			++iterations;
		}

		uint32_t image_index = swapchain->get_current_image_index();

		// If the current image is still in use by another frame, we need to
		// wait for it to finish.
		if (image_fences[image_index] != nullptr) {
			image_fences[image_index]->wait();
		}

		// Mark the current image as a possibility that it is in use
		image_fences[image_index] = &flight_sync.fences[current_frame];


		// ************************ //
		// DESCRIPTOR POOL CREATION //
		// ************************ //

		delete descriptor_pools[current_frame];

		{
			uint32_t directional_shadow_max_sets = 0;
			SimpleBuffer<VkDescriptorPoolSize> directional_shadow_descriptor_pool_sizes{};
			for (auto& light : world.get_components<DirectionalLight>()) {
				if (!light.directional_shadow_pipeline)
					continue;
				directional_shadow_max_sets += light.directional_shadow_pipeline->get_max_descriptor_sets();
				directional_shadow_descriptor_pool_sizes = directional_shadow_descriptor_pool_sizes + light.directional_shadow_pipeline->get_descriptor_pool_sizes();
			}
			descriptor_pools[current_frame] = DescriptorPool::create_descriptor_pool(
				device,
				deferred_pipeline->get_max_descriptor_sets() + directional_shadow_max_sets,
				deferred_pipeline->get_descriptor_pool_sizes() + directional_shadow_descriptor_pool_sizes
			);
		}
		auto& descriptor_pool = *descriptor_pools[current_frame];

		
		// command buffer recording
		CommandPool& command_pool = command_pools[current_frame];
		command_pool.reset();
		CommandBuffer& command_buffer = command_buffers[current_frame];

		command_buffer.begin();

		// drawing
		for (auto& light : world.get_components<DirectionalLight>()) {
			if (!light.directional_shadow_pipeline)
				continue;

			light.directional_shadow_pipeline->draw(descriptor_pool, command_buffer, current_frame);
		}
		deferred_pipeline->draw(descriptor_pool, command_buffer, current_frame);

		command_buffer.end();

		flight_sync.fences[current_frame].reset();

		device->get_graphics_queue().submit(
			{ command_buffer.get_inner() }, 
			{ flight_sync.image_available_semaphores[current_frame].get_inner() }, 
			{ VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT }, 
			{ flight_sync.render_finished_semaphores[current_frame].get_inner() }, 
			flight_sync.fences[current_frame].get_inner()
		);

		swapchain->present(flight_sync.render_finished_semaphores[current_frame].get_inner());

		current_frame = (current_frame + 1) % MAX_FRAMES_IN_FLIGHT;
		frame += 1;
	}

	SDL_SetEventFilter(nullptr, nullptr);
}

void Deferred::create_command_buffers()
{
	command_buffers.clear();
	command_pools.clear();

	command_pools.reserve(MAX_FRAMES_IN_FLIGHT);
	command_buffers.reserve(MAX_FRAMES_IN_FLIGHT);

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
		command_pools.add(CommandPool::create_command_pool(device, device->get_queue_families().graphics));
		command_buffers.add(command_pools[i].allocate_command_buffer());
	}
}

void Deferred::allocate_gbuffer_abuffer()
{
	abuffer_heads.clear();
	abuffers.clear();

	auto swapchain_extent = swapchain->get_extent();

	abuffer_heads.reserve(MAX_FRAMES_IN_FLIGHT);
	abuffers.reserve(MAX_FRAMES_IN_FLIGHT);
	for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
		abuffers.add(Memory::allocate_descriptor_memory(device, ABuffer::get_data_size(swapchain_extent.width, swapchain_extent.height), 
					VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
					VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT));
		abuffer_heads.add(Memory::allocate_descriptor_memory(device, 
					ABuffer::get_nodes_size(swapchain_extent.width, swapchain_extent.height), 
					VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
					VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT));
	}
}

void Deferred::recreate_swapchain()
{
	vkDeviceWaitIdle(device->get_inner());
	flight_sync.reset();
	deferred_pipeline->destroy_render_pass();

	device->recreate_swapchain();
	swapchain = device->get_swapchain();

	allocate_gbuffer_abuffer();
	flight_sync.initialize(device);
	deferred_pipeline->handle_resize();
	create_command_buffers();

	image_fences.clear();
	for (size_t i = 0; i < swapchain->get_image_count(); ++i)
		image_fences.add(nullptr);

	auto extent = swapchain->get_extent();
	world.camera.aspect = f64_div(extent.width, extent.height);
	world.camera.update_projection_matrix();
}


Deferred::~Deferred()
{
	vkDeviceWaitIdle(device->get_inner());

	for (auto& light : world.get_components<DirectionalLight>()) {
		if (light.directional_shadow_pipeline) {
			delete light.directional_shadow_pipeline;
			light.directional_shadow_pipeline = nullptr;
		}
	}

	delete deferred_pipeline;
	for (auto& pool : descriptor_pools)
		delete pool;
}

void signal_handler(int signum)
{
	running = false;
}

int event_filter(void* userdata, SDL_Event* event)
{
	EventUserData& ud = *(EventUserData*) userdata;
	World& world = *ud.world;
	std::mutex& mutex = ud.mutex;

	switch (event->type) {

		case SDL_WINDOWEVENT: {
			switch (event->window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					running = false;
					break;
				case SDL_WINDOWEVENT_RESIZED:
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					resized = true;
					break;
			}
		} break;

		case SDL_KEYUP:
		case SDL_KEYDOWN:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEMOTION:
			if (event->key.repeat)
				break;
			mutex.lock();
			world.add_component(InputEvent{ .event = *event });
			mutex.unlock();
			break;
	}

	return 0;
}

template <typename... T>
bool is_arg(const char* a, T... args) {
	return ((strcmp(a, args) == 0) || ...);
}

template <typename T>
constexpr bool is_power_of_two(T x)
{
	return x == (1 << u32(ceil(log2(f32(x)))));
}

constexpr const char* help_text = 
"Usage: ./deferred [options] \n\
\n\
Options:\n\
    --mode N     Run with shading mode.\n\
                   0 - Regular shading\n\
                   1 - Position buffer\n\
                   2 - Normal buffer\n\
                   3 - Albedo buffer\n\
                   4 - Depth buffer\n\
\n\
    --samples N  Run with N samples.\n\
    --no-vsync   Run without vsync.\n\
\n\
    --help       Display this help message.\n\n";

int main(int argc, char* argv[])
{
	// initialize signal handlers
	signal(SIGABRT, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	for (int i = 0; i < argc; ++i) {
		if (is_arg(argv[i], "--help", "-h", "/?")) {
			std::cout << help_text << std::endl;
			#ifdef _WIN32
			MessageBox(nullptr, help_text, help_text, 0);
			GetStockObject(0);
			#endif
			return 0;
		} else if (is_arg(argv[i], "--samples")) {
			if (i + 1 < argc) {
				int64_t num_samples = atoll(argv[i + 1]);
				i += 1;

				if (num_samples < 0) {
					LOG_ERROR("Number of samples must be greater than 0!\n");
					return 1;
				} else if (num_samples > 64) {
					LOG_ERROR("Vulkan only supports up to 64 samples!\n");
					return 1;
				} else if (!is_power_of_two(num_samples)) {
					LOG_ERROR("Sample count must be a power of two!\n");
					return 1;
				}

				uint32_t sample_mask = (1 << (u32(log2(f32(num_samples))) + 1)) - 1;
				Logger::log_info("Requested sample mask: 0x%02x\n", sample_mask);
				DeferredPipeline::allowed_samples = sample_mask;
			}
		} else if (is_arg(argv[i], "--no-vsync")) {
			Swapchain::set_preferred_present_mode(VK_PRESENT_MODE_MAILBOX_KHR);
		} else if (is_arg(argv[i], "--mode")) {
			if (i + 1 < argc) {
				shading_mode = atoi(argv[i + 1]);
				i += 1;
			}
		}
	}

	Logger::log_info("Starting app \"deferred\".\n");

	Deferred deferred{};


	deferred.run();

	return 0;
}
