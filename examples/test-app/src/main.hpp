#pragma once

#include <mutex>

#include <SDL.h>
#include <SDL_video.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <surface_provider.hpp>
#include <instance.hpp>
#include <swapchain.hpp>
#include <device.hpp>
#include <render_pass.hpp>
#include <command_buffer.hpp>
#include <pipeline.hpp>
#include <memory.hpp>
#include <quad.hpp>
#include <descriptor.hpp>

#include "world.hpp"
#include "deferred_pipeline.hpp"
#include "gbuffer.hpp"
#include "fly_camera_system.hpp"


using namespace renderer;

struct FlightSynchronization {
	Vector<Semaphore> image_available_semaphores;
	Vector<Semaphore> render_finished_semaphores;
	Vector<Fence> fences;

	FlightSynchronization() = default;

	FlightSynchronization(Device* device)
	{
		initialize(device);
	}

	void reset()
	{
		image_available_semaphores.clear();
		render_finished_semaphores.clear();
		fences.clear();
	}

	void initialize(Device* device);
};

class SDLWindow {
public:
	static constexpr int WINDOW_FLAGS = SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE;
	static constexpr int WINDOW_WIDTH = 800;
	static constexpr int WINDOW_HEIGHT = 600;
	static constexpr const char WINDOW_TITLE[] = "Kat Witten - ICS 163 - Deferred Shading";

	SDL_Window* window;

	SDLWindow()
	{
		SDL_Init(SDL_INIT_VIDEO);
		window = SDL_CreateWindow(
				WINDOW_TITLE,
				SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				WINDOW_WIDTH,
				WINDOW_HEIGHT,
				WINDOW_FLAGS);
	}

	~SDLWindow()
	{
		SDL_DestroyWindow(window);
		SDL_Quit();
	}
};

class Deferred {
public:
	static constexpr uint32_t MAX_FRAMES_IN_FLIGHT = 1;

	void run();

	Deferred();
	~Deferred();

private:
	void allocate_gbuffer_abuffer();
	void recreate_swapchain();
	void create_command_buffers();

	SDLWindow sdl_window;
	SDL_Window* window;
	SurfaceProvider provider;
	
	Instance instance;
	Device* device;
	Swapchain* swapchain;
	
	Vector<CommandPool> command_pools;
	Vector<CommandBuffer> command_buffers;

	Vector<DescriptorPool*> descriptor_pools;
	//Vector<SharedDeviceBuffer> descriptor_buffers;

	FlightSynchronization flight_sync;
	Vector<Fence*> image_fences;

	Vector<SharedDeviceBuffer> abuffer_heads;
	Vector<SharedDeviceBuffer> abuffers;

	World world;

	DeferredPipeline* deferred_pipeline;
	
};

struct EventUserData {
	World* world;
	std::mutex mutex{};
};

int event_filter(void*, SDL_Event*);
